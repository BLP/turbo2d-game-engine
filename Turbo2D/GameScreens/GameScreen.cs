﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using Turbo2D.Managers;
using Turbo2D.GameObjects;
using Turbo2D.Collections;
using Turbo2D.Serialization;
using System.IO;

namespace Turbo2D.GameScreens
{
    public enum GameScreenState
    {
        TransitioningOn,
        Active,
        TransitioningOff,
    }

    public abstract class GameScreen
    {
        protected internal bool AllowDrawBelow { get; protected set; }
        protected internal bool AllowUpdateBelow { get; protected set; }
        protected internal bool AllowInputBelow { get; protected set; }
        public GameScreenHelper Parent { get; private set; }
        protected internal BaseEngine Engine { get; private set; }
        protected Vector2 Position;
        protected float Rotation;
        protected Vector2 Scale;
        protected internal float Alpha;
        protected Vector2 Origin;
        public Matrix Transformation { get { return CollisionHelper.CreateMatrix(Origin, Rotation, Position, Scale); } }
        // Transition state information
        protected TimeSpan TransitionOnTime;
        protected TimeSpan TransitionOffTime;
        public GameScreenState State { get; private set; }
        private TimeSpan CurrentTransitionTime;
        protected internal float TransitionLocation { get; private set; }
        // Misc
        private MouseInputHelper MouseInput;
        private KeyboardInputHelper KeyboardInput;
        private GamePadInputHelper GamePadInput;
        protected ContentManager Content;
        protected internal RenderViewportCollection Viewports = new RenderViewportCollection();
        public GameObjectCollection GameObjects { get; private set; }
        protected internal ManagerCollection Managers { get; private set; }
        protected internal Renderer RenderDevice { get; private set; }

        protected internal virtual void LoadContent(ContentManager content, GameScreenHelper parent)
        {
            Parent = parent;
            Engine = Parent.Engine;
            Content = content;

            GameObjects = new GameObjectCollection(Content);
            GameObjects.ObjectAdded += new EventHandler<GameObjectEventArgs>(GameObjects_ObjectAdded);
            GameObjects.ObjectRemoved += new EventHandler<GameObjectEventArgs>(GameObjects_ObjectRemoved);
            Managers = new ManagerCollection();
            Scale = new Vector2(1f);
            State = GameScreenState.TransitioningOn;
            RenderDevice = new Renderer(this, Engine.Batch);
            SetupGraphics();
            Alpha = 1.0f;
            MouseInput = new MouseInputHelper(this);
            KeyboardInput = new KeyboardInputHelper();
            GamePadInput = new GamePadInputHelper();
        }

        private void GameObjects_ObjectRemoved(object sender, GameObjectEventArgs e)
        {
            // Register the GameObject with the managers
            Managers.DeregisterGameObject(e.Object);

            // Register a drawable GameObject with the RenderGroups
            if (e.Object is IDrawableObject)
                RenderDevice.RenderGroups.DeregisterGameObject(e.Object as IDrawableObject);
        }

        private void GameObjects_ObjectAdded(object sender, GameObjectEventArgs e)
        {
            // Deregister the GameObject from the managers
            Managers.RegisterGameObject(e.Object);

            // Deregister a drawable GameObject from the RenderGroups
            if (e.Object is IDrawableObject)
                RenderDevice.RenderGroups.RegisterGameObject(e.Object as IDrawableObject);
        }

        /// <summary>
        /// Sets up the RenderViewports and RenderGroups for the screen.
        /// Override to create a custom setup.
        /// </summary>
        protected virtual void SetupGraphics()
        {
            Viewport viewport = Engine.DefaultViewport;
            Viewports.Add(viewport, "Screen");
            //GameObjects.AddRenderGroup("Default", true, 1);
            RenderDevice.RenderGroups.Add("Default", true, 1);
        }

        internal void UpdateInput(GameTime gameTime)
        {
            MouseInput.Update(gameTime);
            KeyboardInput.Update(gameTime);

            if (State == GameScreenState.Active)
            {
                IEnumerable<IAcceptsMouseInput> mouseTemp = GameObjects.OfType<IAcceptsMouseInput>().ToArray();
                foreach (IAcceptsMouseInput inputObject in mouseTemp.Where(obj => obj.Enabled))
                {
                    /*if (inputObject.Enabled)
                    {*/
                        // Loop through each viewport that the object is in, set the input helper for that viewport, and check input
                        foreach (string viewportName in inputObject.Viewports)
                        {
                            MouseInput.SetCurrentViewport(Viewports[viewportName]);
                            inputObject.UpdateMouse(gameTime, MouseInput);
                        }
                        // Set input viewport back to null
                        MouseInput.SetCurrentViewport(null);
                    //}
                    if (!inputObject.Alive)
                        GameObjects.Remove(inputObject.Name);
                }
                IEnumerable<IAcceptsKeyboardInput> keyboardTemp = GameObjects.OfType<IAcceptsKeyboardInput>().ToArray();
                foreach (IAcceptsKeyboardInput inputObject in keyboardTemp.Where(obj => obj.Enabled))
                {
                    //if (inputObject.Enabled)
                        inputObject.UpdateKeyboard(gameTime, KeyboardInput);
                    if (!inputObject.Alive)
                        GameObjects.Remove(inputObject.Name);
                }
                IEnumerable<IAcceptsGamePadInput> gamePadTemp = GameObjects.OfType<IAcceptsGamePadInput>().ToArray();
                foreach (IAcceptsGamePadInput inputObject in gamePadTemp.Where(obj => obj.Enabled))
                {
                    //if (inputObject.Enabled)
                        inputObject.UpdateGamePad(gameTime, GamePadInput);
                    if (!inputObject.Alive)
                        GameObjects.Remove(inputObject.Name);
                }

                UpdateActiveInput(gameTime, KeyboardInput, MouseInput);
            }
        }

        /// <summary>
        /// Update the GameScreen
        /// </summary>
        /// <param name="gameTime">The time passed since the last call to update</param>
        internal void Update(GameTime gameTime)
        {
            if (State == GameScreenState.TransitioningOn)
            {
                CurrentTransitionTime += gameTime.ElapsedGameTime;
                TransitionLocation = MathHelper.Clamp((float)(CurrentTransitionTime.TotalSeconds / TransitionOnTime.TotalSeconds), 0f, 1f);
                if (CurrentTransitionTime >= TransitionOnTime)
                {
                    CurrentTransitionTime = TimeSpan.Zero;
                    State = GameScreenState.Active;
                }
                UpdateOnTransition(gameTime);
                TransitionLocation = 0f;
            }
            else if (State == GameScreenState.Active)
            {
                foreach (RenderViewport viewport in Viewports)
                    viewport.UpdateCameras(gameTime);

                IEnumerable<IUpdateableObject> temp = GameObjects.OfType<IUpdateableObject>().ToArray();
                foreach (IUpdateableObject updateable in temp.Where(obj => obj.Enabled))
                {
                    //if (updateable.Enabled)
                        updateable.Update(gameTime);
                    if (!updateable.Alive)
                    {
                        GameObjects.Remove(updateable.Name);
                    }
                }
                // Update the managers
                foreach (IManager manager in Managers.Where(manager => manager.Enabled))
                    //if (manager.Enabled)
                        manager.Update(gameTime);

                UpdateActive(gameTime);
            }
            else if (State == GameScreenState.TransitioningOff)
            {
                CurrentTransitionTime += gameTime.ElapsedGameTime;
                TransitionLocation = MathHelper.Clamp((float)(CurrentTransitionTime.TotalSeconds / TransitionOffTime.TotalSeconds), 0f, 1f);
                UpdateOffTransition(gameTime);
            }
        }

        protected virtual void UpdateOnTransition(GameTime gameTime)
        {
        }

        protected virtual void UpdateOffTransition(GameTime gameTime)
        {
        }

        protected virtual void UpdateActive(GameTime gameTime)
        {
        }

        protected virtual void UpdateActiveInput(GameTime gameTime, KeyboardInputHelper keyboardInput, MouseInputHelper mouseInput)
        {
        }

        protected internal virtual void Draw()
        {
            foreach (RenderViewport viewport in Viewports)
            {
                RenderDevice.DrawViewport(viewport);
            }
        }

        public virtual void Exit(GameScreen nextScreen)
        {
            TransitionLocation = 0f;
            State = GameScreenState.TransitioningOff;
            if (nextScreen != null)
                Parent.InsertGameScreen(nextScreen);
        }

        public virtual void Exit()
        {
            Exit(null);
        }

        protected virtual void ImmediateExit()
        {
            ImmediateExit(null);
        }

        protected virtual void ImmediateExit(GameScreen nextScreen)
        {
            if (nextScreen != null)
                Parent.AddGameScreen(nextScreen);
            State = GameScreenState.TransitioningOff;
            TransitionLocation = 1.0f;
        }

        protected internal virtual void Dispose()
        {
            foreach (IDisposableObject disposable in GameObjects.OfType<IDisposableObject>())
                disposable.Dispose();
        }

        protected void Save(string fileName)
        {
            using (BinaryWriter binWriter = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                binWriter.Write(TransitionOnTime);
                binWriter.Write(TransitionOffTime);
                binWriter.Write(AllowDrawBelow);
                binWriter.Write(AllowInputBelow);
                binWriter.Write(AllowUpdateBelow);

                // Save RenderGroups
                RenderGroupCollection.Save(binWriter, RenderDevice.RenderGroups);

                // Save viewports
                RenderViewportCollection.Save(binWriter, Viewports);

                // Save managers
                ManagerCollection.Save(binWriter, Managers);

                // Save GameObjects
                GameObjectCollection.Save(binWriter, GameObjects);

                binWriter.Close();
            }
        }

        protected void Load(string fileName)
        {
            if (File.Exists(fileName))
            {
                BinaryReader binReader = new BinaryReader(File.Open(fileName, FileMode.Open));
                try
                {
                    TransitionOnTime = binReader.ReadTimeSpan();
                    TransitionOffTime = binReader.ReadTimeSpan();
                    AllowDrawBelow = binReader.ReadBoolean();
                    AllowInputBelow = binReader.ReadBoolean();
                    AllowUpdateBelow = binReader.ReadBoolean();

                    // Load RenderGroups
                    RenderDevice.RenderGroups = RenderGroupCollection.Load(binReader);

                    // Load viewports
                    Viewports = RenderViewportCollection.Load(binReader, this);

                    // Load managers
                    Managers = ManagerCollection.Load(binReader, this);

                    // Load GameObjects
                    GameObjects = GameObjectCollection.Load(binReader, Content, this, new EventHandler<GameObjectEventArgs>(GameObjects_ObjectAdded),
                        new EventHandler<GameObjectEventArgs>(GameObjects_ObjectRemoved));
                }
                finally
                {
                    binReader.Close();
                }
            }
        }
    }
}
