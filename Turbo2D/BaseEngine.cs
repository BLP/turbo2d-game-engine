using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Serialization;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Turbo2DEditor")]

namespace Turbo2D
{
    /// <summary>
    /// The BaseEngine class inherits the Game class and provides all of the functionality to create a TurboGame
    /// </summary>
    public sealed class BaseEngine : Game
    {
        // Graphics objects
        /// <summary>
        /// The GraphicsDeviceManager that controls all the drawing for the game
        /// </summary>
        public GraphicsDeviceManager Graphics { get; private set; }
        /// <summary>
        /// The SpriteBatch used for all game drawing
        /// </summary>
        internal SpriteBatch Batch { get; private set; }
        /// <summary>
        /// The Viewport currently set on the GraphicsDevice
        /// </summary>
        public Viewport CurrentViewport { get { return GraphicsDevice.Viewport; } }
        /// <summary>
        /// The original Viewport created for the game
        /// </summary>
        public Viewport DefaultViewport { get; private set; }
        // Helpers
        /// <summary>
        /// The GameScreenHelper that manages all of the GameScreens in the game
        /// </summary>
        public GameScreenHelper ScreenHelper { get; private set; }
        /// <summary>
        /// The SoundHelper that manages the game's sounds (both Xact and SoundEffects)
        /// </summary>
        public SoundHelper Sound { get; private set; }
        /// <summary>
        /// The SongHelper that manages the playing of music
        /// </summary>
        public SongHelper Music { get; private set; }
        /// <summary>
        /// The Random object for generating random numbers in the game
        /// </summary>
        public Random RandomGenerator { get; private set; }
        // Game-specific objects
        /// <summary>
        /// The current TurboGame being run by the BaseEngine
        /// </summary>
        public TurboGame Game { get; private set; }
        /// <summary>
        /// The Settings being used by the TurboGame
        /// </summary>
        public Settings CurrentSettings { get; private set; }
        /// <summary>
        /// The first GameScreen for the game
        /// </summary>
        private GameScreen FirstScreen;
        // EventHandlers
        /// <summary>
        /// Occurs when the Engine's LoadContent function is called
        /// </summary>
        public event EventHandler LoadContentEvent;
        /// <summary>
        /// Occurs when the Engine's UnloadContent function is called
        /// </summary>
        public event EventHandler UnloadContentEvent;
        /// <summary>
        /// Occurs when the Engine's Initialize function is called
        /// </summary>
        public event EventHandler InitializeEvent;
        /// <summary>
        /// Called every Engine update cycle
        /// </summary>
        public event EventHandler<UpdateEventArgs> UpdateEvent;
        /// <summary>
        /// Called every Engine draw cycle
        /// </summary>
        public event EventHandler<DrawEventArgs> DrawEvent;

        /// <summary>
        /// Initializes a new instance of BaseEngine
        /// </summary>
        /// <param name="game">The TurboGame the Engine will be running</param>
        /// <param name="firstScreen">The first GameScreen of the game</param>
        internal BaseEngine(TurboGame game, GameScreen firstScreen)
            : this(game, firstScreen, Settings.CreateDefault())
        {
        }

        /// <summary>
        /// Initializes a new instance of BaseEngine
        /// </summary>
        /// <param name="game">The TurboGame the Engine will be running</param>
        /// <param name="firstScreen">The first GameScreen of the game</param>
        /// <param name="settings">The Settings to be used for the game</param>
        internal BaseEngine(TurboGame game, GameScreen firstScreen, Settings settings)
        {
            // Setup graphics\helper resources.
            Graphics = new GraphicsDeviceManager(this);
            ScreenHelper = new GameScreenHelper(this);
            RandomGenerator = new Random(System.Environment.TickCount);
            Music = new SongHelper(this);
            Sound = new SoundHelper();

            // Store the settings that are passed in and apply them.
            CurrentSettings = settings;
            ApplySettings();

            // Set the TurboGame to be run and the first GameScreen to be displayed.
            Game = game;
            FirstScreen = firstScreen;
        }

        /// <summary>
        /// Updates the Settings for the game
        /// </summary>
        /// <param name="settings">The new Settings to use</param>
        public void UpdateSettings(Settings settings)
        {
            // Store the new Settings and apply them.
            CurrentSettings = settings;
            ApplySettings();
        }

        /// <summary>
        /// Applies the current Settings
        /// </summary>
        private void ApplySettings()
        {
            // Set the ContentManager settings
            Content.RootDirectory = CurrentSettings.Content_RootDirectory;
            // Set the Game settings.
            InactiveSleepTime = CurrentSettings.Game_InactiveSleepTime;
            IsFixedTimeStep = CurrentSettings.Game_IsFixedTimeStep;
            IsMouseVisible = CurrentSettings.Game_IsMouseVisible;
            TargetElapsedTime = CurrentSettings.Game_TargetElapsedTime;
            // Set the GraphicsDeviceManager settings.
            Graphics.IsFullScreen = CurrentSettings.Graphics_IsFullScreen;
            Graphics.PreferredBackBufferHeight = CurrentSettings.Graphics_PreferredBackBufferHeight;
            Graphics.PreferredBackBufferWidth = CurrentSettings.Graphics_PreferredBackBufferWidth;
            Graphics.SynchronizeWithVerticalRetrace = CurrentSettings.Graphics_SychronizeWithVerticleRetrace;
            // Set the GameWindow settings.
            Window.AllowUserResizing = CurrentSettings.Window_AllowUserResizing;
            Window.Title = CurrentSettings.Window_Title;
            // Apply the changes to the GraphicsDeviceManager.
            Graphics.ApplyChanges();
            // Update the default viewport
            DefaultViewport = GraphicsDevice.Viewport;
        }

        /// <summary>
        /// Perform all the Engine initialization.
        /// </summary>
        protected override void Initialize()
        {
            // Initailize the input and sound helpers.
            //Sound.Initialize(Content);
            // Initialize the primitive renderer.
            Primitives.Initialize(Graphics.GraphicsDevice);
            // Let everyone know we're initializing.
            if (InitializeEvent != null)
                InitializeEvent(this, new EventArgs());

            // Let the Game class do it's initialization.
            base.Initialize();
        }

        /// <summary>
        /// Loads the Engine's content
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which is used to draw textures.
            Batch = new SpriteBatch(GraphicsDevice);

            // Add the game's first GameScreen to the GameScreenHelper.
            ScreenHelper.AddGameScreen(FirstScreen);

            // Let everyone know we're loading our content.
            if (LoadContentEvent != null)
                LoadContentEvent(this, new EventArgs());
        }

        /// <summary>
        /// Unloads the Engine's content
        /// </summary>
        protected override void UnloadContent()
        {
            // Finalize the SoundHelper.
            //Sound.Dispose();

            // Let everyone know we're unloading our content.
            if (UnloadContentEvent != null)
                UnloadContentEvent(this, new EventArgs());
        }

        /// <summary>
        /// Handles when the game gets the focus
        /// </summary>
        /// <param name="sender">The game</param>
        /// <param name="args">The arguments</param>
        protected override void OnActivated(object sender, EventArgs args)
        {
            // Resume the all playing sound effects.
            //Sound.ResumeCues();

            // Let the Game class do it's activation.
            base.OnActivated(sender, args);
        }

        /// <summary>
        /// Handles when the game loses the focus
        /// </summary>
        /// <param name="sender">The game</param>
        /// <param name="args">The arguments</param>
        protected override void OnDeactivated(object sender, EventArgs args)
        {
            // Pause the the playing sound effects.
            //Sound.PauseCues();

            // Let the Game class do it's deactivation.
            base.OnDeactivated(sender, args);
        }

        /// <summary>
        /// Does all the game's updating
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Update</param>
        protected override void Update(GameTime gameTime)
        {
            // If the game doesn't have the focus, don't update it.
            if (!IsActive)
                return;

            // Update the sound helper.
            //Sound.Update();

            // Update the song helper
            Music.Update(gameTime);

            // Update the GameScreens.
            ScreenHelper.Update(gameTime);

            // Let everyone know we're updating.
            if (UpdateEvent != null)
                UpdateEvent(this, new UpdateEventArgs(gameTime));

            // Let the Game class do it's updating.
            base.Update(gameTime);
        }

        /// <summary>
        /// Causes a draw to happen immediately.
        /// </summary>
        public void ForceDraw()
        {
            BeginDraw();
            Draw(new GameTime());
            EndDraw();
        }

        /// <summary>
        /// Does all the game's drawing
        /// </summary>
        /// <param name="gameTime">Tme passed since the last call to Draw</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the backbuffer.
            Graphics.GraphicsDevice.Clear(CurrentSettings.DefaultBackgroundColor);
            // Draw the GameScreens.
            ScreenHelper.Draw();
            // Let everyone know we're drawing.
            if (DrawEvent != null)
                DrawEvent(this, new DrawEventArgs(Batch));

            // Let the Game class do it's drawing.
            base.Draw(gameTime);
        }
    }

    /// <summary>
    /// EventArgs for when the Engine is updating
    /// </summary>
    public class UpdateEventArgs : EventArgs
    {
        /// <summary>
        /// The time past since the last call to Update
        /// </summary>
        public GameTime GameTime { get; protected set; }

        /// <summary>
        /// Initializes a new instance of UpdateEventArgs
        /// </summary>
        /// <param name="gameTime">The GameTime passed into the Update function</param>
        public UpdateEventArgs(GameTime gameTime)
        {
            GameTime = gameTime;
        }
    }

    /// <summary>
    /// EventArgs for when the Engine is drawing
    /// </summary>
    public class DrawEventArgs : EventArgs
    {
        /// <summary>
        /// The SpriteBatch used for drawing
        /// </summary>
        public SpriteBatch SpriteBatch { get; protected set; }

        /// <summary>
        /// Initializes a new instance of DrawEventArgs
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch to use for drawing</param>
        public DrawEventArgs(SpriteBatch spriteBatch)
        {
            SpriteBatch = spriteBatch;
        }
    }
}
