﻿using Microsoft.Xna.Framework;
using Turbo2D.Helpers;

namespace Turbo2D.Interfaces
{
    public interface IAcceptsKeyboardInput
    {
        bool Enabled { get; set; }
        bool Alive { get; }
        string Name { get; }

        void UpdateKeyboard(GameTime gameTime, KeyboardInputHelper input);
    }
}
