﻿using System;
using Microsoft.Xna.Framework;

namespace Turbo2D.Interfaces
{
    public interface IClickable
    {
        float Layer { get; set; }
        Rectangle ClickableRectangle { get; set; }
        bool Visible { get; set; }
        bool MouseVisible { get; set; }
        bool Alive { get; }
        string Name { get; }

        event EventHandler KillEvent;

        void MouseUpdate();
    }
}
