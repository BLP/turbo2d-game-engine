﻿using Microsoft.Xna.Framework;

namespace Turbo2D.Interfaces
{
    public interface IUpdateableObject
    {
        bool Enabled { get; set; }
        bool Alive { get; }
        string Name { get; }

        void Update(GameTime gameTime);
    }
}
