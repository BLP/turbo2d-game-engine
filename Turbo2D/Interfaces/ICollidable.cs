﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Turbo2D.GameObjects;

namespace Turbo2D.Interfaces
{
    public interface ICollidable
    {
        Rectangle CollisionRectangle { get; }
        //List<Rectangle> TransformedPartRectangles { get; }
        Matrix Transformation { get; }
        Color[] CollisionColors { get; }
        //List<string> CollidableObjects { get; set; }
        //List<string> CollidedObjects { get; set; }
        string Name { get; }
        int TextureWidth { get; }
        int TextureHeight { get; }
        //Type ObjectType { get; }
        bool Alive { get; }
        bool Collidable { get; }

        void CollisionOccurred(GameObject collidedWith);
    }
}
