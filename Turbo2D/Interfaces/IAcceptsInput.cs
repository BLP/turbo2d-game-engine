﻿using Microsoft.Xna.Framework;

namespace Turbo2D.Interfaces
{
    public interface IAcceptsInput
    {
        bool Enabled { get; set; }
        bool Alive { get; }
        string Name { get; }

        void UpdateInput(GameTime gameTime);
    }
}
