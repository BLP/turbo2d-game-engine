﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Turbo2D.Helpers;

namespace Turbo2D.Interfaces
{
    public interface IAcceptsGamePadInput
    {
        bool Enabled { get; set; }
        bool Alive { get; }
        string Name { get; }

        void UpdateGamePad(GameTime gameTime, GamePadInputHelper input);
    }
}
