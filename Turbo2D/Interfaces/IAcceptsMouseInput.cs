﻿using System;
using Microsoft.Xna.Framework;
using Turbo2D.Helpers;
using System.Collections.Generic;

namespace Turbo2D.Interfaces
{
    public interface IAcceptsMouseInput
    {
        bool Enabled { get; set; }
        bool Alive { get; }
        string Name { get; }
        string[] Viewports { get; set; }

        void UpdateMouse(GameTime gameTime, MouseInputHelper input);
    }
}
