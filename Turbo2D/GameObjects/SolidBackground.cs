﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Serialization;
using System;
using System.IO;

namespace Turbo2D.GameObjects
{
    public class SolidBackground : GameObject, IDrawableObject
    {
        public Color Color { get; set; }
        public float DrawLayer { get; set; }
        public bool Visible { get; set; }
        private string renderGroup;
        public string RenderGroup
        {
            get { return renderGroup; }
            set
            {
                if (RenderGroupChanged != null)
                    RenderGroupChanged(this, new RenderGroupChangedEventArgs(renderGroup, value));
                renderGroup = value;
            }
        }
        public event EventHandler<RenderGroupChangedEventArgs> RenderGroupChanged;

        public SolidBackground(GameScreen parent, string name, Color color)
            : base(parent, name)
        {
            Color = color;
            Visible = true;
            RenderGroup = "Default";
        }

        public override void LoadContent(ContentManager Content)
        {
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(Color);
            writer.Write(DrawLayer);
            writer.Write(Visible);
            writer.Write(renderGroup);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            Color = reader.ReadColor();
            DrawLayer = reader.ReadSingle();
            Visible = reader.ReadBoolean();
            renderGroup = reader.ReadString();
        }

        public override void Draw()
        {
            base.Draw();

            RenderDevice.DrawRectangle(new Rectangle(0, 0,
                (int)(Engine.CurrentViewport.Width), (int)(Engine.CurrentViewport.Height)),
                Color, DrawLayer);
        }
    }
}
