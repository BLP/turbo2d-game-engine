﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using Turbo2D.Serialization;
using System.IO;

namespace Turbo2D.GameObjects
{
    public class AnimatedSprite : Sprite, IUpdateableObject
    {
        protected Dictionary<string, SpriteAnimation> Animations = new Dictionary<string, SpriteAnimation>();
        //protected Rectangle PreviousFrame = new Rectangle();
        protected SpriteAnimation CurrentAnimation;
        // For use with automatic frame calculation
        // Vertical - Y
        protected string[] AnimationNames;
        // Horizontal - X
        protected int NumFramesPerAnimation;
        protected TimeSpan FrameTime;
        public event EventHandler AnimationChanged;
        public event EventHandler FrameChanged;
        public bool Enabled { get; set; }

        public AnimatedSprite(GameScreen parent, string name, string textureName, Dictionary<string, List<Rectangle>> animationFrames, 
            TimeSpan frameTime, DrawableGameObject parentObject = null)
            : this(parent, name, textureName, Vector2.Zero, animationFrames, frameTime, parentObject)
        {
        }

        public AnimatedSprite(GameScreen parent, string name, string textureName, Vector2 position, 
            Dictionary<string, List<Rectangle>> animationFrames, TimeSpan frameTime, DrawableGameObject parentObject = null)
            : this(parent, name, textureName, position, new Rectangle(-1, -1, -1, -1), Vector2.One, animationFrames, frameTime, parentObject)
        {
        }

        public AnimatedSprite(GameScreen parent, string name, string textureName, Vector2 position, Rectangle sourceRectangle, Vector2 scale,
            Dictionary<string, List<Rectangle>> animationFrames, TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, sourceRectangle, scale, parentObject)
        {
            foreach(string key in animationFrames.Keys)
            {
                Animations.Add(key, new SpriteAnimation(animationFrames[key], frameTime));
            }
            Enabled = true;
        }

        public AnimatedSprite(GameScreen parent, string name, string textureName, string[] animationNames, int numFramesPerAnimation, 
            TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, parentObject)
        {
            AnimationNames = animationNames;
            NumFramesPerAnimation = numFramesPerAnimation;
            FrameTime = frameTime;
            Enabled = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            /*Texture = Content.Load<Texture2D>(TextureName);
            if (SourceRectangle == new Rectangle(-1, -1, -1, -1))
                SourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);*/
            base.LoadContent(Content);

            if (AnimationNames != null)
            {
                for (int i = 0; i < AnimationNames.Length; i++)
                {
                    Rectangle sourceRect = new Rectangle(SourceRectangle.X, SourceRectangle.Y + i * (SourceRectangle.Height / 
                        AnimationNames.Length), SourceRectangle.Width, SourceRectangle.Height / AnimationNames.Length);
                    Animations.Add(AnimationNames[i], new SpriteAnimation(sourceRect, NumFramesPerAnimation, FrameTime));
                }
            }

            foreach (SpriteAnimation animation in Animations.Values)
                animation.FrameChangedEvent += new EventHandler(AnimationFrameChanged);
            CurrentAnimation = Animations[Animations.Keys.ElementAt<string>(0)];
            Origin = new Vector2(CurrentAnimation.CurrentFrame.Width / 2, CurrentAnimation.CurrentFrame.Height / 2);
        }

        void AnimationFrameChanged(object sender, EventArgs e)
        {
            if (FrameChanged != null)
                FrameChanged(this, new EventArgs());
            Origin = new Vector2(CurrentAnimation.CurrentFrame.Width / 2, CurrentAnimation.CurrentFrame.Height / 2);
        }

        public void PlayAnimation(string name, bool reverse)
        {
            if (Animations.ContainsKey(name))
            {
                CurrentAnimation = Animations[name];
                CurrentAnimation.Play(reverse);
                if (AnimationChanged != null)
                    AnimationChanged(this, new EventArgs());
                Origin = new Vector2(CurrentAnimation.CurrentFrame.Width / 2, CurrentAnimation.CurrentFrame.Height / 2);
            }
        }

        public void PlayAnimation(string name, bool reverse, int startingFrame)
        {
            if (Animations.ContainsKey(name))
            {
                CurrentAnimation = Animations[name];
                CurrentAnimation.Play(reverse, startingFrame);
                if (AnimationChanged != null)
                    AnimationChanged.Invoke(this, new EventArgs());
                Origin = new Vector2(CurrentAnimation.CurrentFrame.Width / 2, CurrentAnimation.CurrentFrame.Height / 2);
            }
        }

        public void PauseAnimation()
        {
            CurrentAnimation.Pause();
        }

        public void ResumeAnimation()
        {
            CurrentAnimation.Resume();
        }

        public void SetAnimationFrame(int frame)
        {
            CurrentAnimation.SetCurrentFrame(frame);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            CurrentAnimation.Update(gameTime);
        }

        public override void Draw()
        {
            base.Draw();

            RenderDevice.DrawTexture(Texture, ActualPosition, CurrentAnimation.CurrentFrame, TintColor, ActualRotation, ActualOrigin,
                ActualScale, Effects, ActualDrawLayer);
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(Animations.Count);
            foreach (string key in Animations.Keys)
            {
                writer.Write(key);
                SpriteAnimation.Save(writer, Animations[key]);
            }


            SpriteAnimation.Save(writer, CurrentAnimation);

            writer.Write(Enabled);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            int numAnimations = reader.ReadInt32();
            for (int i = 0; i < numAnimations; i++)
                Animations.Add(reader.ReadString(), SpriteAnimation.Load(reader));

            CurrentAnimation = SpriteAnimation.Load(reader);

            Enabled = reader.ReadBoolean();
        }
    }
}
