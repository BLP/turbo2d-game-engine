﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;

namespace Turbo2D.GameObjects
{
    public class CollidableSprite : Sprite, ICollidable
    {
        public Rectangle CollisionRectangle
        {
            get
            {
                return CollisionHelper.CalculateBoundingRectangle(new Rectangle(0, 0, SourceRectangle.Width, SourceRectangle.Height),
                    Transformation);
            }
        }
        //protected List<Rectangle> PartRectangles = new List<Rectangle>();
        //public List<Rectangle> TransformedPartRectangles { get; protected set; }
        public Matrix Transformation { get { return CollisionHelper.CreateMatrix(ActualOrigin, ActualRotation, ActualPosition); } }
        public Color[] CollisionColors { get; protected set; }
        public bool Collidable { get; protected set; }
        public int TextureWidth { get { return SourceRectangle.Width; } }
        public int TextureHeight { get { return SourceRectangle.Height; } }
        //public Type ObjectType { get { return this.GetType(); } }

        public CollidableSprite(GameScreen parent, string name, string textureName, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public CollidableSprite(GameScreen parent, string name, string textureName, Vector2 position, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public CollidableSprite(GameScreen parent, string name, string textureName, Vector2 position, Rectangle sourceRectangle, 
            Vector2 scale, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, sourceRectangle, scale, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

            CollisionColors = new Color[TextureWidth * TextureHeight];
            Texture.GetData<Color>(0, SourceRectangle, CollisionColors, 0, CollisionColors.Length);
        }

        public virtual void CollisionOccurred(GameObject collidedWith)
        {
        }
    }
}
