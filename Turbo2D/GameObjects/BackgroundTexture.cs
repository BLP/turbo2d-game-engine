﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Serialization;
using System;
using System.IO;

namespace Turbo2D.GameObjects
{
    public enum BackgroundTextureModes
    {
        Center,
        Stretch,
        Tile,
        TopLeft
    }

    public class BackgroundTexture : GameObject, IDrawableObject
    {
        public bool Visible { get; set; }
        public BackgroundTextureModes DrawMode { get; set; }
        public float Alpha { get; set; }
        public float DrawLayer { get; set; }
        protected string TextureName;
        public Texture2D Texture { get; protected set; }
        private string renderGroup;
        public string RenderGroup
        {
            get { return renderGroup; }
            set
            {
                if (RenderGroupChanged != null)
                    RenderGroupChanged(this, new RenderGroupChangedEventArgs(renderGroup, value));
                renderGroup = value;
            }
        }
        public event EventHandler<RenderGroupChangedEventArgs> RenderGroupChanged;

        public BackgroundTexture(GameScreen parent, string name, string textureName, BackgroundTextureModes mode)
            : base(parent, name)
        {
            TextureName = textureName;
            DrawMode = mode;
            Alpha = 1.0f;
            Visible = true;
            renderGroup = "Default";
        }

        public override void LoadContent(ContentManager Content)
        {
            Texture = Content.Load<Texture2D>(TextureName);
        }

        public override void Draw()
        {
            base.Draw();

            Vector2 position;
            switch (DrawMode)
            {
                case BackgroundTextureModes.Center:
                    position = new Vector2((Engine.CurrentViewport.Width) / 2, (Engine.CurrentViewport.Height) / 2);
                    RenderDevice.DrawTexture(Texture, position, null, Color.White * Alpha, 0f, 
                        new Vector2(Texture.Width / 2, Texture.Height / 2), 1f, SpriteEffects.None, DrawLayer);
                    break;
                case BackgroundTextureModes.Stretch:
                    Rectangle rect = new Rectangle(0, 0, (int)(Engine.CurrentViewport.Width),
                        (int)(Engine.CurrentViewport.Height));
                    RenderDevice.DrawTexture(Texture, rect, null, Color.White * Alpha, 0f, Vector2.Zero,
                        SpriteEffects.None, DrawLayer);
                    break;
                case BackgroundTextureModes.Tile:
                    position = Vector2.Zero;
                    while (position.X < Engine.CurrentViewport.Width)
                    {
                        while (position.Y < Engine.CurrentViewport.Height)
                        {
                            RenderDevice.DrawTexture(Texture, position, null, Color.White * Alpha, 0f, 
                                Vector2.Zero, 1f, SpriteEffects.None, DrawLayer);
                            position.Y += Texture.Height;
                        }
                        position.X += Texture.Width;
                        position.Y = 0;
                    }
                    break;
                case BackgroundTextureModes.TopLeft:
                    RenderDevice.DrawTexture(Texture, Vector2.Zero, null, Color.White * Alpha, 0f, Vector2.Zero, 1f, SpriteEffects.None, DrawLayer);
                    break;
            }
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(TextureName);
            writer.Write(Visible);
            writer.Write(DrawMode);
            writer.Write(Alpha);
            writer.Write(DrawLayer);
            writer.Write(renderGroup);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            TextureName = reader.ReadString();
            Visible = reader.ReadBoolean();
            DrawMode = reader.ReadEnum<BackgroundTextureModes>();
            Alpha = reader.ReadSingle();
            DrawLayer = reader.ReadSingle();
            renderGroup = reader.ReadString();
        }
    }
}
