﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Helpers;
using Turbo2D.Serialization;
using System.IO;

namespace Turbo2D.GameObjects
{
    public abstract class DrawableGameObject : GameObject, IDrawableObject
    {
        public bool Visible { get; set; } // Whether this GameObject should be drawn
        public Vector2 Position { get; set; } // The position of the GameObject
        public Vector2 ActualPosition { get { return GetPosition(); } }
        private Matrix DrawTransform
        {
            get
            {
                return Matrix.CreateTranslation(new Vector3(-Origin, 0)) * Matrix.CreateScale(new Vector3(Scale, 1)) *
                    Matrix.CreateRotationZ(Rotation) * Matrix.CreateTranslation(new Vector3(Position, 0)) * (ParentObject == null ?
                    Matrix.Identity : ParentObject.DrawTransform);
            }
        }
        public Vector2 Origin { get; set; } // The "anchor point" of the GameObject
        public Vector2 ActualOrigin { get { return Vector2.Zero; } }
        public float Rotation { get; set; } // The rotation of the GameObject
        public float ActualRotation { get { return GetRotation(); } }
        public Vector2 Scale { get; set; } // The scale of the GameObject
        public Vector2 ActualScale { get { return GetScale(); } }
        public float DrawLayer { get; set; } // The GameObject's depth on the screen
        public float ActualDrawLayer { get { return ParentObject == null ? DrawLayer : ParentObject.ActualDrawLayer - ((1f - DrawLayer) / (float)Math.Pow(10, ComponentLayer)); } }
        private string renderGroup; // The GameObject's RenderGroup name
        public string RenderGroup
        {
            get { return ParentObject == null ? renderGroup : ParentObject.RenderGroup; }
            set
            {
                if (RenderGroupChanged != null)
                    RenderGroupChanged(this, new RenderGroupChangedEventArgs(renderGroup, value));
                renderGroup = value;

                // If this accepts mouse input, update the viewports
                if (this is IAcceptsMouseInput)
                    UpdateViewports();
            }
        }
        public event EventHandler<RenderGroupChangedEventArgs> RenderGroupChanged; // An event when the RenderGoup name is changed

        public new DrawableGameObject ParentObject { get { return base.ParentObject as DrawableGameObject; } } // The GameObject that owns this one

        public DrawableGameObject(GameScreen parent, string name, DrawableGameObject parentObject = null)
            : base(parent, name, parentObject)
        {
            Scale = Vector2.One;
            Visible = true;
            RenderGroup = "Default";
        }

        private Vector2 GetPosition()
        {
            Vector3 scale;
            Quaternion rotation;
            Vector3 translation;
            DrawTransform.Decompose(out scale, out rotation, out translation);

            return new Vector2(translation.X, translation.Y);
        }

        private Vector2 GetScale()
        {
            Vector3 scale;
            Quaternion rotation;
            Vector3 translation;
            DrawTransform.Decompose(out scale, out rotation, out translation);

            return new Vector2(scale.X, scale.Y);
        }

        private float GetRotation()
        {
            Vector3 scale;
            Quaternion rotation;
            Vector3 translation;
            DrawTransform.Decompose(out scale, out rotation, out translation);

            Vector2 direction = Vector2.Transform(Vector2.UnitX, rotation);

            return (float)Math.Atan2(direction.Y, direction.X);
        }

        private void UpdateViewports()
        {
            IAcceptsMouseInput asMouse = this as IAcceptsMouseInput;

            // Clear out the viewports
            asMouse.Viewports = Parent.Viewports.GetViewportNames(RenderGroup);

            /*foreach (RenderViewport viewport in Parent.Viewports)
            {
                if (viewport.RenderGroups.Contains(this.RenderGroup))
                    asMouse.Viewports.Add(viewport.Name);
            }*/
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(Visible);
            writer.Write(Position);
            writer.Write(Origin);
            writer.Write(Rotation);
            writer.Write(Scale);
            writer.Write(DrawLayer);
            writer.Write(renderGroup);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            Visible = reader.ReadBoolean();
            Position = reader.ReadVector2();
            Origin = reader.ReadVector2();
            Rotation = reader.ReadSingle();
            Scale = reader.ReadVector2();
            DrawLayer = reader.ReadSingle();
            renderGroup = reader.ReadString();
        }
    }
}
