﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using Turbo2D.Serialization;
using System.ComponentModel;

namespace Turbo2D.GameObjects.GUI
{
    /// <summary>
    /// An enumeration to indicate which side of a menu a pointer should appear on
    /// </summary>
    public enum PointerAlignment
    {
        Left,
        Right
    }

    /// <summary>
    /// Arguments for when a menu's select event is fired
    /// </summary>
    public class MenuSelectEventArgs : EventArgs
    {
        /// <summary>
        /// The text of the currently selected item
        /// </summary>
        public string CurrentItem { get; protected set; }

        /// <summary>
        /// Creates a new instance of MenuSelectedEventArgs
        /// </summary>
        /// <param name="currentItem">The text of the currently selected item</param>
        public MenuSelectEventArgs(string currentItem)
        {
            CurrentItem = currentItem;
        }
    }

    /// <summary>
    /// Arguments for when a menu's scroll event is fired
    /// </summary>
    public class MenuScrollEventArgs : EventArgs
    {
        /// <summary>
        /// The text of the previously selected item
        /// </summary>
        public string PreviousItem { get; protected set; }
        /// <summary>
        /// The text of the currently selected item
        /// </summary>
        public string CurrentItem { get; protected set; }
        /// <summary>
        /// The index of the previously selected item
        /// </summary>
        public int PreviousIndex { get; protected set; }
        /// <summary>
        /// The index of the currently selected item
        /// </summary>
        public int CurrentIndex { get; protected set; }

        /// <summary>
        /// Creates a new instance of MenuScrollEventArgs
        /// </summary>
        /// <param name="previousItem">The text of the previously selected item</param>
        /// <param name="currentItem">The text of the currently selected item</param>
        /// <param name="previousIndex">The index of the previously selected item</param>
        /// <param name="currentIndex">The index of the currently selected item</param>
        public MenuScrollEventArgs(string previousItem, string currentItem, int previousIndex, int currentIndex)
        {
            PreviousItem = previousItem;
            CurrentItem = currentItem;
            PreviousIndex = previousIndex;
            CurrentIndex = currentIndex;
        }
    }

    /// <summary>
    /// A pointer to indicate the selected item in a menu
    /// </summary>
    public class MenuPointer : Sprite, IAcceptsKeyboardInput
    {
        /// <summary>
        /// The index of the currently selected item
        /// </summary>
        public int SelectedIndex { get; set; }
        /// <summary>
        /// The currently selected item
        /// </summary>
        protected MenuItem SelectedItem { get { return MenuItems[SelectedIndex]; } }
        /// <summary>
        /// The pointer's alignment relative to the menu
        /// </summary>
        public PointerAlignment Alignment { get; set; }
        /// <summary>
        /// The text of the currently selected item
        /// </summary>
        [Browsable(false)]
        public string SelectedText { get { return SelectedItem.Text; } }
        /// <summary>
        /// The menu items in the pointer's menu
        /// </summary>
        [Browsable(false)]
        public List<MenuItem> MenuItems { get { return Menu.Items; } }
        /// <summary>
        /// The key used to move the pointer down one item
        /// </summary>
        public Keys ScrollDown { get; set; }
        /// <summary>
        /// The key used to move the pointer up one item
        /// </summary>
        public Keys ScrollUp { get; set; }
        /// <summary>
        /// The key used to select an item with the pointer
        /// </summary>
        public Keys Select { get; set; }

        /// <summary>
        /// The distance, in pixels, between a pointer and the menu items
        /// </summary>
        public const float BufferRoom = 3f;

        protected TextPointerMenu Menu;
        /// <summary>
        /// Whether the pointer should be updated
        /// </summary>
        public bool Enabled { get; set; }
        /// <summary>
        /// Fired when the pointer selects an item
        /// </summary>
        public event EventHandler<MenuSelectEventArgs> SelectEvent;
        /// <summary>
        /// Fired when the pointer scrolls its position
        /// </summary>
        public event EventHandler<MenuScrollEventArgs> Scroll;

        /// <summary>
        /// Creates a new instance of a MenuPointer
        /// </summary>
        /// <param name="name">The pointer's name</param>
        /// <param name="textureName">The path to the pointer's texture</param>
        /// <param name="alignment">The pointer's alignment relative to the menu</param>
        /// <param name="menu">The menu that has this pointer</param>
        public MenuPointer(string name, string textureName, PointerAlignment alignment, TextPointerMenu menu)
            : this(name, textureName, alignment, Keys.Down, Keys.Up, Keys.Enter, menu)
        {
        }

        /// <summary>
        /// Creates a new instance of a MenuPointer
        /// </summary>
        /// <param name="name">The pointer's name</param>
        /// <param name="textureName">The path to the pointer's texture</param>
        /// <param name="alignment">The pointer's alignment relative to the menu</param>
        /// <param name="scrollDown">The key used to scroll the pointer down the menu</param>
        /// <param name="scrollUp">The key used to scroll the pointer up the menu</param>
        /// <param name="select">The key used to make a selection with the pointer</param>
        /// <param name="menu">The menu that has this pointer</param>
        public MenuPointer(string name, string textureName, PointerAlignment alignment, Keys scrollDown, Keys scrollUp,
            Keys select, TextPointerMenu menu)
            : base(menu.Parent, name, textureName, menu)
        {
            SelectedIndex = 0;
            Menu = menu;
            Alignment = alignment;
            ScrollDown = scrollDown;
            ScrollUp = scrollUp;
            Select = select;
            Enabled = true;
        }

        /// <summary>
        /// Loads the pointer's content
        /// </summary>
        /// <param name="Content">The ContentManager to use to load content</param>
        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

            UpdatePosition();
        }

        /// <summary>
        /// Updates the pointer's position
        /// </summary>
        public void UpdatePosition()
        {
            switch (Alignment)
            {
                case PointerAlignment.Left:
                    Position = new Vector2(SelectedItem.Position.X - SelectedItem.Origin.X - BufferRoom, SelectedItem.Position.Y +
                            SelectedItem.TextSize.Y / 2);
                    Origin = new Vector2(Texture.Width, Texture.Height / 2);
                    break;
                case PointerAlignment.Right:
                    Position = new Vector2(SelectedItem.Position.X + (SelectedItem.TextSize.X - SelectedItem.Origin.X) + BufferRoom,
                            SelectedItem.Position.Y + SelectedItem.TextSize.Y / 2);
                    Origin = new Vector2(0, Texture.Height / 2);
                    break;
            }
        }

        /// <summary>
        /// Updates the pointer based on keyboard input
        /// </summary>
        /// <param name="gameTime">The amount of time passed since the last update</param>
        /// <param name="input">The helper to get keyboard input from</param>
        public override void UpdateKeyboard(GameTime gameTime, KeyboardInputHelper input)
        {
            base.UpdateKeyboard(gameTime, input);
            if (input.KeyPressed(ScrollDown))
            {
                string previousItem = SelectedItem.Text;
                int previousIndex = SelectedIndex;
                SelectedIndex++;
                if (SelectedIndex >= MenuItems.Count)
                    SelectedIndex = 0;
                else if (SelectedIndex < 0)
                    SelectedIndex = MenuItems.Count - 1;

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedItem.Text, previousIndex, SelectedIndex));
            }
            else if (input.KeyPressed(ScrollUp))
            {
                string previousItem = SelectedItem.Text;
                int previousIndex = SelectedIndex;
                SelectedIndex--;
                if (SelectedIndex >= MenuItems.Count)
                    SelectedIndex = 0;
                else if (SelectedIndex < 0)
                    SelectedIndex = MenuItems.Count - 1;

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedItem.Text, previousIndex, SelectedIndex));
            }
            if (input.KeyPressed(Select))
            {
                if (SelectEvent != null)
                    SelectEvent.Invoke(this, new MenuSelectEventArgs(SelectedText));
            }
            UpdatePosition();
        }

        /// <summary>
        /// Draws the pointer
        /// </summary>
        public override void Draw()
        {
            if (Enabled)
                TintColor = Color.White;
            else
                TintColor = Color.Gray;
            base.Draw();
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(SelectedIndex);
            writer.Write(Alignment);
            writer.Write(ScrollDown);
            writer.Write(ScrollUp);
            writer.Write(Select);
            writer.Write(Enabled);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            SelectedIndex = reader.ReadInt32();
            Alignment = reader.ReadEnum<PointerAlignment>();
            ScrollDown = reader.ReadEnum<Keys>();
            ScrollUp = reader.ReadEnum<Keys>();
            Select = reader.ReadEnum<Keys>();
            Enabled = reader.ReadBoolean();
        }
    }
}
