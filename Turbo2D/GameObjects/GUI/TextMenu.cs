﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.GameObjects;
using Turbo2D.Helpers;
using Turbo2D.Serialization;

namespace Turbo2D.GameObjects.GUI
{
    public class TextMenu : Menu, IAcceptsKeyboardInput
    {
        public int SelectedIndex { get; set; }
        public TextMenuItem SelectedItem { get { return Items[SelectedIndex] as TextMenuItem; } }
        public string SelectedText { get { return Items[SelectedIndex].Text; } }
        protected Keys ScrollUp;
        protected Keys ScrollDown;
        protected Keys Select;
        public event EventHandler<MenuScrollEventArgs> Scroll;
        public event EventHandler<MenuSelectEventArgs> SelectEvent;

        public TextMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, DrawableGameObject parentObject = null)
            : this(parent, name, fontName, menuSize, Keys.Down, Keys.Up, Keys.Enter, parentObject)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, Keys scrollDown, Keys scrollUp, Keys select,
            DrawableGameObject parentObject = null)
            : this(parent, name, fontName, menuSize, scrollDown, scrollUp, select, MenuAlignments.Center, parentObject)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, Keys scrollDown, Keys scrollUp, Keys select,
            MenuAlignments alignment, DrawableGameObject parentObject = null)
            : this(parent, name, fontName, menuSize, scrollDown, scrollUp, select, alignment, 1f, 5f, parentObject)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, Keys scrollDown, Keys scrollUp, Keys select,
            MenuAlignments alignment, float verticalSpacing, float horizontalSpacing, DrawableGameObject parentObject = null)
            : base(parent, name, fontName, menuSize, alignment, verticalSpacing, horizontalSpacing, parentObject)
        {
            ScrollUp = scrollUp;
            ScrollDown = scrollDown;
            Select = select;
        }

        public void AddMenuItem(string text, Color color, Color selectedColor)
        {
            TextMenuItem item = new TextMenuItem("", text, FontName, color, selectedColor, this);
            AddMenuItem(item);
        }

        /*protected class ColumnInfo
        {
            public List<TextMenuItem> Items = new List<TextMenuItem>();
            public float Width = 0;
            public float WidestIndex = 0;
            public int Page;

            public ColumnInfo()
            {
            }

            public void AddItem(TextMenuItem item)
            {
                Items.Add(item);
                if (item.TextSize.X > Width)
                    Width = item.TextSize.X;
                WidestIndex = Items.Count - 1;
            }
        }

        protected class ColumnPair
        {
            public ColumnInfo Column1;
            public ColumnInfo Column2;
            public float HighestAvWidth;
            public float HighestAvWidthIndex;

            public ColumnPair(ColumnInfo column1, ColumnInfo column2)
            {
                Column1 = column1;
                Column2 = column2;
                for (int i = 0; i < Column1.Items.Count; i++)
                {
                    if (i < Column2.Items.Count)
                    {
                        float av = (Column1.Items[i].TextSize.X + Column2.Items[i].TextSize.X) / 2;
                        if (av > HighestAvWidth)
                        {
                            av = HighestAvWidth;
                            HighestAvWidthIndex = i;
                        }
                    }
                }
            }
        }*/

        /*protected void UpdateItemPositions()
        {
            ArrItems.Clear();
            List<ColumnInfo> temp = new List<ColumnInfo>();
            temp.Add(new ColumnInfo());
            Vector2 nextPos = new Vector2(MenuArea.X, MenuArea.Y);
            for (int i = 0; i < Items.Count; i++)
            {
                if (nextPos.Y + Items[i].TextSize.Y > MenuArea.Bottom)
                {
                    temp.Add(new ColumnInfo());
                    nextPos.Y = MenuArea.Y;
                }
                Items[i].Position = nextPos;
                temp[temp.Count - 1].AddItem(Items[i]);
                nextPos.Y += Items[i].TextSize.Y + VerticalSpacing;
            }

            ArrItems.Add(new List<TextMenuItem>());
            switch (Alignment)
            {
                case MenuAlignments.Left:
                    float prevX;
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X, item.Position.Y);
                    temp[0].Page = 1;
                    prevX = MenuArea.X;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += temp[i - 1].Width + HorizontalSpacing;
                        if (prevX + temp[i].Width  > MenuArea.Right)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X;
                        }
                        temp[i].Page = ArrItems.Count;
                        foreach (TextMenuItem item in temp[i].Items)
                            item.Position = new Vector2(prevX, item.Position.Y);
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    break;
                case MenuAlignments.Center:
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X  + temp[0].Width / 2, item.Position.Y);
                    prevX = MenuArea.X  + temp[0].Width / 2;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += HorizontalSpacing + temp[i - 1].Width / 2 + temp[i].Width / 2;
                        if (prevX + temp[i].Width / 2 > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X + temp[i].Width / 2;
                        }
                        foreach (TextMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int minColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = (MenuArea.Right - (temp[0].Items[0].Position.X + temp[0].Width / 2)) / 2;
                        }
                        else
                        {
                            for (int column = minColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = (MenuArea.Right - (temp[column].Items[0].Position.X + temp[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }

                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = (MenuArea.Right - (temp[column - 1].Items[0].Position.X + temp[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }
                    break;
                case MenuAlignments.Right:
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X  + temp[0].Width, item.Position.Y);
                    prevX = MenuArea.X + temp[0].Width;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += HorizontalSpacing + temp[i].Width;
                        if (prevX > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X + temp[i].Width;
                        }
                        foreach (TextMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int prevColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = MenuArea.Right - (temp[0].Items[0].Position.X);
                        }
                        else
                        {
                            for (int column = prevColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = MenuArea.Right - (temp[column].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = MenuArea.Right - (temp[column - 1].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }

                    break;
            }

            CurrentPage = 0;
        }*/

        public override void UpdateKeyboard(GameTime gameTime, KeyboardInputHelper input)
        {
            base.UpdateKeyboard(gameTime, input);

            if (input.KeyPressed(ScrollDown))
            {
                string previousItem = SelectedText;
                int previousIndex = SelectedIndex;
                SelectedIndex++;
                if (SelectedIndex >= Items.Count)
                    SelectedIndex = 0;
                if (!ArrItems[CurrentPage].Contains(SelectedItem))
                {
                    int newPage = CurrentPage + 1;
                    if (newPage >= ArrItems.Count)
                        CurrentPage = 0;
                    else
                        CurrentPage = newPage;
                }

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedText, previousIndex, SelectedIndex));
            }
            if (input.KeyPressed(ScrollUp))
            {
                string previousItem = SelectedText;
                int previousIndex = SelectedIndex;
                SelectedIndex--;
                if (SelectedIndex < 0)
                    SelectedIndex = Items.Count - 1;
                if (!ArrItems[CurrentPage].Contains(SelectedItem))
                {
                    int newPage = CurrentPage - 1;
                    if (newPage < 0)
                        CurrentPage = ArrItems.Count - 1;
                    else
                        CurrentPage = newPage;
                }

                if (Scroll != null)
                    Scroll(this, new MenuScrollEventArgs(previousItem, SelectedText, previousIndex, SelectedIndex));
            }
            if (input.KeyPressed(Select))
            {
                if (SelectEvent != null)
                    SelectEvent(this, new MenuSelectEventArgs(SelectedText));
            }
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(SelectedIndex);
            writer.Write(ScrollUp);
            writer.Write(ScrollDown);
            writer.Write(Select);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            SelectedIndex = reader.ReadInt32();
            ScrollUp = reader.ReadEnum<Keys>();
            ScrollDown = reader.ReadEnum<Keys>();
            Select = reader.ReadEnum<Keys>();
        }
    }
}
