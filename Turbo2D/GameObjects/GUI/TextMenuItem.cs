﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.ComponentModel;
using Turbo2D.Serialization;
using System.IO;

namespace Turbo2D.GameObjects.GUI
{
    public class TextMenuItem : MenuItem
    {
        public Color SelectedColor { get; set; }
        public Color StandardColor { get; set; }
        [Browsable(false)]
        public new TextMenu Menu { get { return base.Menu as TextMenu; } }
        protected bool Selected { get { return Menu.SelectedItem == this; } }

        public TextMenuItem(string name, string text, string fontName, Color standardColor, Color selectedColor, TextMenu menu)
            : base(name, text, fontName, standardColor, menu)
        {
            StandardColor = standardColor;
            SelectedColor = selectedColor;
        }

        public override void Draw()
        {
            if (Selected)
                Color = SelectedColor;
            else
                Color = StandardColor;
            base.Draw();
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(SelectedColor);
            writer.Write(StandardColor);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            SelectedColor = reader.ReadColor();
            StandardColor = reader.ReadColor();
        }
    }
}
