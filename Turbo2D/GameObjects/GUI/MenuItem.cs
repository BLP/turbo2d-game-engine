﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;
using Microsoft.Xna.Framework.Content;

namespace Turbo2D.GameObjects.GUI
{
    /// <summary>
    /// An item to be displayed in a menu
    /// </summary>
    public class MenuItem : TextItem
    {
        /// <summary>
        /// The menu that contains this item
        /// </summary>
        public Menu Menu { get { return ParentObject as Menu; } }

        /// <summary>
        /// Creates a new instance of a MenuItem
        /// </summary>
        /// <param name="name">The name of the item</param>
        /// <param name="text">The item's text</param>
        /// <param name="fontName">The path of the SpriteFont file to use for the item</param>
        /// <param name="color">The item's color</param>
        /// <param name="menu">The menu that contains this item</param>
        public MenuItem(string name, string text, string fontName, Color color, Menu menu)
            : base(menu.Parent, name, fontName, text, color, menu)
        {
        }

        /// <summary>
        /// Loads the item's content
        /// </summary>
        /// <param name="Content">The ContentManager to use to load content</param>
        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

            switch (Menu.Alignment)
            {
                case MenuAlignments.Left:
                    Origin = Vector2.Zero;
                    break;
                case MenuAlignments.Center:
                    Origin = new Vector2(TextSize.X / 2, 0);
                    break;
                case MenuAlignments.Right:
                    Origin = new Vector2(TextSize.X, 0);
                    break;
            }
        }
    }
}
