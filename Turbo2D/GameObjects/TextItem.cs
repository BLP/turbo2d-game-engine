﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Serialization;
using System.IO;

namespace Turbo2D.GameObjects
{
    public class TextItem : DrawableGameObject
    {
        private string FontName;
        public SpriteFont Font { get; set; }
        public Color Color { get; set; }
        public string Text { get; set; }
        public SpriteEffects Effects { get; set; }
        public Vector2 TextSize { get { return Font.MeasureString(Text); } }

        public TextItem(GameScreen parent, string name, string fontName, string text, Color color, DrawableGameObject parentObject = null)
            : this(parent, name, fontName, text, color, Vector2.Zero, parentObject)
        {
        }

        public TextItem(GameScreen parent, string name, string fontName, string text, Color color, Vector2 position, 
            DrawableGameObject parentObject = null)
            : base(parent, name, parentObject)
        {
            FontName = fontName;
            Text = text;
            Position = position;
            Color = color;
        }

        public override void LoadContent(ContentManager Content)
        {
            Font = Content.Load<SpriteFont>(FontName);
        }

        public override void Draw()
        {
            base.Draw();
            
            RenderDevice.DrawText(Font, Text, ActualPosition, Color, ActualRotation, ActualOrigin, ActualScale, Effects, ActualDrawLayer);
        }

        protected override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);

            writer.Write(FontName);
            writer.Write(Color);
            writer.Write(Text);
            writer.Write(Effects);
        }

        protected override void LoadData(BinaryReader reader)
        {
            base.LoadData(reader);

            FontName = reader.ReadString();
            Color = reader.ReadColor();
            Text = reader.ReadString();
            Effects = reader.ReadEnum<SpriteEffects>();
        }
    }
}
