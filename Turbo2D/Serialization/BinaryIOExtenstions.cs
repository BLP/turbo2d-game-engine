﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.Serialization
{
    public static class BinaryIOExtenstions
    {
        /// <summary>
        /// Writes all four components of a Color struct to the current stream
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="color">The Color struct to write</param>
        public static void Write(this BinaryWriter writer, Color color)
        {
            writer.Write(color.R);
            writer.Write(color.G);
            writer.Write(color.B);
            writer.Write(color.A);
        }

        /// <summary>
        /// Reads a Color struct from the current stream. The struct is stored as four bytes, one for each component
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Color ReadColor(this BinaryReader reader)
        {
            return new Color(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte());
        }

        /// <summary>
        /// Writes a TimeSpan to the current stream
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="time">The TimeSpan to write</param>
        public static void Write(this BinaryWriter writer, TimeSpan time)
        {
            writer.Write(time.Ticks);
        }

        /// <summary>
        /// Reads a TimeSpan from the current stream. The TimeSpan is stored as a number of ticks.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static TimeSpan ReadTimeSpan(this BinaryReader reader)
        {
            return TimeSpan.FromTicks(reader.ReadInt64());
        }

        /// <summary>
        /// Writes a Viewport to the current stream
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="viewport">The Viewport to write</param>
        public static void Write(this BinaryWriter writer, Viewport viewport)
        {
            writer.Write(viewport.X);
            writer.Write(viewport.Y);
            writer.Write(viewport.Width);
            writer.Write(viewport.Height);
        }

        /// <summary>
        /// Reads a Viewport from the current stream. The Viewport is stored using its position and size data
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="defaultViewport">The BaseEngine's default viewport</param>
        /// <returns></returns>
        public static Viewport ReadViewport(this BinaryReader reader, Viewport defaultViewport)
        {
            Viewport newViewport = defaultViewport;

            newViewport.X = reader.ReadInt32();
            newViewport.Y = reader.ReadInt32();
            newViewport.Width = reader.ReadInt32();
            newViewport.Height = reader.ReadInt32();

            return newViewport;
        }

        /// <summary>
        /// Writers a Vector2 to the current stream
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="vector">The Vector2 to write</param>
        public static void Write(this BinaryWriter writer, Vector2 vector)
        {
            writer.Write(vector.X);
            writer.Write(vector.Y);
        }

        /// <summary>
        /// Reads a Vector2 from the current stream. The Vector2 is stored as two floats.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Vector2 ReadVector2(this BinaryReader reader)
        {
            return new Vector2(reader.ReadSingle(), reader.ReadSingle());
        }

        /// <summary>
        /// Writes a Rectangle to the current stream.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="rect">The Rectangle to write</param>
        public static void Write(this BinaryWriter writer, Rectangle rect)
        {
            writer.Write(rect.X);
            writer.Write(rect.Y);
            writer.Write(rect.Width);
            writer.Write(rect.Height);
        }

        /// <summary>
        /// Reads a Rectangle from the current stream. The rectangle is stored as four integers.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Rectangle ReadRectangle(this BinaryReader reader)
        {
            return new Rectangle(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
        }

        /// <summary>
        /// Writes an Enum to the current stream
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value">The Enum to write</param>
        public static void Write(this BinaryWriter writer, Enum value)
        {
            writer.Write(value.ToString());
        }

        /// <summary>
        /// Reads an Enum from the current stream. The Enum is stored as a string representation
        /// </summary>
        /// <typeparam name="T">The Enum type to read</typeparam>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static T ReadEnum<T>(this BinaryReader reader) where T : struct, IConvertible
        {
            return (T)Enum.Parse(typeof(T), reader.ReadString()); 
        }
    }
}
