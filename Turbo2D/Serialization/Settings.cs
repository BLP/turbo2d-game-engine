﻿using System;
using System.IO;
using Microsoft.Xna.Framework;

namespace Turbo2D.Serialization
{
    public class Settings
    {
        public TimeSpan Game_InactiveSleepTime { get; set; }
        public bool Game_IsFixedTimeStep { get; set; }
        public bool Game_IsMouseVisible { get; set; }
        public TimeSpan Game_TargetElapsedTime { get; set; }
        public bool Graphics_SychronizeWithVerticleRetrace { get; set; }
        public bool Graphics_IsFullScreen { get; set; }
        public int Graphics_PreferredBackBufferWidth { get; set; }
        public int Graphics_PreferredBackBufferHeight { get; set; }
        public string Window_Title { get; set; }
        public bool Window_AllowUserResizing { get; set; }
        public string Content_RootDirectory { get; set; }
        public Color DefaultBackgroundColor { get; set; }
        
        public Settings()
        {
        }

        public static TimeSpan TargetElapsedTimeFromFPS(int framesPerSecond)
        {
            return TimeSpan.FromSeconds(1 / framesPerSecond);
        }

        public static Settings CreateDefault()
        {
            Settings settings = new Settings();
            settings.Content_RootDirectory = "Content";
            settings.Game_InactiveSleepTime = new TimeSpan(0, 0, 0, 0, 20);
            settings.Game_IsFixedTimeStep = true;
            settings.Game_IsMouseVisible = false;
            settings.Game_TargetElapsedTime = new TimeSpan(166667);
            settings.Graphics_SychronizeWithVerticleRetrace = true;
            settings.Graphics_IsFullScreen = false;
            settings.Graphics_PreferredBackBufferHeight = 600;
            settings.Graphics_PreferredBackBufferWidth = 800;
            settings.Window_AllowUserResizing = false;
            settings.Window_Title = "Turbo2D Application";
            settings.DefaultBackgroundColor = Color.CornflowerBlue;
            return settings;
        }

        public static Settings Load(string FileName)
        {
            Settings settings = new Settings();
            if (File.Exists(FileName))
            {
                BinaryReader binReader = new BinaryReader(File.Open(FileName, FileMode.Open));
                try
                {
                    settings.Content_RootDirectory = binReader.ReadString();
                    settings.Game_InactiveSleepTime = binReader.ReadTimeSpan();
                    settings.Game_IsFixedTimeStep = binReader.ReadBoolean();
                    settings.Game_IsMouseVisible = binReader.ReadBoolean();
                    settings.Game_TargetElapsedTime = binReader.ReadTimeSpan();
                    settings.Graphics_IsFullScreen = binReader.ReadBoolean();
                    settings.Graphics_PreferredBackBufferHeight = binReader.ReadInt32();
                    settings.Graphics_PreferredBackBufferWidth = binReader.ReadInt32();
                    settings.Graphics_SychronizeWithVerticleRetrace = binReader.ReadBoolean();
                    settings.Window_AllowUserResizing = binReader.ReadBoolean();
                    settings.Window_Title = binReader.ReadString();
                    settings.DefaultBackgroundColor = binReader.ReadColor();
                }
                catch
                {
                    settings = CreateDefault();
                }
                binReader.Close();
            }
            else
                settings = CreateDefault();
            return settings;
        }

        public static void Save(Settings settings, string FileName)
        {
            using (BinaryWriter binWriter = new BinaryWriter(File.Open(FileName, FileMode.Create)))
            {
                binWriter.Write(settings.Content_RootDirectory);
                binWriter.Write(settings.Game_InactiveSleepTime);
                binWriter.Write(settings.Game_IsFixedTimeStep);
                binWriter.Write(settings.Game_IsMouseVisible);
                binWriter.Write(settings.Game_TargetElapsedTime);
                binWriter.Write(settings.Graphics_IsFullScreen);
                binWriter.Write(settings.Graphics_PreferredBackBufferHeight);
                binWriter.Write(settings.Graphics_PreferredBackBufferWidth);
                binWriter.Write(settings.Graphics_SychronizeWithVerticleRetrace);
                binWriter.Write(settings.Window_AllowUserResizing);
                binWriter.Write(settings.Window_Title);
                binWriter.Write(settings.DefaultBackgroundColor);
                binWriter.Close();
            }
        }
    }
}
