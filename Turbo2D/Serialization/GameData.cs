﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turbo2D.Serialization
{
    public class GameData
    {
        public string Name { get; set; }
        public string SettingsFile { get; set; }
        public string FirstScreen { get; set; }
        public List<string> Screens { get; set; }

        public GameData()
        {
            Screens = new List<string>();
        }

        public static GameData CreateDefault()
        {
            GameData data = new GameData();
            data.Name = "";
            data.SettingsFile = "";
            data.FirstScreen = "";
            data.Screens = new List<string>();

            return data;
        }

        public static GameData Load(string FileName)
        {
            GameData data = new GameData();
            if (File.Exists(FileName))
            {
                BinaryReader binReader = new BinaryReader(File.Open(FileName, FileMode.Open));
                try
                {
                    data.Name = binReader.ReadString();
                    data.SettingsFile = binReader.ReadString();
                    data.FirstScreen = binReader.ReadString();

                    while (binReader.PeekChar() != -1)
                        data.Screens.Add(binReader.ReadString());
                }
                catch
                {
                    data = CreateDefault();
                }
                binReader.Close();
            }
            else
                data = CreateDefault();

            return data;
        }

        public static void Save(GameData data, string fileName)
        {
            using (BinaryWriter binWriter = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                binWriter.Write(data.Name);
                binWriter.Write(data.SettingsFile);
                binWriter.Write(data.FirstScreen);

                foreach (string screen in data.Screens)
                    binWriter.Write(screen);

                binWriter.Close();
            }
        }
    }
}
