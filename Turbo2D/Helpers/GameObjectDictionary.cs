﻿using System;
using System.Collections.Generic;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Managers;

namespace Turbo2D.Helpers
{
    public sealed class GameObjectDictionary
    {
        public List<IUpdateableObject> UpdateableObjects { get; private set; }
        public List<IAcceptsInput> InputObjects { get; private set; }
        public List<IDisposableObject> DisposableObjects { get; private set; }
        public Dictionary<string, RenderGroup> DrawableObjects { get; private set; }
        //public event EventHandler<GameObjectEventArgs> ObjectAdded;
        //public event EventHandler<GameObjectEventArgs> ObjectRemoved;
        private Dictionary<string, GameObject> GameObjects = new Dictionary<string, GameObject>();
        private Dictionary<Type, List<ManagerBase>> Managers = new Dictionary<Type, List<ManagerBase>>();
        public List<ManagerBase> ObjectManagers { get; private set; }
        private GameScreen Screen;

        public GameObjectDictionary(GameScreen screen)
            : base()
        {
            UpdateableObjects = new List<IUpdateableObject>();
            InputObjects = new List<IAcceptsInput>();
            DisposableObjects = new List<IDisposableObject>();
            DrawableObjects = new Dictionary<string, RenderGroup>();
            ObjectManagers = new List<ManagerBase>();
            Screen = screen;
        }

        public void Add(ManagerBase manager)
        {
            if (Managers.ContainsKey(manager.ManagedType))
                Managers[manager.ManagedType].Add(manager);
            else
            {
                Managers.Add(manager.ManagedType, new List<ManagerBase>());
                Managers[manager.ManagedType].Add(manager);
            }
            ObjectManagers.Add(manager);
        }

        public void Add(GameObject value)
        {
            foreach (string key in DrawableObjects.Keys)
            {
                Add(value, key);
                break;
            }
        }

        public void Add(GameObject value, string renderGroupName)
        {
            GameObjects.Add(value.Name, value);

            if (value is IUpdateableObject)
                UpdateableObjects.Add(value as IUpdateableObject);
            if (value is IAcceptsInput)
                InputObjects.Add(value as IAcceptsInput);
            if (value is IDisposableObject)
                DisposableObjects.Add(value as IDisposableObject);
            if (value is IDrawableObject)
            {
                if (DrawableObjects.ContainsKey(renderGroupName))
                    DrawableObjects[renderGroupName].AddObject(value as IDrawableObject);
                else
                {
                    throw new Exception("Render group with name " + renderGroupName + " does not exist.");
                }
            }

            value.LoadContent(Screen.Engine.Content);

            Type currentType = value.GetType();
            Type[] interfaces = currentType.GetInterfaces();
            bool originalType = true;
            while (!(currentType == typeof(object)))
            {
                //ObjectAdded.Invoke(this, new GameObjectEventArgs(value));
                if (Managers.ContainsKey(currentType))
                {
                    foreach (ManagerBase manager in Managers[currentType])
                        if (originalType || manager.ManageInheritedTypes)
                            manager.Add(value);
                }
                currentType = currentType.BaseType;
                originalType = false;
            }

            foreach (Type tInterface in interfaces)
            {
                if (Managers.ContainsKey(tInterface))
                {
                    foreach (ManagerBase manager in Managers[tInterface])
                        manager.Add(value);
                }
                    
            }
        }

        /// <summary>
        /// Creates a new RenderGroup for drawing.
        /// </summary>
        /// <param name="groupName">The name of the RenderGroup.</param>
        /// <param name="cameraAffected">If the objects drawn in this RenderGroup are affected by cameras.</param>
        /// <param name="drawOrder">The position in the draw order where this RenderGroup should be drawn.
        /// The higher the index, the earlier in the draw order.</param>
        public void AddRenderGroup(string groupName, bool cameraAffected, int drawOrder)
        {
            DrawableObjects.Add(groupName, new RenderGroup(cameraAffected, drawOrder));
        }

        public void Remove(ManagerBase manager)
        {
            Managers[manager.ManagedType].Remove(manager);
            ObjectManagers.Remove(manager);
        }

        public void Remove(string key)
        {
            GameObject gObject = GameObjects[key];
            if (UpdateableObjects.Contains(gObject as IUpdateableObject))
                UpdateableObjects.Remove(gObject as IUpdateableObject);
            if (InputObjects.Contains(gObject as IAcceptsInput))
                InputObjects.Remove(gObject as IAcceptsInput);
            if (DisposableObjects.Contains(gObject as IDisposableObject))
                DisposableObjects.Remove(gObject as IDisposableObject);
            foreach (string group in DrawableObjects.Keys)
                DrawableObjects[group].RemoveObject(gObject as IDrawableObject);
            if (gObject is IDisposableObject)
                (gObject as IDisposableObject).Dispose();
            GameObjects.Remove(key);

            /*if (ObjectRemoved != null)
                ObjectRemoved.Invoke(this, new GameObjectEventArgs(gObject));*/
            Type currentType = gObject.GetType();
            Type[] interfaces = currentType.GetInterfaces();
            bool originalType = true;
            while (!(currentType == typeof(object)))
            {
                //ObjectAdded.Invoke(this, new GameObjectEventArgs(value));
                if (Managers.ContainsKey(currentType))
                {
                    foreach (ManagerBase manager in Managers[currentType])
                        if (originalType || manager.ManageInheritedTypes)
                            manager.Remove(gObject);
                }
                currentType = currentType.BaseType;
                originalType = false;
            }

            foreach (Type tInterface in interfaces)
            {
                if (Managers.ContainsKey(tInterface))
                {
                    foreach (ManagerBase manager in Managers[tInterface])
                        manager.Remove(gObject);
                }

            }
        }

        public bool Contains(string key)
        {
            return GameObjects.ContainsKey(key);
        }

        public bool Contains(GameObject gameObject)
        {
            return GameObjects.ContainsValue(gameObject);
        }

        public T Get<T>(string name) where T : GameObject
        {
            if (GameObjects.ContainsKey(name))
            {
                return (GameObjects[name] as T);
            }
            else
                throw new KeyNotFoundException();
        }

        public List<T> GetAll<T>() where T : GameObject
        {
            List<T> matches = new List<T>();
            foreach (GameObject gObject in GameObjects.Values)
            {
                if (gObject is T)
                    matches.Add(gObject as T);
            }
            return matches;
        }
    }

    public class GameObjectEventArgs : EventArgs
    {
        public GameObject Object { get; protected set; }

        public GameObjectEventArgs(GameObject gameObject)
        {
            Object = gameObject;
        }
    }
}
