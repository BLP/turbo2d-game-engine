﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameScreens;
using Microsoft.Xna.Framework;
using Turbo2D.Interfaces;

namespace Turbo2D.Helpers
{
    public sealed class KeyboardInputHelper
    {
        public KeyboardState OldKeyboardState { get; private set; }
        public KeyboardState NewKeyboardState { get; private set; }
        public Dictionary<Keys, TimeSpan> KeyHoldTimes { get; private set; }

        //protected List<IAcceptsKeyboardInput> KeyboardObjects = new List<IAcceptsKeyboardInput>();
        //private GameScreen Screen;

        internal KeyboardInputHelper(/*GameScreen screen*/)
        {
            //Screen = screen;
            OldKeyboardState = Keyboard.GetState();
            KeyHoldTimes = new Dictionary<Keys, TimeSpan>();
            foreach (int key in Enum.GetValues(typeof(Keys)))
            {
                KeyHoldTimes.Add((Keys)key, TimeSpan.Zero);
            }
        }

        internal void Update(GameTime gameTime)
        {
            OldKeyboardState = NewKeyboardState;

            NewKeyboardState = Keyboard.GetState();

            foreach (Keys key in NewKeyboardState.GetPressedKeys())
            {
                if (KeyHeld(key))
                    KeyHoldTimes[key] += gameTime.ElapsedGameTime;
            }

            foreach (Keys key in OldKeyboardState.GetPressedKeys())
            {
                if (KeyReleased(key))
                    KeyHoldTimes[key] = TimeSpan.Zero;
            }
        }

        /*public void UpdateKeyboardObjects(GameTime gameTime)
        {
            for (int i = 0; i < KeyboardObjects.Count; i++)
            {
                if (KeyboardObjects[i].Enabled && KeyboardObjects[i].HasFocus)
                {
                    KeyboardObjects[i].UpdateKeyboard(gameTime, this);
                    if (!KeyboardObjects[i].Alive)
                        Screen.GameObjects.Remove(KeyboardObjects[i].Name);
                }
            }
        }*/

        public bool KeyPressed(Keys key)
        {
            return (NewKeyboardState.IsKeyDown(key) && OldKeyboardState.IsKeyUp(key));
        }

        public bool KeyReleased(Keys key)
        {
            return (NewKeyboardState.IsKeyUp(key) && OldKeyboardState.IsKeyDown(key));
        }

        public bool KeyHeld(Keys key)
        {
            return (NewKeyboardState.IsKeyDown(key) && OldKeyboardState.IsKeyDown(key));
        }

        /*public void RegisterKeyboardObject(IAcceptsKeyboardInput keyboardObject)
        {
            KeyboardObjects.Add(keyboardObject);
        }*/

        /*public void DeregisterKeyboardObject(IAcceptsKeyboardInput keyboardObject)
        {
            // Remove it from the list of clickable objects
            KeyboardObjects.Remove(keyboardObject);
        }*/
    }
}
