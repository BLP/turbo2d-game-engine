﻿using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;
using Turbo2D.Serialization;
using System.ComponentModel;
using System.IO;
using System;
using System.Reflection;

namespace Turbo2D.Helpers
{
    public class Camera2D
    {
        public Vector2 Position { get; set; }
        public float Rotation { get; set; }
        public float Scale { get; set; }
        public Rectangle CameraBounds { get; set; }
        public Vector2 RotationBounds { get; set; }
        public bool WrapRotation { get; set; }
        public Vector2 ScaleBounds { get; set; }
        public Vector2 CameraOrigin { get; set; }
        public string Name { get; private set; }
        /*public Rectangle CameraView
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, Engine.CurrentViewport.Width,
                    Engine.CurrentViewport.Height);
            }
        }*/
        [Browsable(false)]
        protected GameScreen Parent { get; private set; }
        [Browsable(false)]
        protected BaseEngine Engine { get; private set; }
        [Browsable(false)]
        public Matrix Transformation { get { return CreateCameraMatrix(CameraOrigin, Rotation, Position, Scale); } }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin, string name)
            : this(parent, cameraBounds, cameraOrigin, new Vector2(0f, MathHelper.TwoPi), name)
        {
        }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin, Vector2 rotationBounds, string name)
            : this(parent, cameraBounds, cameraOrigin, rotationBounds, new Vector2(0.1f, 2f), name)
        {
        }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin, Vector2 rotationBounds, Vector2 scaleBounds, string name)
        {
            Parent = parent;
            Engine = parent.Engine;
            CameraBounds = cameraBounds;
            CameraOrigin = cameraOrigin;
            Rotation = 0f;
            RotationBounds = rotationBounds;
            WrapRotation = true;
            Scale = 1f;
            ScaleBounds = scaleBounds;
            Name = name;
        }

        protected internal virtual void Update(GameTime gameTime)
        {
            Position = Vector2.Max(new Vector2(CameraBounds.Left, CameraBounds.Top), Position);
            Position = Vector2.Min(new Vector2(CameraBounds.Right, CameraBounds.Bottom), Position);
            if (WrapRotation)
            {
                if (Rotation > RotationBounds.Y)
                {
                    Rotation = RotationBounds.X + (Rotation - RotationBounds.Y);
                }
                else if (Rotation < RotationBounds.X)
                {
                    Rotation = RotationBounds.Y + (Rotation - RotationBounds.X);
                }
            }
            else
            {
                Rotation = MathHelper.Clamp(Rotation, RotationBounds.X, RotationBounds.Y);
            }
            Scale = MathHelper.Clamp(Scale, ScaleBounds.X, ScaleBounds.Y);
        }

        protected static Matrix CreateCameraMatrix(Vector2 origin, float rotation, Vector2 position, float scale)
        {
            return Matrix.CreateTranslation(new Vector3(-position, 0)) * 
                Matrix.CreateRotationZ(-rotation) *      
                Matrix.CreateScale(new Vector3(scale, scale, 1)) * 
                Matrix.CreateTranslation(new Vector3(origin, 0));
        }

        protected virtual void SaveData(BinaryWriter writer)
        {
            writer.Write(CameraBounds);
            writer.Write(CameraOrigin);
            writer.Write(RotationBounds);
            writer.Write(ScaleBounds);
            writer.Write(WrapRotation);
            writer.Write(Position);
            writer.Write(Rotation);
            writer.Write(Scale);
            writer.Write(Name);
        }

        protected virtual void LoadData(BinaryReader reader)
        {
            CameraBounds = reader.ReadRectangle();
            CameraOrigin = reader.ReadVector2();
            RotationBounds = reader.ReadVector2();
            ScaleBounds = reader.ReadVector2();
            WrapRotation = reader.ReadBoolean();
            Position = reader.ReadVector2();
            Rotation = reader.ReadSingle();
            Scale = reader.ReadSingle();
            Name = reader.ReadString();
        }

        internal static void Save(BinaryWriter writer, Camera2D camera)
        {
            writer.Write(camera.GetType().AssemblyQualifiedName);
            camera.SaveData(writer);
        }

        internal static Camera2D Load(BinaryReader reader, GameScreen parent)
        {
            Type type = Type.GetType(reader.ReadString());
            ConstructorInfo[] info = type.GetConstructors();
            ParameterInfo[] parameters = info[0].GetParameters();
            object[] args = new object[parameters.Length];

            for (int i = 0; i < parameters.Length; i++)
                if (parameters[i].ParameterType == typeof(GameScreen))
                    args[i] = parent;
            Camera2D camera = (Camera2D)info[0].Invoke(args);

            camera.LoadData(reader);

            return camera;
        }
    }
}
