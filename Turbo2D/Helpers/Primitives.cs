﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.Helpers
{
    public static class Primitives
    {
        private static Texture2D Square;

        internal static void Initialize(GraphicsDevice gDevice)
        {
            Square = new Texture2D(gDevice, 1, 1, false, SurfaceFormat.Color);
            Square.SetData<Color>(new Color[] { Color.White });
        }

        public static void DrawRectangle(SpriteBatch batch, Rectangle rect, Color color)
        {
            batch.Draw(Square, rect, color);
        }

        public static void DrawRectangle(SpriteBatch batch, Rectangle rect, Color color, float drawLayer)
        {
            batch.Draw(Square, rect, null, color, 0f, Vector2.Zero, SpriteEffects.None, drawLayer);
        }

        public static void DrawRectangle(SpriteBatch batch, Rectangle rect, Color color, Vector2 origin, float rotation, float drawLayer)
        {
            batch.Draw(Square, rect, null, color, rotation, origin, SpriteEffects.None, drawLayer);
        }

        public static void DrawRectangle(SpriteBatch batch, Vector2 position, float Width, float Height, Color color, Vector2 origin,
            float rotation, Vector2 scale, float drawLayer)
        {
            Vector2 totalScale = scale * new Vector2(Width, Height);
            batch.Draw(Square, position, null, color, rotation, origin / totalScale, totalScale, SpriteEffects.None, 
                drawLayer);
        }

        public static void DrawLine(SpriteBatch batch, Vector2 startPoint, Vector2 endPoint, Color color, float drawLayer)
        {
            float distance = Vector2.Distance(startPoint, endPoint);
            float angle = (float)Math.Atan2((double)(endPoint.Y - startPoint.Y), (double)(endPoint.X - startPoint.X));
            batch.Draw(Square, startPoint, null, color, angle, Vector2.Zero, new Vector2(distance, 1), SpriteEffects.None, drawLayer);
        }
    }
}
