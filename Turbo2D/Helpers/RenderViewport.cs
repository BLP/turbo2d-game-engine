﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using Turbo2D.Serialization;
using Turbo2D.GameScreens;
using System.ComponentModel;

namespace Turbo2D.Helpers
{
    public sealed class RenderViewport
    {
        public Viewport Viewport { get; private set; }
        public List<string> RenderGroups { get; private set; }
        [Browsable(false)]
        public CameraManager Cameras { get; private set; }
        public string Name { get; private set; }

        internal RenderViewport(Viewport viewport, string name)//, Renderer renderer)
            : this(viewport, name, new string[] { "Default" })//, renderer)
        {
        }

        /// <summary>
        /// Creates a new RenderViewport
        /// </summary>
        /// <param name="viewport">The Viewport to use for drawing</param>
        /// <param name="renderGroups">The RenderGroups to draw in the Viewport</param>
        /// <param name="renderer">The Renderer to user for drawing</param>
        internal RenderViewport(Viewport viewport, string name, string[] renderGroups)//, Renderer renderer)
        {
            Viewport = viewport;
            RenderGroups = new List<string>(renderGroups);
            Cameras = new CameraManager();
            Name = name;
        }

        /// <summary>
        /// Updates the RenderViewport's cameras
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Update</param>
        internal void UpdateCameras(GameTime gameTime)
        {
            // Update the CameraManager
            Cameras.Update(gameTime);
        }

        internal static void Save(BinaryWriter writer, RenderViewport viewport)
        {
            writer.Write(viewport.Viewport);
            writer.Write(viewport.Name);
            writer.Write(viewport.RenderGroups.Count);
            foreach (string groupName in viewport.RenderGroups)
                writer.Write(groupName);

            CameraManager.Save(writer, viewport.Cameras);
        }

        internal static RenderViewport Load(BinaryReader reader, GameScreen parent)
        {
            RenderViewport viewport = new RenderViewport(reader.ReadViewport(parent.Engine.DefaultViewport), reader.ReadString());

            int numGroups = reader.ReadInt32();
            viewport.RenderGroups.Clear();
            for (int i = 0; i < numGroups; i++)
                viewport.RenderGroups.Add(reader.ReadString());

            viewport.Cameras = CameraManager.Load(reader, parent);
            return viewport;
        }
    }
}
