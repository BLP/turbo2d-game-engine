﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace Turbo2D.Helpers
{
    public sealed class GamePadInputHelper
    {
        /// <summary>
        /// The gamepad states from the last update call
        /// </summary>
        public Dictionary<PlayerIndex, GamePadState> OldGamePadStates { get; private set; }
        /// <summary>
        /// The gamepad states from the current update call
        /// </summary>
        public Dictionary<PlayerIndex, GamePadState> NewGamePadStates { get; private set; }

        /// <summary>
        /// The capabilities for each gamepad
        /// </summary>
        public Dictionary<PlayerIndex, GamePadCapabilities> Capabilities { get; private set; }

        internal GamePadInputHelper()
        {
            // Create the arrays for gamepad states
            OldGamePadStates = new Dictionary<PlayerIndex, GamePadState>();
            NewGamePadStates = new Dictionary<PlayerIndex,GamePadState>();
            Capabilities = new Dictionary<PlayerIndex, GamePadCapabilities>();

            // Initialize old states and gamepad capabilities
            for (int i = 0; i < 4; i++)
            {
                OldGamePadStates[(PlayerIndex)i] = GamePad.GetState((PlayerIndex)i);
                Capabilities[(PlayerIndex)i] = GamePad.GetCapabilities((PlayerIndex)i);
            }
        }

        internal void Update(GameTime gameTime)
        {
            // Copy previous states to old states
            for (int i = 0; i < 4; i++)
                OldGamePadStates[(PlayerIndex)i] = NewGamePadStates[(PlayerIndex)i];

            // Get new gamepad states
            for (int i = 0; i < 4; i++)
            {
                NewGamePadStates[(PlayerIndex)i] = GamePad.GetState((PlayerIndex)i);
                
                // Refresh capabilities if connected state has changed
                if (NewGamePadStates[(PlayerIndex)i].IsConnected != OldGamePadStates[(PlayerIndex)i].IsConnected)
                    Capabilities[(PlayerIndex)i] = GamePad.GetCapabilities((PlayerIndex)i);
            }
        }

        /// <summary>
        /// Determines if a button was just pressed
        /// </summary>
        /// <param name="player">The controller index</param>
        /// <param name="button">The button to check</param>
        /// <returns>True if the button was just pressed, false otherwise</returns>
        public bool ButtonPressed(PlayerIndex player, Buttons button)
        {
            return OldGamePadStates[player].IsButtonUp(button) && NewGamePadStates[player].IsButtonDown(button);
        }

        /// <summary>
        /// Determines if a button was just released
        /// </summary>
        /// <param name="player">The controller index</param>
        /// <param name="button">The button to check</param>
        /// <returns>True if the button was just released, false otherwise</returns>
        public bool ButtonReleased(PlayerIndex player, Buttons button)
        {
            return OldGamePadStates[player].IsButtonDown(button) && NewGamePadStates[player].IsButtonUp(button);
        }

        /// <summary>
        /// Determines if a button is being held down
        /// </summary>
        /// <param name="player">The controller index</param>
        /// <param name="button">The button to check</param>
        /// <returns>True if the button is being held, false otherwise</returns>
        public bool ButtonHeld(PlayerIndex player, Buttons button)
        {
            return OldGamePadStates[player].IsButtonDown(button) && NewGamePadStates[player].IsButtonDown(button);
        }
    }
}
