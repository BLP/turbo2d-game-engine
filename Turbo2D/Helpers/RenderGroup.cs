﻿using System.Collections.Generic;
using Turbo2D.Interfaces;
using System.ComponentModel;
using System.IO;
using System;

namespace Turbo2D.Helpers
{
    public sealed class RenderGroup
    {
        internal List<IDrawableObject> DrawableObjects = new List<IDrawableObject>();
        public bool Visible { get; set; }
        public bool CameraAffected { get; set; }
        public string Name { get; private set; }
        public int DrawIndex 
        { 
            get { return pDrawIndex; } 
            set 
            { 
                pDrawIndex = value; 
                if (DrawIndexChanged != null) 
                    DrawIndexChanged(this, new EventArgs()); 
            } 
        }
        private int pDrawIndex;

        internal event EventHandler DrawIndexChanged;

        /// <summary>
        /// Creates a new RenderGroup.
        /// </summary>
        /// <param name="cameraAffected">If the objects drawn in this RenderGroup are affected by cameras.</param>
        /// <param name="drawIndex">The position in the draw order where this RenderGroup should be drawn.
        /// The higher the index, the earlier in the draw order.</param>
        internal RenderGroup(bool cameraAffected, int drawIndex, string name)
        {
            Visible = true;
            CameraAffected = cameraAffected;
            pDrawIndex = drawIndex;
            Name = name;
        }

        internal void AddObject(IDrawableObject Object)
        {
            //if (!DrawableObjects.Contains(Object))
            //{
                DrawableObjects.Add(Object);
            //}
        }

        internal void RemoveObject(IDrawableObject Object)
        {
            //if (DrawableObjects.Contains(Object))
                DrawableObjects.Remove(Object);
        }

        internal bool Contains(IDrawableObject Object)
        {
            return DrawableObjects.Contains(Object);
        }

        internal void Draw()
        {
            for (int i = 0; i < DrawableObjects.Count; i++)
            {
                if (DrawableObjects[i].Alive)
                {
                    if (DrawableObjects[i].Visible)
                        DrawableObjects[i].Draw();
                }
                else
                {
                    DrawableObjects.RemoveAt(i);
                    i--;
                }
            }
        }

        internal static void Save(BinaryWriter writer, RenderGroup group)
        {
            writer.Write(group.CameraAffected);
            writer.Write(group.pDrawIndex);
            writer.Write(group.Name);
            writer.Write(group.Visible);
        }

        internal static RenderGroup Load(BinaryReader reader)
        {
            RenderGroup group = new RenderGroup(reader.ReadBoolean(), reader.ReadInt32(), reader.ReadString());
            group.Visible = reader.ReadBoolean();

            return group;
        }
    }
}