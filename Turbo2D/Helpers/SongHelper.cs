﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;

namespace Turbo2D.Helpers
{
    public sealed class SongHelper
    {
        /// <summary>
        /// The currently active song
        /// </summary>
        public TurboSong ActiveSong { get; private set; }
        /// <summary>
        /// The Engine that owns this SongHelper
        /// </summary>
        public BaseEngine Engine { get; private set; }

        internal SongHelper(BaseEngine engine)
        {
            Engine = engine;
        }

        /// <summary>
        /// Sets the currently active song
        /// </summary>
        /// <param name="xnaSong">The song to make active</param>
        /// <param name="volume">The song's volume</param>
        public void SetSong(Song xnaSong, float volume)
        {
            if (ActiveSong != null)
            {
                ActiveSong.Stop();
                Engine.Activated -= ActiveSong.GainedFocus;
                Engine.Deactivated -= ActiveSong.LostFocus;
            }

            ActiveSong = new TurboSong(xnaSong, volume);
            Engine.Activated += ActiveSong.GainedFocus;
            Engine.Deactivated += ActiveSong.LostFocus;
        }

        internal void Update(GameTime gameTime)
        {
            // If there is an active song and it is fading, update its fade
            if (ActiveSong != null && ActiveSong.Fading)
                ActiveSong.UpdateFade(gameTime);
        }
    }
}
