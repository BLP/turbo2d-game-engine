﻿using System;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using System.Collections.Generic;
using System.Linq;
using Turbo2D.Collections;

namespace Turbo2D.Helpers
{
    public sealed class Renderer
    {
        private SpriteBatch Batch;
        private GameScreen Screen;
        private BaseEngine Engine;
        public bool DrawInProgress { get; private set; }
        public Viewport CurrentViewport { get; private set; }
        private GraphicsDevice GDevice;
        private Matrix FinalTransformation;
        public RenderGroupCollection RenderGroups { get; internal set; }

        internal Renderer(GameScreen screen, SpriteBatch batch)
        {
            Screen = screen;
            Batch = batch;
            Engine = screen.Engine;
            GDevice = Engine.GraphicsDevice;
            DrawInProgress = false;
            RenderGroups = new RenderGroupCollection();
        }

        internal void DrawViewport(RenderViewport viewport)
        {
            // Set the viewport on the GraphicsDevice
            SelectViewport(viewport.Viewport);

            // Loop through each RenderGroup, making sure the RenderGroup is visible and that it is supposed to draw in the current viewport

            foreach (RenderGroup group in RenderGroups.Where(obj => obj.Visible && viewport.RenderGroups.Contains(obj.Name)))
            {
                /*if (group.Visible && viewport.RenderGroups.Contains(group.Name))
                {*/
                    if (group.CameraAffected && viewport.Cameras.ActiveCamera != null)
                    {
                        // The RenderGroup responds to cameras and an active camera is set - begin drawing with a camera
                        BeginDraw(viewport.Cameras.ActiveCamera);
                    }
                    else
                        // Begin drawing without a camera
                        BeginDraw();
                    // Draw the RenderGroup
                    group.Draw();
                    // Finish drawing
                    EndDraw();
                //}
            }
        }

        private void SelectViewport(Viewport viewport)
        {
            if (!DrawInProgress)
            {
                GDevice.Viewport = viewport;
                CurrentViewport = viewport;
            }
            else
                throw new Exception("Cannot change viewport while draw is in progress.");
        }

        /// <summary>
        /// Begin a draw batch.
        /// </summary>
        private void BeginDraw()
        {
            DrawInProgress = true;
            Batch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, 
                Screen.Transformation);
            FinalTransformation = Screen.Transformation;
        }

        /// <summary>
        /// Begin a draw batch using a camera.
        /// </summary>
        /// <param name="camera">The active camera</param>
        private void BeginDraw(Camera2D camera)
        {
            DrawInProgress = true;
            Batch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null,
                Screen.Transformation * camera.Transformation);
            FinalTransformation = Screen.Transformation * camera.Transformation;
        }

        /// <summary>
        /// End a draw batch.
        /// </summary>
        private void EndDraw()
        {
            DrawInProgress = false;
            Batch.End();
        }

        private bool IsOnScreen(Rectangle objectSize, Vector2 position, Vector2 origin, float rotation, Vector2 scale)
        {
            Rectangle objectRectangle = CollisionHelper.CalculateBoundingRectangle(objectSize, CollisionHelper.CreateMatrix(origin, rotation,
                position, scale));
            Rectangle viewportRect = new Rectangle(0, 0, CurrentViewport.Width, CurrentViewport.Height);
            return viewportRect.Intersects(CollisionHelper.CalculateBoundingRectangle(objectRectangle, FinalTransformation));
        }

        // Drawing fuction section

        public void DrawTexture(Texture2D texture, Rectangle destinationRectangle, Color color)
        {
            if (IsOnScreen(new Rectangle(0, 0, destinationRectangle.Width, destinationRectangle.Height),
                new Vector2(destinationRectangle.X, destinationRectangle.Y), Vector2.Zero, 0f, Vector2.One))
            {
                Batch.Draw(texture, destinationRectangle, color * Screen.Alpha);
            }
        }

        public void DrawTexture(Texture2D texture, Vector2 position, Color color)
        {
            if (IsOnScreen(new Rectangle(0, 0, texture.Width, texture.Height), position, Vector2.Zero, 0f, Vector2.One))
            {
                Batch.Draw(texture, position, color * Screen.Alpha);
            }
        }

        public void DrawTexture(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color)
        {
            if (IsOnScreen(new Rectangle(0, 0, destinationRectangle.Width, destinationRectangle.Height), new Vector2(destinationRectangle.X,
                destinationRectangle.Y), Vector2.Zero, 0f, Vector2.One))
            {
                Batch.Draw(texture, destinationRectangle, sourceRectangle, color * Screen.Alpha);
            }
        }

        public void DrawTexture(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color)
        {
            if (IsOnScreen(new Rectangle(0, 0, sourceRectangle.HasValue ? sourceRectangle.Value.Width :
                texture.Width, sourceRectangle.HasValue ? sourceRectangle.Value.Height : texture.Height), position, Vector2.Zero, 0f,
                Vector2.One))
            {
                Batch.Draw(texture, position, sourceRectangle, color * Screen.Alpha);
            }
        }

        public void DrawTexture(Texture2D texture, Rectangle destinationRectangle, Rectangle? sourceRectangle, Color color, float rotation, 
            Vector2 origin, SpriteEffects effects, float layerDepth)
        {
            if (IsOnScreen(new Rectangle(0, 0, destinationRectangle.Width, destinationRectangle.Height), new Vector2(destinationRectangle.X,
                destinationRectangle.Y), origin, rotation, Vector2.One))
            {
                Batch.Draw(texture, destinationRectangle, sourceRectangle, color * Screen.Alpha, rotation, origin, effects, layerDepth);
            }
        }

        public void DrawTexture(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin,
            float scale, SpriteEffects effects, float layerDepth)
        {
            if (IsOnScreen(new Rectangle(0, 0, sourceRectangle.HasValue ? sourceRectangle.Value.Width : texture.Width,
                sourceRectangle.HasValue ? sourceRectangle.Value.Height : texture.Height), position, origin, rotation, new Vector2(scale)))
            {
                Batch.Draw(texture, position, sourceRectangle, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        public void DrawTexture(Texture2D texture, Vector2 position, Rectangle? sourceRectangle, Color color, float rotation, Vector2 origin, 
            Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            if (IsOnScreen(new Rectangle(0, 0, sourceRectangle.HasValue ? sourceRectangle.Value.Width : texture.Width,
                sourceRectangle.HasValue ? sourceRectangle.Value.Height : texture.Height), position, origin, rotation, scale))
            {
                Batch.Draw(texture, position, sourceRectangle, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        public void DrawText(SpriteFont spriteFont, string text, Vector2 position, Color color)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, Vector2.Zero, 0f, Vector2.One))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha);
            }
        }

        public void DrawText(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, Vector2.Zero, 0f, Vector2.One))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha);
            }
        }

        public void DrawText(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, float scale, 
            SpriteEffects effects, float layerDepth)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, origin, rotation, new Vector2(scale)))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        public void DrawText(SpriteFont spriteFont, string text, Vector2 position, Color color, float rotation, Vector2 origin, Vector2 scale,
            SpriteEffects effects, float layerDepth)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, origin, rotation, scale))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        public void DrawText(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color, float rotation, Vector2 origin, 
            float scale, SpriteEffects effects, float layerDepth)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, origin, rotation, new Vector2(scale)))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        public void DrawText(SpriteFont spriteFont, StringBuilder text, Vector2 position, Color color, float rotation, Vector2 origin, 
            Vector2 scale, SpriteEffects effects, float layerDepth)
        {
            Vector2 textSize = spriteFont.MeasureString(text);
            if (IsOnScreen(new Rectangle(0, 0, (int)textSize.X, (int)textSize.Y), position, origin, rotation, scale))
            {
                Batch.DrawString(spriteFont, text, position, color * Screen.Alpha, rotation, origin, scale, effects, layerDepth);
            }
        }

        // Primitive function section
        public void DrawRectangle(Rectangle rect, Color color)
        {
            if (IsOnScreen(new Rectangle(0, 0, rect.Width, rect.Height), new Vector2(rect.X, rect.Y), Vector2.Zero, 0f, Vector2.One))
            {
                Primitives.DrawRectangle(Batch, rect, color * Screen.Alpha);
            }
        }

        public void DrawRectangle(Rectangle rect, Color color, float drawLayer)
        {
            if (IsOnScreen(new Rectangle(0, 0, rect.Width, rect.Height), new Vector2(rect.X, rect.Y), Vector2.Zero, 0f, Vector2.One))
            {
                Primitives.DrawRectangle(Batch, rect, color * Screen.Alpha, drawLayer);
            }
        }

        public void DrawRectangle(Rectangle rect, Color color, Vector2 origin, float rotation, float drawLayer)
        {
            if (IsOnScreen(new Rectangle(0, 0, rect.Width, rect.Height), new Vector2(rect.X, rect.Y), origin, rotation, Vector2.One))
            {
                Primitives.DrawRectangle(Batch, rect, color * Screen.Alpha, origin, rotation, drawLayer);
            }
        }

        public void DrawRectangle(Vector2 position, float width, float height, Color color, Vector2 origin, float rotation, Vector2 scale, 
            float drawLayer)
        {
            Vector2 totalScale = scale * new Vector2(width, height);
            if (IsOnScreen(new Rectangle(0, 0, 1, 1), position, origin / totalScale, rotation, totalScale))
            {
                Primitives.DrawRectangle(Batch, position, width, height, color * Screen.Alpha, origin, rotation, scale, drawLayer);
            }
        }

        public void DrawLine(Vector2 startPoint, Vector2 endPoint, Color color, float drawLayer)
        {
            if (IsOnScreen(new Rectangle(0, 0, (int)Math.Abs(endPoint.X - startPoint.X), (int)Math.Abs(endPoint.Y - startPoint.Y)),
                Vector2.Min(startPoint, endPoint), Vector2.Zero, 0f, Vector2.One))
            {
                Primitives.DrawLine(Batch, startPoint, endPoint, color * Screen.Alpha, drawLayer);
            }
        }
    }
}