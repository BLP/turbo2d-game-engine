﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.IO;
using Turbo2D.GameScreens;
using System.ComponentModel;

namespace Turbo2D.Helpers
{
    public sealed class CameraManager : IEnumerable<Camera2D>
    {
        private Dictionary<string, Camera2D> Cameras = new Dictionary<string, Camera2D>();
        public string ActiveCameraName { get; set; }
        [Browsable(false)]
        public Camera2D ActiveCamera { get { return (Cameras.Count > 0 && !String.IsNullOrEmpty(ActiveCameraName) ? 
            Cameras[ActiveCameraName] : null); } }

        internal CameraManager()
        {
            ActiveCameraName = "";
        }

        public void AddCamera(Camera2D camera)
        {
            Cameras.Add(camera.Name, camera);
        }

        /// <summary>
        /// Sets the active camera in the CameraManager
        /// </summary>
        /// <param name="name">The name of the camera. Set to "" if you want no camera to be active.</param>
        public void SetActiveCamera(string name)
        {
            ActiveCameraName = name;
        }

        /// <summary>
        /// Updates the CameraManager
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Update</param>
        internal void Update(GameTime gameTime)
        {
            // Update the active camera
            if (ActiveCamera != null)
                ActiveCamera.Update(gameTime);
        }

        internal static void Save(BinaryWriter writer, CameraManager manager)
        {
            writer.Write(manager.ActiveCameraName);
            writer.Write(manager.Cameras.Count);

            foreach (string key in manager.Cameras.Keys)
            {
                writer.Write(key);
                Camera2D.Save(writer, manager.Cameras[key]);
            }
        }

        internal static CameraManager Load(BinaryReader reader, GameScreen parent)
        {
            CameraManager manager = new CameraManager();

            manager.ActiveCameraName = reader.ReadString();

            int numCameras = reader.ReadInt32();

            for (int i = 0; i < numCameras; i++)
            {
                manager.Cameras.Add(reader.ReadString(), Camera2D.Load(reader, parent));
            }

            return manager;
        }

        public IEnumerator<Camera2D> GetEnumerator()
        {
            return Cameras.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Cameras.Values.GetEnumerator();
        }
    }
}
