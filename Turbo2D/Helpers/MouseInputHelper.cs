﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;

namespace Turbo2D.Helpers
{
    public enum MouseButtons
    {
        LeftButton,
        RightButton,
        MiddleButton
    }

    public sealed class MouseInputHelper
    {
        public MouseState OldMouseState { get; private set; }
        public MouseState NewMouseState { get; private set; }

        public Vector2 NewMousePosition { get { return CalculateMouseCoords(new Vector2(NewMouseState.X, NewMouseState.Y)); } }
        public Vector2 OldMousePosition { get { return CalculateMouseCoords(new Vector2(OldMouseState.X, OldMouseState.Y)); } }
        public Vector2 MousePositionDelta { get { return NewMousePosition - OldMousePosition; } }

        public int MouseWheelDelta { get { return NewMouseState.ScrollWheelValue - OldMouseState.ScrollWheelValue; } }

        //protected List<IAcceptsMouseInput> ClickableObjects = new List<IAcceptsMouseInput>();
        private GameScreen Screen;
        private RenderViewport CurrentViewport = null;

        internal MouseInputHelper(GameScreen screen)
        {
            Screen = screen;
            OldMouseState = Mouse.GetState();
        }

        internal void Update(GameTime gameTime)
        {
            OldMouseState = NewMouseState;

            NewMouseState = Mouse.GetState();
        }

        private Vector2 CalculateMouseCoords(Vector2 rawCoords)
        {
            if (CurrentViewport == null)
                // There's no viewport or camera, so just transform based on the screen transformation
                return Vector2.Transform(rawCoords, Matrix.Invert(Screen.Transformation));
            else
            {
                // There is an active viewport. Adjust the rawCoords based on the viewport position, then transform based on
                // screen transformation and camera transformation

                // Calculate transformation
                Matrix transformation = Screen.Transformation * (CurrentViewport.Cameras.ActiveCamera == null ? Matrix.Identity :
                    CurrentViewport.Cameras.ActiveCamera.Transformation);

                // Return transformed coordinates
                return Vector2.Transform(rawCoords - new Vector2(CurrentViewport.Viewport.X, CurrentViewport.Viewport.Y), Matrix.Invert(transformation));
            }
        }

        // Sets the current viewport
        internal void SetCurrentViewport(RenderViewport viewport)
        {
            CurrentViewport = viewport;
        }
        /*public void UpdateClickableObjects(GameTime gameTime)
        {
            for (int i = 0; i < ClickableObjects.Count; i++)
            {
                if (ClickableObjects[i].Enabled && ClickableObjects[i].ClickableRectangle.Contains(NewMouseState.X, NewMouseState.Y))
                {
                    Screen.Engine.IsMouseVisible = ClickableObjects[i].MouseVisible;
                    ClickableObjects[i].UpdateMouse(gameTime, this);
                    if (!ClickableObjects[i].Alive)
                        Screen.GameObjects.Remove(ClickableObjects[i].Name);
                    break;
                }
            }
        }*/

        public bool MouseButtonPressed(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Pressed && OldMouseState.LeftButton == ButtonState.Released);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Pressed && OldMouseState.MiddleButton == ButtonState.Released);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Pressed && OldMouseState.RightButton == ButtonState.Released);
                default:
                    return false;
            }
        }

        public bool MouseButtonReleased(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Released && OldMouseState.LeftButton == ButtonState.Pressed);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Released && OldMouseState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Released && OldMouseState.RightButton == ButtonState.Pressed);
                default:
                    return false;
            }
        }

        public bool MouseButtonHeld(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Pressed && OldMouseState.LeftButton == ButtonState.Pressed);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Pressed && OldMouseState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Pressed && OldMouseState.RightButton == ButtonState.Pressed);
                default:
                    return false;
            }
        }

        public bool IsMouseOver(Rectangle area)
        {
            return area.Contains((int)NewMousePosition.X, (int)NewMousePosition.Y);
        }

        public bool MouseClicked(Rectangle area, MouseButtons button)
        {
            return IsMouseOver(area) && MouseButtonReleased(button);
        }

        public bool MouseClicking(Rectangle area, MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return IsMouseOver(area) && NewMouseState.LeftButton == ButtonState.Pressed;
                case MouseButtons.MiddleButton:
                    return IsMouseOver(area) && NewMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButtons.RightButton:
                    return IsMouseOver(area) && NewMouseState.RightButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public bool MouseEntered(Rectangle area)
        {
            return IsMouseOver(area) && !area.Contains((int)OldMousePosition.X, (int)OldMousePosition.Y);
        }

        public bool MouseLeft(Rectangle area)
        {
            return !IsMouseOver(area) && area.Contains((int)OldMousePosition.X, (int)OldMousePosition.Y);
        }

        /*public void RegisterClickable(IAcceptsMouseInput clickable)
        {
            int index = ClickableObjects.FindIndex(layer => clickable.Layer >= layer.Layer);
            if (index == -1)
            {
                // If no match was found, put it at the top
                ClickableObjects.Add(clickable);
            }
            else
            {
                // Otherwise, put insert it at the found location
                ClickableObjects.Insert(index, clickable);
            }
            /*for (int index = 0; index < ClickableLayers.Count; index++)
            {
                if (clickable.Layer >= ClickableLayers[index].Layer)
                {
                    ClickableLayers.Insert(index, clickable);
                    break;
                }
            }
            if (!ClickableLayers.Contains(clickable))
                ClickableLayers.Add(clickable);
        }

        public void DeregisterClickable(IAcceptsMouseInput clickable)
        {
            // Remove it from the list of clickable objects
            ClickableObjects.Remove(clickable);
        }*/
    }
}
