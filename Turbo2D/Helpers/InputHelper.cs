﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;

namespace Turbo2D.Helpers
{
    public enum MouseButtons
    {
        LeftButton,
        RightButton,
        MiddleButton
    }

    public class InputHelper
    {
        public KeyboardState OldKeyboardState { get; private set; }
        public MouseState OldMouseState { get; private set; }
        public KeyboardState NewKeyboardState { get; private set; }
        public MouseState NewMouseState { get; private set; }
        public Dictionary<Keys, TimeSpan> KeyHoldTimes { get; private set; }

        public Vector2 NewMousePosition { get { return Vector2.Transform(new Vector2(NewMouseState.X, NewMouseState.Y), Screen.Transformation); } }
        public Vector2 OldMousePosition { get { return Vector2.Transform(new Vector2(OldMouseState.X, OldMouseState.Y), Screen.Transformation); } }

        public int MouseWheelDelta { get { return NewMouseState.ScrollWheelValue - OldMouseState.ScrollWheelValue; } }

        protected List<IClickable> ClickableLayers = new List<IClickable>();
        protected GameScreen Screen;

        public InputHelper(GameScreen screen)
        {
            Screen = screen;
        }

        public void Initialize()
        {
            OldKeyboardState = Keyboard.GetState();
            OldMouseState = Mouse.GetState();
            KeyHoldTimes = new Dictionary<Keys, TimeSpan>();
            foreach (int key in Enum.GetValues(typeof(Keys)))
            {
                KeyHoldTimes.Add((Keys)key, TimeSpan.Zero);
            }
        }

        public void Update(GameTime gameTime)
        {
            OldKeyboardState = NewKeyboardState;
            OldMouseState = NewMouseState;

            NewMouseState = Mouse.GetState();
            NewKeyboardState = Keyboard.GetState();

            foreach (Keys key in NewKeyboardState.GetPressedKeys())
            {
                if (KeyHeld(key))
                    KeyHoldTimes[key] += gameTime.ElapsedGameTime;
            }

            foreach (Keys key in OldKeyboardState.GetPressedKeys())
            {
                if (KeyReleased(key))
                    KeyHoldTimes[key] = TimeSpan.Zero;
            }

            UpdateClickableLayers();
        }

        protected void UpdateClickableLayers()
        {
            List<IClickable> tempList = new List<IClickable>(ClickableLayers);
            for (int i = 0; i < tempList.Count; i++)
            {
                if (ClickableLayers[i].Visible && ClickableLayers[i].ClickableRectangle.Contains(NewMouseState.X, NewMouseState.Y))
                {
                    Screen.Engine.IsMouseVisible = ClickableLayers[i].MouseVisible;
                    ClickableLayers[i].MouseUpdate();
                    break;
                }
            }
        }

        public bool KeyPressed(Keys key)
        {
            return (NewKeyboardState.IsKeyDown(key) && OldKeyboardState.IsKeyUp(key));
        }

        public bool KeyReleased(Keys key)
        {
            return (NewKeyboardState.IsKeyUp(key) && OldKeyboardState.IsKeyDown(key));
        }

        public bool KeyHeld(Keys key)
        {
            return (NewKeyboardState.IsKeyDown(key) && OldKeyboardState.IsKeyDown(key));
        }

        public bool MouseButtonPressed(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Pressed && OldMouseState.LeftButton == ButtonState.Released);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Pressed && OldMouseState.MiddleButton == ButtonState.Released);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Pressed && OldMouseState.RightButton == ButtonState.Released);
                default:
                    return false;
            }
        }

        public bool MouseButtonReleased(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Released && OldMouseState.LeftButton == ButtonState.Pressed);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Released && OldMouseState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Released && OldMouseState.RightButton == ButtonState.Pressed);
                default:
                    return false;
            }
        }

        public bool MouseButtonHeld(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (NewMouseState.LeftButton == ButtonState.Pressed && OldMouseState.LeftButton == ButtonState.Pressed);
                case MouseButtons.MiddleButton:
                    return (NewMouseState.MiddleButton == ButtonState.Pressed && OldMouseState.MiddleButton == ButtonState.Pressed);
                case MouseButtons.RightButton:
                    return (NewMouseState.RightButton == ButtonState.Pressed && OldMouseState.RightButton == ButtonState.Pressed);
                default:
                    return false;
            }
        }

        public void RegisterClickable(IClickable clickable)
        {
            for (int index = 0; index < ClickableLayers.Count; index++)
            {
                if (clickable.Layer >= ClickableLayers[index].Layer)
                {
                    ClickableLayers.Insert(index, clickable);
                    break;
                }
            }
            if (!ClickableLayers.Contains(clickable))
                ClickableLayers.Add(clickable);
            clickable.KillEvent += new EventHandler(Clickable_KillEvent);
        }

        void Clickable_KillEvent(object sender, EventArgs e)
        {
            IClickable clickable = (sender as IClickable);
            ClickableLayers.Remove(clickable);
        }
    }
}
