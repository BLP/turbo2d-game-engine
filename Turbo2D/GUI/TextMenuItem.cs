﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.GUI
{
    public class TextMenuItem
    {
        public string Text { get; protected set; }
        public Color Color { get; protected set; }
        public Color SelectedColor { get; protected set; }
        public Vector2 Position { get; set; }
        public Vector2 Origin { get; protected set; }
        public TextMenu Menu { get; protected set; }
        public Vector2 TextSize { get { return Menu.Font.MeasureString(Text); } }
        protected bool Selected { get { return Menu.SelectedItem == this; } }

        public TextMenuItem(TextMenu menu, string text, Color color, Color selectedColor)
        {
            Menu = menu;
            Text = text;
            Color = color;
            SelectedColor = selectedColor;
        }

        public void LoadContent(ContentManager Content)
        {
            switch (Menu.Alignment)
            {
                case MenuAlignments.Left:
                    Origin = Vector2.Zero;
                    break;
                case MenuAlignments.Center:
                    Origin = new Vector2(TextSize.X / 2, 0);
                    break;
                case MenuAlignments.Right:
                    Origin = new Vector2(TextSize.X, 0);
                    break;
            }
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw()
        {
            Menu.RenderDevice.DrawText(Menu.Font, Text, Position, (Selected ? SelectedColor : Color), 0f, Origin, 1f, SpriteEffects.None, 
                Menu.DrawLayer);
        }
    }
}
