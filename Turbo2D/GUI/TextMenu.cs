﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.GameObjects;

namespace Turbo2D.GUI
{
    public class TextMenu : GameObject, IDrawableObject, IAcceptsInput
    {
        public Rectangle MenuArea { get; protected set; }
        protected List<List<TextMenuItem>> ArrItems = new List<List<TextMenuItem>>();
        public bool Visible { get; set; }
        public bool Enabled { get; set; }
        public float DrawLayer { get; set; }
        public List<TextMenuItem> Items { get; protected set; }
        public MenuAlignments Alignment { get; protected set; }
        public float VerticalSpacing { get; protected set; }
        public float HorizontalSpacing { get; protected set; }
        public SpriteFont Font { get; protected set; }
        protected string FontName;
        public int CurrentPage { get; set; }
        public int SelectedIndex { get; set; }
        public TextMenuItem SelectedItem { get { return Items[SelectedIndex]; } }
        public string SelectedText { get { return Items[SelectedIndex].Text; } }
        protected Keys ScrollUp;
        protected Keys ScrollDown;
        protected Keys Select;
        public event EventHandler<MenuScrollEventArgs> Scroll;
        public event EventHandler<MenuSelectEventArgs> SelectEvent;

        public TextMenu(GameScreen parent, string name, string fontName, Rectangle menuArea)
            :this(parent, name, fontName, menuArea, Keys.Down, Keys.Up, Keys.Enter)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Rectangle menuArea, Keys scrollDown, Keys scrollUp, Keys select)
            :this(parent, name, fontName, menuArea, scrollDown, scrollUp, select, MenuAlignments.Center)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Rectangle menuArea, Keys scrollDown, Keys scrollUp, Keys select,
            MenuAlignments alignment)
            : this(parent, name, fontName, menuArea, scrollDown, scrollUp, select, alignment, 5f, 1f)
        {
        }

        public TextMenu(GameScreen parent, string name, string fontName, Rectangle menuArea, Keys scrollDown, Keys scrollUp, Keys select,
            MenuAlignments alignment, float verticalSpacing, float horizontalSpacing)
            :base(parent, name)
        {
            MenuArea = menuArea;
            FontName = fontName;
            Items = new List<TextMenuItem>();
            VerticalSpacing = verticalSpacing;
            HorizontalSpacing = horizontalSpacing;
            Alignment = alignment;
            Visible = true;
            ScrollUp = scrollUp;
            ScrollDown = scrollDown;
            Select = select;
            Enabled = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            Font = Content.Load<SpriteFont>(FontName);

            for (int i = 0; i < Items.Count; i++)
            {
                Items[i].LoadContent(Content);
            }

            UpdateItemPositions();
        }

        public void AddMenuItem(string text, Color color, Color selectedColor)
        {
            TextMenuItem item = new TextMenuItem(this, text, color, selectedColor);
            AddMenuItem(item);
        }

        public void AddMenuItem(TextMenuItem item)
        {
            Items.Add(item);
        }

        protected class ColumnInfo
        {
            public List<TextMenuItem> Items = new List<TextMenuItem>();
            public float Width = 0;
            public float WidestIndex = 0;
            public int Page;

            public ColumnInfo()
            {
            }

            public void AddItem(TextMenuItem item)
            {
                Items.Add(item);
                if (item.TextSize.X > Width)
                    Width = item.TextSize.X;
                WidestIndex = Items.Count - 1;
            }
        }

        protected class ColumnPair
        {
            public ColumnInfo Column1;
            public ColumnInfo Column2;
            public float HighestAvWidth;
            public float HighestAvWidthIndex;

            public ColumnPair(ColumnInfo column1, ColumnInfo column2)
            {
                Column1 = column1;
                Column2 = column2;
                for (int i = 0; i < Column1.Items.Count; i++)
                {
                    if (i < Column2.Items.Count)
                    {
                        float av = (Column1.Items[i].TextSize.X + Column2.Items[i].TextSize.X) / 2;
                        if (av > HighestAvWidth)
                        {
                            av = HighestAvWidth;
                            HighestAvWidthIndex = i;
                        }
                    }
                }
            }
        }

        protected void UpdateItemPositions()
        {
            ArrItems.Clear();
            List<ColumnInfo> temp = new List<ColumnInfo>();
            temp.Add(new ColumnInfo());
            Vector2 nextPos = new Vector2(MenuArea.X, MenuArea.Y);
            for (int i = 0; i < Items.Count; i++)
            {
                if (nextPos.Y + Items[i].TextSize.Y > MenuArea.Bottom)
                {
                    temp.Add(new ColumnInfo());
                    nextPos.Y = MenuArea.Y;
                }
                Items[i].Position = nextPos;
                temp[temp.Count - 1].AddItem(Items[i]);
                nextPos.Y += Items[i].TextSize.Y + VerticalSpacing;
            }

            ArrItems.Add(new List<TextMenuItem>());
            switch (Alignment)
            {
                case MenuAlignments.Left:
                    float prevX;
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X, item.Position.Y);
                    temp[0].Page = 1;
                    prevX = MenuArea.X;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += temp[i - 1].Width + HorizontalSpacing;
                        if (prevX + temp[i].Width  > MenuArea.Right)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X;
                        }
                        temp[i].Page = ArrItems.Count;
                        foreach (TextMenuItem item in temp[i].Items)
                            item.Position = new Vector2(prevX, item.Position.Y);
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    break;
                case MenuAlignments.Center:
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X  + temp[0].Width / 2, item.Position.Y);
                    prevX = MenuArea.X  + temp[0].Width / 2;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += HorizontalSpacing + temp[i - 1].Width / 2 + temp[i].Width / 2;
                        if (prevX + temp[i].Width / 2 > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X + temp[i].Width / 2;
                        }
                        foreach (TextMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int minColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = (MenuArea.Right - (temp[0].Items[0].Position.X + temp[0].Width / 2)) / 2;
                        }
                        else
                        {
                            for (int column = minColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = (MenuArea.Right - (temp[column].Items[0].Position.X + temp[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }

                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = (MenuArea.Right - (temp[column - 1].Items[0].Position.X + temp[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }
                    break;
                case MenuAlignments.Right:
                    foreach (TextMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X  + temp[0].Width, item.Position.Y);
                    prevX = MenuArea.X + temp[0].Width;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += HorizontalSpacing + temp[i].Width;
                        if (prevX > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextMenuItem>());
                            prevX = MenuArea.X + temp[i].Width;
                        }
                        foreach (TextMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int prevColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = MenuArea.Right - (temp[0].Items[0].Position.X);
                        }
                        else
                        {
                            for (int column = prevColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = MenuArea.Right - (temp[column].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = MenuArea.Right - (temp[column - 1].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }

                    break;
            }

            CurrentPage = 0;
        }

        public virtual void UpdateInput(GameTime gameTime)
        {
            if (Parent.Input.KeyPressed(ScrollDown))
            {
                string previousItem = SelectedText;
                int previousIndex = SelectedIndex;
                SelectedIndex++;
                if (SelectedIndex >= Items.Count)
                    SelectedIndex = 0;
                if (!ArrItems[CurrentPage].Contains(SelectedItem))
                {
                    CurrentPage++;
                    if (CurrentPage >= ArrItems.Count)
                        CurrentPage = 0;
                }

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedText, previousIndex, SelectedIndex));
            }
            if (Parent.Input.KeyPressed(ScrollUp))
            {
                string previousItem = SelectedText;
                int previousIndex = SelectedIndex;
                SelectedIndex--;
                if (SelectedIndex < 0)
                    SelectedIndex = Items.Count - 1;
                if (!ArrItems[CurrentPage].Contains(SelectedItem))
                {
                    CurrentPage--;
                    if (CurrentPage < 0)
                        CurrentPage = ArrItems.Count - 1;
                }

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedText, previousIndex, SelectedIndex));
            }
            if (Parent.Input.KeyPressed(Select))
            {
                if (SelectEvent != null)
                    SelectEvent.Invoke(this, new MenuSelectEventArgs(SelectedText));
            }
            for (int i = 0; i < Items.Count; i++)
                Items[i].Update(gameTime);
        }

        public virtual void Draw()
        {
            for (int i = 0; i < ArrItems[CurrentPage].Count; i++)
                ArrItems[CurrentPage][i].Draw();
        }
    }
}
