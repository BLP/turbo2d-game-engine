﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;

namespace Turbo2D.GUI
{
    public class TextButton : TextItem, IButton, IAcceptsInput
    {
        public event EventHandler<ButtonClickedEventArgs> Clicked;
        public event EventHandler<ButtonClickedEventArgs> MouseOver;
        public event EventHandler<ButtonClickedEventArgs> MouseEnter;
        public event EventHandler<ButtonClickedEventArgs> MouseLeave;
        protected Rectangle TextRect;
        public bool Enabled { get; set; }
        public new Vector2 Position { get { return base.Position; } set { base.Position = value; UpdateRect(); } }
        public new float Rotation { get { return base.Rotation; } set { base.Rotation = value; UpdateRect(); } }
        public new Vector2 Origin { get { return base.Origin; } set { base.Origin = value; UpdateRect(); } }
        public new Vector2 Scale { get { return base.Scale; } set { base.Scale = value; UpdateRect(); } }

        public TextButton(GameScreen parent, string name, string text, string fontName, Color color)
            : this(parent, name, text, fontName, color, Vector2.Zero)
        {
        }

        public TextButton(GameScreen parent, string name, string text, string fontName, Color color, Vector2 position)
            : base(parent, name, fontName, text, color, position)
        {
            Enabled = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

            TextRect = CollisionHelper.CalculateBoundingRectangle(
                new Rectangle((int)(Position.X - Origin.X), (int)(Position.Y - Origin.X), (int)TextSize.X, (int)TextSize.Y),
                CollisionHelper.CreateMatrix(Origin, Rotation, Position, Scale));
        }

        protected void UpdateRect()
        {
            UpdateRect();
        }

        protected bool IsMouseOver()
        {
            if (TextRect.Contains((int)Parent.Input.NewMousePosition.X, (int)Parent.Input.NewMousePosition.Y))
                return true;
            return false;
        }

        protected bool MouseClicked()
        {
            if (IsMouseOver() && Parent.Input.MouseButtonReleased(MouseButtons.LeftButton))
                return true;
            return false;
        }

        protected bool MouseClicking()
        {
            return (IsMouseOver() && Parent.Input.NewMouseState.LeftButton == ButtonState.Pressed);
        }

        protected bool MouseEntered()
        {
            return (IsMouseOver() && !TextRect.Contains((int)Parent.Input.OldMousePosition.X, (int)Parent.Input.OldMousePosition.Y));
        }

        protected bool MouseLeft()
        {
            return (!IsMouseOver() && TextRect.Contains((int)Parent.Input.OldMousePosition.X, (int)Parent.Input.OldMousePosition.Y));
        }

        public void UpdateInput(GameTime gameTime)
        {
            if (MouseEntered())
            {
                if (MouseEnter != null)
                    MouseEnter.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            else if (MouseLeft())
            {
                if (MouseLeave != null)
                    MouseLeave.Invoke(this, new ButtonClickedEventArgs(Name));
            }

            if (MouseClicked())
            {
                if (Clicked != null)
                    Clicked.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            else if (IsMouseOver())
            {
                if (MouseOver != null)
                    MouseOver.Invoke(this, new ButtonClickedEventArgs(Name));
            }
        }
    }
}
