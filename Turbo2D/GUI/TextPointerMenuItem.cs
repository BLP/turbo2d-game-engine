﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.GUI
{
    public class TextPointerMenuItem
    {
        public string Text { get; protected set; }
        public Color Color { get; protected set; }
        public Vector2 Position { get; set; }
        public Vector2 Origin { get; protected set; }
        public TextPointerMenu Menu { get; protected set; }
        public Vector2 TextSize { get { return Menu.Font.MeasureString(Text); } }

        public TextPointerMenuItem(TextPointerMenu menu, string text, Color color)
        {
            Menu = menu;
            Text = text;
            Color = color;
        }

        public void LoadContent(ContentManager Content)
        {
            switch (Menu.Alignment)
            {
                case MenuAlignments.Left:
                    Origin = Vector2.Zero;
                    break;
                case MenuAlignments.Center:
                    Origin = new Vector2(TextSize.X / 2, 0);
                    break;
                case MenuAlignments.Right:
                    Origin = new Vector2(TextSize.X, 0);
                    break;
            }
        }

        public virtual void Draw()
        {
            Menu.RenderDevice.DrawText(Menu.Font, Text, Position, Color, 0f, Origin, 1f, SpriteEffects.None, Menu.DrawLayer);
        }
    }
}
