﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Turbo2D.GUI
{
    public enum PointerAlignment
    {
        Left,
        Right
    }

    public class MenuSelectEventArgs : EventArgs
    {
        public string CurrentItem { get; protected set; }

        public MenuSelectEventArgs(string currentItem)
        {
            CurrentItem = currentItem;
        }
    }

    public class MenuScrollEventArgs : EventArgs
    {
        public string PreviousItem { get; protected set; }
        public string CurrentItem { get; protected set; }
        public int PreviousIndex { get; protected set; }
        public int CurrentIndex { get; protected set; }

        public MenuScrollEventArgs(string previousItem, string currentItem, int previousIndex, int currentIndex)
        {
            PreviousItem = previousItem;
            CurrentItem = currentItem;
            PreviousIndex = previousIndex;
            CurrentIndex = currentIndex;
        }
    }

    public class MenuPointer
    {
        public int SelectedIndex { get; set; }
        protected TextPointerMenuItem SelectedItem { get { return MenuItems[SelectedIndex]; } }
        public PointerAlignment Alignment { get; protected set; }
        public string SelectedText { get { return Menu.Items[SelectedIndex].Text; } }
        public List<TextPointerMenuItem> MenuItems { get { return Menu.Items; } }
        public const float BufferRoom = 3f;
        protected TextPointerMenu Menu;
        protected Keys ScrollDown;
        protected Keys ScrollUp;
        protected Keys Select;
        protected string TextureName;
        public Texture2D Texture { get; protected set; }
        public Vector2 Origin { get; protected set; }
        public Vector2 Position { get; set; }
        public bool Enabled { get; set; }
        public Rectangle SourceRectangle { get; set; }
        public event EventHandler<MenuSelectEventArgs> SelectEvent;
        public event EventHandler<MenuScrollEventArgs> Scroll;

        public MenuPointer(string textureName, TextPointerMenu menu, PointerAlignment alignment)
            : this(textureName, menu, alignment, Keys.Down, Keys.Up, Keys.Enter)
        {
        }

        public MenuPointer(string textureName, TextPointerMenu menu, PointerAlignment alignment, Keys scrollDown, Keys scrollUp,
            Keys select)
        {
            SelectedIndex = 0;
            TextureName = textureName;
            Menu = menu;
            Alignment = alignment;
            ScrollDown = scrollDown;
            ScrollUp = scrollUp;
            Select = select;
            SourceRectangle = new Rectangle(-1, -1, -1, -1);
            Enabled = true;
        }

        public void LoadContent(ContentManager Content)
        {
            Texture = Content.Load<Texture2D>(TextureName);

            if (SourceRectangle == new Rectangle(-1, -1, -1, -1))
                SourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);

            UpdatePosition();
        }

        public void UpdatePosition()
        {
            switch (Alignment)
            {
                case PointerAlignment.Left:
                    Position = new Vector2(SelectedItem.Position.X - SelectedItem.Origin.X - BufferRoom, SelectedItem.Position.Y +
                            SelectedItem.TextSize.Y / 2);
                    Origin = new Vector2(Texture.Width, Texture.Height / 2);
                    break;
                case PointerAlignment.Right:
                    Position = new Vector2(SelectedItem.Position.X + (SelectedItem.TextSize.X - SelectedItem.Origin.X) + BufferRoom,
                            SelectedItem.Position.Y + SelectedItem.TextSize.Y / 2);
                    Origin = new Vector2(0, Texture.Height / 2);
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            if (Menu.Parent.Input.KeyPressed(ScrollDown))
            {
                string previousItem = SelectedItem.Text;
                int previousIndex = SelectedIndex;
                SelectedIndex++;
                if (SelectedIndex >= MenuItems.Count)
                    SelectedIndex = 0;
                else if (SelectedIndex < 0)
                    SelectedIndex = MenuItems.Count - 1;

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedItem.Text, previousIndex, SelectedIndex));
            }
            if (Menu.Parent.Input.KeyPressed(ScrollUp))
            {
                string previousItem = SelectedItem.Text;
                int previousIndex = SelectedIndex;
                SelectedIndex--;
                if (SelectedIndex >= MenuItems.Count)
                    SelectedIndex = 0;
                else if (SelectedIndex < 0)
                    SelectedIndex = MenuItems.Count - 1;

                if (Scroll != null)
                    Scroll.Invoke(this, new MenuScrollEventArgs(previousItem, SelectedItem.Text, previousIndex, SelectedIndex));
            }
            if (Menu.Parent.Input.KeyPressed(Select))
            {
                if (SelectEvent != null)
                    SelectEvent.Invoke(this, new MenuSelectEventArgs(SelectedText));
            }
            UpdatePosition();
        }

        public void Draw()
        {
            Menu.RenderDevice.DrawTexture(Texture, Position, SourceRectangle, (Enabled ? Color.White : Color.Gray), 0f, Origin, 1f, SpriteEffects.None, 
                Menu.DrawLayer);
        }
    }
}
