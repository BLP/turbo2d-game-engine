﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;

namespace Turbo2D.GUI
{
    public class AnimatedMenuButton : AnimatedSprite, IButton, IAcceptsInput
    {
        public event EventHandler<ButtonClickedEventArgs> Clicked;
        public event EventHandler<ButtonClickedEventArgs> MouseOver;
        public event EventHandler<ButtonClickedEventArgs> MouseEnter;
        public event EventHandler<ButtonClickedEventArgs> MouseLeave;
        protected Rectangle ClickRect;
        public new Vector2 Position { get { return base.Position; } set { base.Position = value; UpdateRect(); } }
        public new float Rotation { get { return base.Rotation; } set { base.Rotation = value; UpdateRect(); } }
        public new Vector2 Origin { get { return base.Origin; } set { base.Origin = value; UpdateRect(); } }
        public new Vector2 Scale { get { return base.Scale; } set { base.Scale = value; UpdateRect(); } }


        public AnimatedMenuButton(GameScreen parent, string name, string textureName, Vector2 position)
            : base(parent, name, textureName, new string[] { "Button" }, 2, TimeSpan.Zero)
        {
            Position = position;
        }

        protected bool IsMouseOver()
        {
            return ClickRect.Contains((int)Parent.Input.NewMousePosition.X, (int)Parent.Input.NewMousePosition.Y);
        }

        protected bool MouseClicked()
        {
            return (IsMouseOver() && Parent.Input.MouseButtonReleased(MouseButtons.LeftButton));
        }

        protected bool MouseClicking()
        {
            return (IsMouseOver() && Parent.Input.NewMouseState.LeftButton == ButtonState.Pressed);
        }

        protected bool MouseEntered()
        {
            return (IsMouseOver() && !ClickRect.Contains((int)Parent.Input.OldMousePosition.X, (int)Parent.Input.OldMousePosition.Y));
        }

        protected bool MouseLeft()
        {
            return (!IsMouseOver() && ClickRect.Contains((int)Parent.Input.OldMousePosition.X, (int)Parent.Input.OldMousePosition.Y));
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            Origin = Vector2.Zero;
            CurrentAnimation.Pause();
            UpdateRect();
        }

        protected void UpdateRect()
        {
            ClickRect = CollisionHelper.CalculateBoundingRectangle(CurrentAnimation.CurrentFrame, CollisionHelper.CreateMatrix(Origin, Rotation,
                Position, Scale));
        }

        public void UpdateInput(GameTime gameTime)
        {
            if (MouseEntered())
            {
                if (MouseEnter != null)
                    MouseEnter.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            else if (MouseLeft())
            {
                if (MouseLeave != null)
                    MouseLeave.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            if (MouseClicked())
            {
                CurrentAnimation.SetCurrentFrame(0);
                if (Clicked != null)
                    Clicked.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            else if (MouseClicking())
                CurrentAnimation.SetCurrentFrame(1);
            else if (IsMouseOver())
            {
                if (MouseOver != null)
                    MouseOver.Invoke(this, new ButtonClickedEventArgs(Name));
            }
            else
                CurrentAnimation.SetCurrentFrame(0);
        }

        public override void Update(GameTime gameTime)
        {
                if (CurrentAnimation.Playing)
                    CurrentAnimation.Pause();
        }
    }
}
