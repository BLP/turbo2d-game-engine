var class_turbo2_d_1_1_serialization_1_1_settings =
[
    [ "Settings", "class_turbo2_d_1_1_serialization_1_1_settings.html#a462f3ecd356b18ec234d66da708f47f7", null ],
    [ "CreateDefault", "class_turbo2_d_1_1_serialization_1_1_settings.html#a0357b5dc6dd4d6afca2f0a61eed9a4bb", null ],
    [ "Load", "class_turbo2_d_1_1_serialization_1_1_settings.html#aee5006acd6bacee3e42dfe0a12c1bac6", null ],
    [ "Save", "class_turbo2_d_1_1_serialization_1_1_settings.html#aeb6e48cbd550d1021c07a437273e29ce", null ],
    [ "TargetElapsedTimeFromFPS", "class_turbo2_d_1_1_serialization_1_1_settings.html#a704481d3452a833ddfee3e3de7c19056", null ],
    [ "Content_RootDirectory", "class_turbo2_d_1_1_serialization_1_1_settings.html#a382555d7f028fe571b223dc0875c24aa", null ],
    [ "DefaultBackgroundColor", "class_turbo2_d_1_1_serialization_1_1_settings.html#aace4410dd7b958c06c62973941121b80", null ],
    [ "Game_InactiveSleepTime", "class_turbo2_d_1_1_serialization_1_1_settings.html#abe0b22d73e7600948c852d71988498c4", null ],
    [ "Game_IsFixedTimeStep", "class_turbo2_d_1_1_serialization_1_1_settings.html#a120f117d5b7ee2cbb83d8b0889089ebd", null ],
    [ "Game_IsMouseVisible", "class_turbo2_d_1_1_serialization_1_1_settings.html#a4fc1f998193f2d4758c264e66c40bca9", null ],
    [ "Game_TargetElapsedTime", "class_turbo2_d_1_1_serialization_1_1_settings.html#a14ca0a8d57664e0d07007c6973a08d52", null ],
    [ "Graphics_IsFullScreen", "class_turbo2_d_1_1_serialization_1_1_settings.html#adbea64556e2516df8b6c2e44c07c2899", null ],
    [ "Graphics_PreferredBackBufferHeight", "class_turbo2_d_1_1_serialization_1_1_settings.html#a3b147e0324aadd2bd4e1aaea0fff6f02", null ],
    [ "Graphics_PreferredBackBufferWidth", "class_turbo2_d_1_1_serialization_1_1_settings.html#a90d029563d80d90c5faebf0a3cf4003a", null ],
    [ "Graphics_SychronizeWithVerticleRetrace", "class_turbo2_d_1_1_serialization_1_1_settings.html#a4f97747ece0722f998b36636385125f5", null ],
    [ "Window_AllowUserResizing", "class_turbo2_d_1_1_serialization_1_1_settings.html#a00deb40133c928bb520106c82c8163e2", null ],
    [ "Window_Title", "class_turbo2_d_1_1_serialization_1_1_settings.html#acc6ddc16f2c0f36a971d763d3c5af7c0", null ]
];