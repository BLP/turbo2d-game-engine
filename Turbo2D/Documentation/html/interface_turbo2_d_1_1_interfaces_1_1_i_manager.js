var interface_turbo2_d_1_1_interfaces_1_1_i_manager =
[
    [ "Add", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#a15428c7ed3b5a6eef030639b3300fab7", null ],
    [ "LoadData", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#ad8ecdee7a1b6a3db0b8c1b6a561b1626", null ],
    [ "Remove", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#a75921eafc721a174660ed499fa0d3266", null ],
    [ "SaveData", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#ac685e8f3777cef2044fbacdf158c9264", null ],
    [ "Update", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#a5981f44a992a020203b08c5c9e2db70b", null ],
    [ "Enabled", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#a74fe06ce2c26105c504310380eb35457", null ],
    [ "ManagedType", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html#aef29ade4c03434b2ada2ce43b866a467", null ]
];