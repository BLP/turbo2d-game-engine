var class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args =
[
    [ "MenuScrollEventArgs", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#add50436e2b60f05ae72cd546899f8250", null ],
    [ "CurrentIndex", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#a2399c35c510c326f4f214fcfe0338023", null ],
    [ "CurrentItem", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#aef9c09d9995b61e3002e5ffaacdfa432", null ],
    [ "PreviousIndex", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#ab4b784f7584a3c7b9eeddbd39669a479", null ],
    [ "PreviousItem", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#a8868db4ea858639b4d17f121b95e9771", null ]
];