var hierarchy =
[
    [ "Turbo2D.Serialization.BinaryIOExtenstions", "class_turbo2_d_1_1_serialization_1_1_binary_i_o_extenstions.html", null ],
    [ "Turbo2D.Helpers.Camera2D", "class_turbo2_d_1_1_helpers_1_1_camera2_d.html", null ],
    [ "Turbo2D.Helpers.CollisionHelper", "class_turbo2_d_1_1_helpers_1_1_collision_helper.html", null ],
    [ "Turbo2D.GameObjects.GUI.Menu.ColumnInfo", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_1_1_column_info.html", null ],
    [ "Turbo2D.GameObjects.GUI.Menu.ColumnPair", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_1_1_column_pair.html", null ],
    [ "EventArgs", null, [
      [ "Turbo2D.Collections.GameObjectEventArgs", "class_turbo2_d_1_1_collections_1_1_game_object_event_args.html", null ],
      [ "Turbo2D.DrawEventArgs", "class_turbo2_d_1_1_draw_event_args.html", null ],
      [ "Turbo2D.GameObjects.GUI.MenuScrollEventArgs", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html", null ],
      [ "Turbo2D.GameObjects.GUI.MenuSelectEventArgs", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_select_event_args.html", null ],
      [ "Turbo2D.Helpers.CueDoneEventArgs", "class_turbo2_d_1_1_helpers_1_1_cue_done_event_args.html", null ],
      [ "Turbo2D.Interfaces.ButtonClickedEventArgs", "class_turbo2_d_1_1_interfaces_1_1_button_clicked_event_args.html", null ],
      [ "Turbo2D.Interfaces.RenderGroupChangedEventArgs", "class_turbo2_d_1_1_interfaces_1_1_render_group_changed_event_args.html", null ],
      [ "Turbo2D.Managers.CollisionEventArgs", "class_turbo2_d_1_1_managers_1_1_collision_event_args.html", null ],
      [ "Turbo2D.UpdateEventArgs", "class_turbo2_d_1_1_update_event_args.html", null ]
    ] ],
    [ "Game", null, [
      [ "Turbo2D.BaseEngine", "class_turbo2_d_1_1_base_engine.html", null ]
    ] ],
    [ "Turbo2D.Serialization.GameData", "class_turbo2_d_1_1_serialization_1_1_game_data.html", null ],
    [ "Turbo2D.GameObjects.GameObject", "class_turbo2_d_1_1_game_objects_1_1_game_object.html", [
      [ "Turbo2D.GameObjects.BackgroundTexture", "class_turbo2_d_1_1_game_objects_1_1_background_texture.html", null ],
      [ "Turbo2D.GameObjects.DrawableGameObject", "class_turbo2_d_1_1_game_objects_1_1_drawable_game_object.html", [
        [ "Turbo2D.GameObjects.DrawableRectangle", "class_turbo2_d_1_1_game_objects_1_1_drawable_rectangle.html", null ],
        [ "Turbo2D.GameObjects.GUI.Menu", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu.html", [
          [ "Turbo2D.GameObjects.GUI.TextMenu", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_menu.html", null ],
          [ "Turbo2D.GameObjects.GUI.TextPointerMenu", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_pointer_menu.html", null ]
        ] ],
        [ "Turbo2D.GameObjects.Sprite", "class_turbo2_d_1_1_game_objects_1_1_sprite.html", [
          [ "Turbo2D.GameObjects.AnimatedSprite", "class_turbo2_d_1_1_game_objects_1_1_animated_sprite.html", [
            [ "Turbo2D.GameObjects.CollidableAnimatedSprite", "class_turbo2_d_1_1_game_objects_1_1_collidable_animated_sprite.html", null ],
            [ "Turbo2D.GameObjects.GUI.AnimatedMenuButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_animated_menu_button.html", null ]
          ] ],
          [ "Turbo2D.GameObjects.CollidableSprite", "class_turbo2_d_1_1_game_objects_1_1_collidable_sprite.html", null ],
          [ "Turbo2D.GameObjects.GUI.MenuButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_button.html", null ],
          [ "Turbo2D.GameObjects.GUI.MenuPointer", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_pointer.html", null ]
        ] ],
        [ "Turbo2D.GameObjects.TextItem", "class_turbo2_d_1_1_game_objects_1_1_text_item.html", [
          [ "Turbo2D.GameObjects.GUI.MenuItem", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_item.html", [
            [ "Turbo2D.GameObjects.GUI.TextMenuItem", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_menu_item.html", null ]
          ] ],
          [ "Turbo2D.GameObjects.GUI.TextButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_button.html", null ]
        ] ]
      ] ],
      [ "Turbo2D.GameObjects.SolidBackground", "class_turbo2_d_1_1_game_objects_1_1_solid_background.html", null ]
    ] ],
    [ "Turbo2D.Helpers.GamePadInputHelper", "class_turbo2_d_1_1_helpers_1_1_game_pad_input_helper.html", null ],
    [ "Turbo2D.GameScreens.GameScreen", "class_turbo2_d_1_1_game_screens_1_1_game_screen.html", [
      [ "Turbo2D.GameScreens.LoadScreen< T >", "class_turbo2_d_1_1_game_screens_1_1_load_screen.html", null ]
    ] ],
    [ "GameScreen", null, [
      [ "Turbo2D.Implementation.ScreenTypes.TextPointerMenuScreen", "class_turbo2_d_1_1_implementation_1_1_screen_types_1_1_text_pointer_menu_screen.html", null ]
    ] ],
    [ "Turbo2D.Helpers.GameScreenHelper", "class_turbo2_d_1_1_helpers_1_1_game_screen_helper.html", null ],
    [ "Turbo2D.Interfaces.IAcceptsGamePadInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_game_pad_input.html", null ],
    [ "Turbo2D.Interfaces.IAcceptsKeyboardInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_keyboard_input.html", [
      [ "Turbo2D.GameObjects.GUI.MenuPointer", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_pointer.html", null ],
      [ "Turbo2D.GameObjects.GUI.TextMenu", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_menu.html", null ],
      [ "Turbo2D.GameObjects.GUI.TextPointerMenu", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_pointer_menu.html", null ]
    ] ],
    [ "Turbo2D.Interfaces.IAcceptsMouseInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_mouse_input.html", [
      [ "Turbo2D.Interfaces.IButton", "interface_turbo2_d_1_1_interfaces_1_1_i_button.html", [
        [ "Turbo2D.GameObjects.GUI.AnimatedMenuButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_animated_menu_button.html", null ],
        [ "Turbo2D.GameObjects.GUI.MenuButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_button.html", null ],
        [ "Turbo2D.GameObjects.GUI.TextButton", "class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_button.html", null ]
      ] ]
    ] ],
    [ "Turbo2D.Interfaces.IClickableLayer", "class_turbo2_d_1_1_interfaces_1_1_i_clickable_layer.html", null ],
    [ "ICollection", null, [
      [ "Turbo2D.Collections.GameObjectCollection", "class_turbo2_d_1_1_collections_1_1_game_object_collection.html", null ]
    ] ],
    [ "Turbo2D.Interfaces.ICollidable", "interface_turbo2_d_1_1_interfaces_1_1_i_collidable.html", [
      [ "Turbo2D.GameObjects.CollidableAnimatedSprite", "class_turbo2_d_1_1_game_objects_1_1_collidable_animated_sprite.html", null ],
      [ "Turbo2D.GameObjects.CollidableSprite", "class_turbo2_d_1_1_game_objects_1_1_collidable_sprite.html", null ]
    ] ],
    [ "Turbo2D.Interfaces.IDisposableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_disposable_object.html", null ],
    [ "Turbo2D.Interfaces.IDrawableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_drawable_object.html", [
      [ "Turbo2D.GameObjects.BackgroundTexture", "class_turbo2_d_1_1_game_objects_1_1_background_texture.html", null ],
      [ "Turbo2D.GameObjects.DrawableGameObject", "class_turbo2_d_1_1_game_objects_1_1_drawable_game_object.html", null ],
      [ "Turbo2D.GameObjects.SolidBackground", "class_turbo2_d_1_1_game_objects_1_1_solid_background.html", null ]
    ] ],
    [ "IEnumerable", null, [
      [ "Turbo2D.Collections.RenderGroupCollection", "class_turbo2_d_1_1_collections_1_1_render_group_collection.html", null ],
      [ "Turbo2D.Collections.RenderViewportCollection", "class_turbo2_d_1_1_collections_1_1_render_viewport_collection.html", null ],
      [ "Turbo2D.Helpers.CameraManager", "class_turbo2_d_1_1_helpers_1_1_camera_manager.html", null ]
    ] ],
    [ "Turbo2D.Interfaces.IManager", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html", [
      [ "Turbo2D.Managers.Manager< T >", "class_turbo2_d_1_1_managers_1_1_manager.html", null ]
    ] ],
    [ "Turbo2D.Interfaces.IUpdateableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_updateable_object.html", [
      [ "Turbo2D.GameObjects.AnimatedSprite", "class_turbo2_d_1_1_game_objects_1_1_animated_sprite.html", null ]
    ] ],
    [ "Turbo2D.Helpers.KeyboardInputHelper", "class_turbo2_d_1_1_helpers_1_1_keyboard_input_helper.html", null ],
    [ "List", null, [
      [ "Turbo2D.Collections.ManagerCollection", "class_turbo2_d_1_1_collections_1_1_manager_collection.html", null ]
    ] ],
    [ "Turbo2D.Managers.Manager< ICollidable >", "class_turbo2_d_1_1_managers_1_1_manager.html", [
      [ "Turbo2D.Managers.CollisionManager", "class_turbo2_d_1_1_managers_1_1_collision_manager.html", null ]
    ] ],
    [ "Turbo2D.Helpers.MouseInputHelper", "class_turbo2_d_1_1_helpers_1_1_mouse_input_helper.html", null ],
    [ "Turbo2D.Helpers.Primitives", "class_turbo2_d_1_1_helpers_1_1_primitives.html", null ],
    [ "Turbo2D.Helpers.Renderer", "class_turbo2_d_1_1_helpers_1_1_renderer.html", null ],
    [ "Turbo2D.Helpers.RenderGroup", "class_turbo2_d_1_1_helpers_1_1_render_group.html", null ],
    [ "Turbo2D.Helpers.RenderViewport", "class_turbo2_d_1_1_helpers_1_1_render_viewport.html", null ],
    [ "Turbo2D.Serialization.Settings", "class_turbo2_d_1_1_serialization_1_1_settings.html", null ],
    [ "Turbo2D.Helpers.SongHelper", "class_turbo2_d_1_1_helpers_1_1_song_helper.html", null ],
    [ "Turbo2D.Helpers.SoundHelper", "class_turbo2_d_1_1_helpers_1_1_sound_helper.html", null ],
    [ "Turbo2D.Helpers.SpriteAnimation", "class_turbo2_d_1_1_helpers_1_1_sprite_animation.html", null ],
    [ "Turbo2D.TurboGame", "class_turbo2_d_1_1_turbo_game.html", null ],
    [ "Turbo2D.Helpers.TurboSong", "class_turbo2_d_1_1_helpers_1_1_turbo_song.html", null ],
    [ "Turbo2D.Serialization.XMLHelper", "class_turbo2_d_1_1_serialization_1_1_x_m_l_helper.html", null ]
];