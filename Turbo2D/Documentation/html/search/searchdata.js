var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwx",
  1: "abcdgiklmprstux",
  2: "t",
  3: "abcdgiklmprstx",
  4: "abcdefgiklmoprstuw",
  5: "abcdefhilmnoprstuw",
  6: "bgmp",
  7: "aclmrst",
  8: "abcdefghiklmnoprstvwx",
  9: "acdfilmorsu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

