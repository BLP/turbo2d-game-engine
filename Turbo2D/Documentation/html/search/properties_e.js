var searchData=
[
  ['parent',['Parent',['../class_turbo2_d_1_1_game_screens_1_1_game_screen.html#addd35a0c68ea7175b4d5232762f8cddd',1,'Turbo2D.GameScreens.GameScreen.Parent()'],['../class_turbo2_d_1_1_helpers_1_1_camera2_d.html#a3b0aa9a30f172158dd7a9d9165b35447',1,'Turbo2D.Helpers.Camera2D.Parent()'],['../class_turbo2_d_1_1_managers_1_1_manager.html#a7f9c52180775f06594274f258661a6ea',1,'Turbo2D.Managers.Manager.Parent()']]],
  ['parentobject',['ParentObject',['../class_turbo2_d_1_1_game_objects_1_1_drawable_game_object.html#a5cdad2c3eac29c1a0ab03875a6c7000d',1,'Turbo2D::GameObjects::DrawableGameObject']]],
  ['playing',['Playing',['../class_turbo2_d_1_1_helpers_1_1_sprite_animation.html#af36e05bacad99ffb81d46132da08198e',1,'Turbo2D::Helpers::SpriteAnimation']]],
  ['pointers',['Pointers',['../class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_text_pointer_menu.html#af1e0a95a9b585dd0e33794fc9458813f',1,'Turbo2D::GameObjects::GUI::TextPointerMenu']]],
  ['position',['Position',['../class_turbo2_d_1_1_game_objects_1_1_drawable_game_object.html#a504f36a3073294171d4189fb8a481ec4',1,'Turbo2D.GameObjects.DrawableGameObject.Position()'],['../class_turbo2_d_1_1_helpers_1_1_camera2_d.html#a8b0f371390d4bc35c65ba041b0ee73c2',1,'Turbo2D.Helpers.Camera2D.Position()']]],
  ['previousindex',['PreviousIndex',['../class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#ab4b784f7584a3c7b9eeddbd39669a479',1,'Turbo2D::GameObjects::GUI::MenuScrollEventArgs']]],
  ['previousitem',['PreviousItem',['../class_turbo2_d_1_1_game_objects_1_1_g_u_i_1_1_menu_scroll_event_args.html#a8868db4ea858639b4d17f121b95e9771',1,'Turbo2D::GameObjects::GUI::MenuScrollEventArgs']]]
];
