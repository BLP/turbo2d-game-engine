var namespace_turbo2_d_1_1_interfaces =
[
    [ "ButtonClickedEventArgs", "class_turbo2_d_1_1_interfaces_1_1_button_clicked_event_args.html", "class_turbo2_d_1_1_interfaces_1_1_button_clicked_event_args" ],
    [ "IAcceptsGamePadInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_game_pad_input.html", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_game_pad_input" ],
    [ "IAcceptsKeyboardInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_keyboard_input.html", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_keyboard_input" ],
    [ "IAcceptsMouseInput", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_mouse_input.html", "interface_turbo2_d_1_1_interfaces_1_1_i_accepts_mouse_input" ],
    [ "IButton", "interface_turbo2_d_1_1_interfaces_1_1_i_button.html", "interface_turbo2_d_1_1_interfaces_1_1_i_button" ],
    [ "IClickableLayer", "class_turbo2_d_1_1_interfaces_1_1_i_clickable_layer.html", null ],
    [ "ICollidable", "interface_turbo2_d_1_1_interfaces_1_1_i_collidable.html", "interface_turbo2_d_1_1_interfaces_1_1_i_collidable" ],
    [ "IDisposableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_disposable_object.html", "interface_turbo2_d_1_1_interfaces_1_1_i_disposable_object" ],
    [ "IDrawableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_drawable_object.html", "interface_turbo2_d_1_1_interfaces_1_1_i_drawable_object" ],
    [ "IManager", "interface_turbo2_d_1_1_interfaces_1_1_i_manager.html", "interface_turbo2_d_1_1_interfaces_1_1_i_manager" ],
    [ "IUpdateableObject", "interface_turbo2_d_1_1_interfaces_1_1_i_updateable_object.html", "interface_turbo2_d_1_1_interfaces_1_1_i_updateable_object" ],
    [ "RenderGroupChangedEventArgs", "class_turbo2_d_1_1_interfaces_1_1_render_group_changed_event_args.html", "class_turbo2_d_1_1_interfaces_1_1_render_group_changed_event_args" ]
];