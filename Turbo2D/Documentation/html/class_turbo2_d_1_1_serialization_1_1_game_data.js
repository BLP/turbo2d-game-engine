var class_turbo2_d_1_1_serialization_1_1_game_data =
[
    [ "GameData", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a19430f72490ef845a7fc2aaa15b8f17c", null ],
    [ "CreateDefault", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a909321f12f52bc065f34acbfd6ebd5ce", null ],
    [ "Load", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a431bbfe7f2b791a0b0543e5fec02519d", null ],
    [ "Save", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a50ffe20d13018156d28b0c406df19b68", null ],
    [ "FirstScreen", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a73c0ef82146cd7537d878d20bfe9f8df", null ],
    [ "Name", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a554205e6aa4080d48223343cc8243786", null ],
    [ "Screens", "class_turbo2_d_1_1_serialization_1_1_game_data.html#ab680f88ae0c2b7bf9af6a7b3f8a796a3", null ],
    [ "SettingsFile", "class_turbo2_d_1_1_serialization_1_1_game_data.html#a0866b38fee904629619c7c1841f9c0d9", null ]
];