var NAVTREE =
[
  [ "Turbo2D Game Engine", "index.html", [
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_animated_menu_button_8cs.html",
"class_turbo2_d_1_1_game_objects_1_1_collidable_animated_sprite.html#a5d79bb58d76e62eb9283343a8aff7aad",
"class_turbo2_d_1_1_game_objects_1_1_sprite.html#a9087479792d582f83483f7930b7a5a23",
"class_turbo2_d_1_1_serialization_1_1_binary_i_o_extenstions.html#af809ba7412ffd5fed599086891faaa37"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';