var class_turbo2_d_1_1_game_objects_1_1_text_item =
[
    [ "TextItem", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#abcf0c990134e66dc476decd85df87e67", null ],
    [ "TextItem", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#ac9d6a1c4f22e2d0cd48390b25bde60c8", null ],
    [ "Draw", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a0fc41bfc53beb470b496f9ca4561ced6", null ],
    [ "LoadContent", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a8e02e34eb781e94b033b207b839ad322", null ],
    [ "LoadData", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#ac8a85200f2e32c25250abbe47f226b77", null ],
    [ "SaveData", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a651c2459ba0a845e3e39ef82b84d45d0", null ],
    [ "Color", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a39538f93c8aed9f77562c359959fb3df", null ],
    [ "Effects", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a7c2c4bf5e3d7d04d3c73761459aaa0fc", null ],
    [ "Font", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#ad6d63be05eb310d8e9fa11919a3d0414", null ],
    [ "Text", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#aba9e07ba81fa10fc06d96d031db78abe", null ],
    [ "TextSize", "class_turbo2_d_1_1_game_objects_1_1_text_item.html#a516e8b5b725f5978ef6fa039e6ace18c", null ]
];