﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Managers;
using Turbo2D.GameScreens;
using Turbo2D.GameObjects;
using Turbo2D.Interfaces;
using System.IO;
using System.Reflection;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of object managers
    /// </summary>
    public sealed class ManagerCollection : List<IManager>
    {
        /// <summary>
        /// Creates a new instance of a ManagerCollection
        /// </summary>
        public ManagerCollection()
        {
        }

        /// <summary>
        /// Adds a GameObject to manage
        /// </summary>
        /// <param name="gameObject">The GameObject to be managed</param>
        public void RegisterGameObject(GameObject gameObject)
        {
            // Loop through each manager and add the GameObject to each manager that manages its type
            foreach (IManager manager in this)
            {
                if (manager.ManagedType.IsInstanceOfType(gameObject))
                    manager.Add(gameObject);
            }
        }

        /// <summary>
        /// Removes a GameObject from management
        /// </summary>
        /// <param name="gameObject">The GameObject to cease being managed</param>
        public void DeregisterGameObject(GameObject gameObject)
        {
            // Loop through each manager and remove the GameObject to each manager that manages its type
            foreach (IManager manager in this)
            {
                if (manager.ManagedType.IsInstanceOfType(gameObject))
                    manager.Remove(gameObject);
            }
        }

        internal static void Save(BinaryWriter writer, ManagerCollection collection)
        {
            writer.Write(collection.Count);

            foreach (IManager manager in collection)
            {
                writer.Write(manager.GetType().AssemblyQualifiedName);
                manager.SaveData(writer);
            }
        }

        internal static ManagerCollection Load(BinaryReader reader, GameScreen parent)
        {
            ManagerCollection collection = new ManagerCollection();

            int numManagers = reader.ReadInt32();

            for (int i = 0; i < numManagers; i++)
            {
                Type type = Type.GetType(reader.ReadString());
                ConstructorInfo[] info = type.GetConstructors();
                ParameterInfo[] parameters = info[0].GetParameters();
                object[] args = new object[parameters.Length];

                for (int j = 0; j < parameters.Length; i++)
                    if (parameters[j].ParameterType == typeof(GameScreen))
                        args[j] = parent;
                IManager manager = (IManager)info[0].Invoke(args);

                manager.LoadData(reader);

                collection.Add(manager);
            }

            return collection;
        }
    }
}
