﻿using System;
using System.Collections.Generic;
using System.Linq;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Managers;
using Microsoft.Xna.Framework.Content;
using System.IO;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of GameObjects
    /// </summary>
    public sealed class GameObjectCollection : ICollection<GameObject>
    {
        /// <summary>
        /// Fires when an object is added to the collection
        /// </summary>
        public event EventHandler<GameObjectEventArgs> ObjectAdded;
        /// <summary>
        /// Fires when an object is removed from the collection
        /// </summary>
        public event EventHandler<GameObjectEventArgs> ObjectRemoved;

        private Dictionary<string, GameObject> GameObjectStorage = new Dictionary<string, GameObject>();
        private ContentManager Content;

        /// <summary>
        /// Creates a new instance of a GameObjectCollection
        /// </summary>
        /// <param name="content">The ContentManager used to load GameObjects</param>
        public GameObjectCollection(ContentManager content)
            : base()
        {
            Content = content;
        }

        /// <summary>
        /// Adds a GameObject to the collection
        /// </summary>
        /// <param name="value">The GameObject to add</param>
        public void Add(GameObject value)
        {
            GameObjectStorage.Add(value.Name, value);

            value.LoadContent(Content);

            if (ObjectAdded != null)
                ObjectAdded(this, new GameObjectEventArgs(value));
        }

        /// <summary>
        /// Removes a GameObject from the collection
        /// </summary>
        /// <param name="item">The GameObject to remove</param>
        /// <returns>Whether or not the removal was successful</returns>
        public bool Remove(GameObject item)
        {
            return Remove(item.Name);
        }

        /// <summary>
        /// Removes a GameObject from the collection
        /// </summary>
        /// <param name="key">The name of the GameObject to remove</param>
        /// <returns>Whether or not the removal was successful</returns>
        public bool Remove(string key)
        {
            GameObject gObject = GameObjectStorage[key];
            if (gObject is IDisposableObject)
                (gObject as IDisposableObject).Dispose();
            bool result = GameObjectStorage.Remove(key);

            if (ObjectRemoved != null)
                ObjectRemoved(this, new GameObjectEventArgs(gObject));

            return result;
        }

        /// <summary>
        /// Returns whether or not the collection contains a GameObject with the specified name
        /// </summary>
        /// <param name="key">The name of the GameObject</param>
        /// <returns>Whether a GameObject by the given name was found</returns>
        public bool Contains(string key)
        {
            return GameObjectStorage.ContainsKey(key);
        }

        /// <summary>
        /// Returns whether or not the collection contains the specified GameObject
        /// </summary>
        /// <param name="gameObject">The GameObject to check</param>
        /// <returns>Whether the GameObject was found or not</returns>
        public bool Contains(GameObject gameObject)
        {
            return GameObjectStorage.ContainsValue(gameObject);
        }

        /// <summary>
        /// Returns the GameObject of the specified name cast to the specified type
        /// </summary>
        /// <typeparam name="T">The type to cast the GameObject to</typeparam>
        /// <param name="name">The name of the GameObject</param>
        /// <returns>The cast GameObject</returns>
        public T Get<T>(string name) where T : GameObject
        {
            if (GameObjectStorage.ContainsKey(name))
            {
                return (GameObjectStorage[name] as T);
            }
            else
                throw new KeyNotFoundException();
        }

        /// <summary>
        /// Gets all GameObjects of the specified type
        /// </summary>
        /// <typeparam name="T">The type of GameObject to get</typeparam>
        /// <returns>An array of the matching GameObjects</returns>
        public T[] GetAll<T>() where T : GameObject
        {
            return GameObjectStorage.Values.OfType<T>().ToArray<T>();
        }

        /// <summary>
        /// Gets the enumerator of the collecton
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<GameObject> GetEnumerator()
        {
            return GameObjectStorage.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GameObjectStorage.Values.GetEnumerator();
        }


        /// <summary>
        /// Clears out all GameObjects from the collection
        /// </summary>
        public void Clear()
        {
            foreach (GameObject gameObject in GameObjectStorage.Values)
                Remove(gameObject);
        }

        /// <summary>
        /// Copies the GameObjects in the collection to the array, starting at the specified index
        /// </summary>
        /// <param name="array">The array to copy the values to</param>
        /// <param name="arrayIndex">The index in the array at which to start the copying</param>
        public void CopyTo(GameObject[] array, int arrayIndex)
        {
            int currentIndex = arrayIndex;
            foreach (GameObject gameObject in GameObjectStorage.Values)
            {
                array[currentIndex] = gameObject;
                currentIndex++;
            }
        }

        /// <summary>
        /// The number of GameObjects in the collection
        /// </summary>
        public int Count
        {
            get { return GameObjectStorage.Count; }
        }

        /// <summary>
        /// Whether or not the collection is read-only
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        internal static void Save(BinaryWriter writer, GameObjectCollection collection)
        {
            writer.Write(collection.Count);

            foreach (GameObject gObject in collection)
                GameObject.Save(writer, gObject);
        }

        internal static GameObjectCollection Load(BinaryReader reader, ContentManager content, GameScreen parent, GameObject parentObject = null)
        {
            return Load(reader, content, parent, null, null);
        }

        internal static GameObjectCollection Load(BinaryReader reader, ContentManager content, GameScreen parent, EventHandler<GameObjectEventArgs> objectAdded,
            EventHandler<GameObjectEventArgs> objectRemoved, GameObject parentObject = null)
        {
            GameObjectCollection collection = new GameObjectCollection(content);
            
            if (objectAdded != null)
                collection.ObjectAdded += new EventHandler<GameObjectEventArgs>(objectAdded);
            if (objectRemoved != null)
                collection.ObjectRemoved += new EventHandler<GameObjectEventArgs>(objectRemoved);

            int numObjects = reader.ReadInt32();

            for (int i = 0; i < numObjects; i++)
            {
                collection.Add(GameObject.Load(reader, parent, parentObject));
            }

            return collection;
        }
    }

    public class GameObjectEventArgs : EventArgs
    {
        public GameObject Object { get; protected set; }

        public GameObjectEventArgs(GameObject gameObject)
        {
            Object = gameObject;
        }
    }
}
