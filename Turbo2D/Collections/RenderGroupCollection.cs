﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Interfaces;
using Turbo2D.Helpers;
using System.IO;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of RenderGroups
    /// </summary>
    public sealed class RenderGroupCollection : IEnumerable<RenderGroup>
    {
        /// <summary>
        /// The RenderGroups contained in the collection sorted by DrawIndex
        /// </summary>
        private List<RenderGroup> SortedList = new List<RenderGroup>();

        /// <summary>
        /// Get a RenderGroup by name
        /// </summary>
        /// <param name="key">The name of the RenderGroup</param>
        /// <returns>A RenderGroup</returns>
        public RenderGroup this[string key] { get { return RenderGroups[key]; } }

        /// <summary>
        /// Get a RenderGroup by its key in the sorted list
        /// </summary>
        /// <param name="index">The index of the RenderGroup in the sorted list</param>
        /// <returns>A RenderGroup</returns>
        public RenderGroup this[int index] { get { return SortedList[index]; } }

        /// <summary>
        /// The number of RenderGroups in the collection
        /// </summary>
        public int Count { get { return RenderGroups.Count; } }

        // Internal storage of RenderGroups
        private Dictionary<string, RenderGroup> RenderGroups = new Dictionary<string, RenderGroup>();

        /// <summary>
        /// Gets a list of all the RenderGroup names in the collection
        /// </summary>
        internal string[] GroupNames { get { return RenderGroups.Keys.ToArray<string>(); } }

        /// <summary>
        /// Creates a new instance of a RenderGroupCollection
        /// </summary>
        public RenderGroupCollection()
        {
        }

        /// <summary>
        /// Creates a new RenderGroup for drawing.
        /// </summary>
        /// <param name="groupName">The name of the RenderGroup.</param>
        /// <param name="cameraAffected">If the objects drawn in this RenderGroup are affected by cameras.</param>
        /// <param name="drawOrder">The position in the draw order where this RenderGroup should be drawn.
        /// The higher the index, the earlier in the draw order.</param>
        public void Add(string groupName, bool cameraAffected, int drawOrder)
        {
            // Create the new RenderGroup
            RenderGroup newRenderGroup = new RenderGroup(cameraAffected, drawOrder, groupName);

            // Hook up to draw index changed event
            newRenderGroup.DrawIndexChanged += newRenderGroup_DrawIndexChanged;
            // Add it to the dictionary storage
            RenderGroups.Add(newRenderGroup.Name, newRenderGroup);
            // Add it to the sorted list
            SortedList.Add(newRenderGroup);

            SortList();
        }

        void newRenderGroup_DrawIndexChanged(object sender, EventArgs e)
        {
            // Re-sort the list
            SortList();
        }

        private void SortList()
        {
            // Sort the list
            SortedList.Sort((group1, group2) => group1.DrawIndex - group2.DrawIndex);
        }

        internal void Remove(string name, string newGroup)
        {
            // Reassign all game objects to different RenderGroup
            foreach (IDrawableObject drawable in RenderGroups[name].DrawableObjects)
                drawable.RenderGroup = newGroup;

            // Remove event hook
            RenderGroups[name].DrawIndexChanged -= newRenderGroup_DrawIndexChanged;

            // Remove RenderGroup from sorted list
            SortedList.Remove(RenderGroups[name]);

            // Remove RenderGroup from main list
            RenderGroups.Remove(name);
        }

        /// <summary>
        /// Adds a GameObject to draw
        /// </summary>
        /// <param name="renderGroups">The RenderGroup to add the GameObject to</param>
        public void RegisterGameObject(IDrawableObject gameObject)
        {
            // Add the GameObject to the RenderGroup it belongs in
            RenderGroups[gameObject.RenderGroup].AddObject(gameObject);
            // Register this with the object's RenderGroupChanged event
            gameObject.RenderGroupChanged += new EventHandler<RenderGroupChangedEventArgs>(gameObject_RenderGroupChanged);
        }

        private void gameObject_RenderGroupChanged(object sender, RenderGroupChangedEventArgs e)
        {
            IDrawableObject drawableObject = sender as IDrawableObject;
            // Remove the object from the old group
            RenderGroups[e.OldGroup].RemoveObject(drawableObject);
            // Add it to the new group
            RenderGroups[e.NewGroup].AddObject(drawableObject);
        }

        /// <summary>
        /// Removes a GameObject from drawing
        /// </summary>
        /// <param name="renderGroups">The RenderGroups to remove the GameObject from</param>
        /// <param name="gameObject">The GameObject to be drawn</param>
        public void DeregisterGameObject(IDrawableObject gameObject)
        {
            // Remove the GameObject from the RenderGroup it belongs in
            RenderGroups[gameObject.RenderGroup].RemoveObject(gameObject);
            // Deregister this with the object's RenderGroupChanged event
            gameObject.RenderGroupChanged -= new EventHandler<RenderGroupChangedEventArgs>(gameObject_RenderGroupChanged);
        }

        internal static void Save(BinaryWriter writer, RenderGroupCollection collection)
        {
            writer.Write(collection.RenderGroups.Count);
            foreach (string key in collection.RenderGroups.Keys)
            {
                writer.Write(key);
                RenderGroup.Save(writer, collection.RenderGroups[key]);
            }
        }

        internal static RenderGroupCollection Load(BinaryReader reader)
        {
            RenderGroupCollection collection = new RenderGroupCollection();

            int numGroups = reader.ReadInt32();

            for (int i = 0; i < numGroups; i++)
            {
                string name = reader.ReadString();
                RenderGroup group = RenderGroup.Load(reader);
                group.DrawIndexChanged += collection.newRenderGroup_DrawIndexChanged;
                collection.RenderGroups.Add(name, group);
                collection.SortedList.Add(group);
            }
            collection.SortList();

            return collection;
        }

        /// <summary>
        /// Gets the enumerator for the collection
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<RenderGroup> GetEnumerator()
        {
            return SortedList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return SortedList.GetEnumerator();
        }
    }

    internal class RenderGroupEventArgs : EventArgs
    {
        public string RenderGroupName { get; private set; }

        public RenderGroupEventArgs(string name)
        {
            RenderGroupName = name;
        }
    }
}
