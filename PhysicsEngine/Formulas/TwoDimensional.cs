﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turbo2D.Formulas
{
    /// <summary>
    /// Provides two-dimensional motion equations solved for different variables.
    /// </summary>
    public static class TwoDimensional
    {
        /// <summary>
        /// Implementation of the range equation: range = v0*v0 * sin(2*theta)/g.
        /// </summary>
        /// <param name="range">The horizontal range of the projectile</param>
        /// <param name="v0">The initial velocity of the projectile</param>
        /// <param name="theta">The angle at which the projectile is launched</param>
        /// <param name="g">The acceleration due to gravity</param>
        public static void Range(out float range, float v0, float theta, float g)
        {
            range = (float)((Math.Pow(v0, 2) * Math.Sin(2 * theta)) / g);
        }

        /// <summary>
        /// Implementation of the range equation: range = v0*v0 * sin(2*theta)/g.
        /// </summary>
        /// <param name="range">The horizontal range of the projectile</param>
        /// <param name="v0">The initial velocity of the projectile</param>
        /// <param name="theta">The angle at which the projectile is launched</param>
        /// <param name="g">The acceleration due to gravity</param>
        public static void Range(float range, out float v0, float theta, float g)
        {
            v0 = (float)Math.Sqrt((range * g) / Math.Sin(2 * theta));
        }

        /// <summary>
        /// Implementation of the range equation: range = v0*v0 * sin(2*theta)/g.
        /// </summary>
        /// <param name="range">The horizontal range of the projectile</param>
        /// <param name="v0">The initial velocity of the projectile</param>
        /// <param name="theta">The angle at which the projectile is launched</param>
        /// <param name="g">The acceleration due to gravity</param>
        public static void Range(float range, float v0, out float theta, float g)
        {
            theta = (float)(Math.Asin((range * g) / Math.Pow(v0, 2)) / 2);
        }

        /// <summary>
        /// Implementation of the range equation: range = v0*v0 * sin(2*theta)/g.
        /// </summary>
        /// <param name="range">The horizontal range of the projectile</param>
        /// <param name="v0">The initial velocity of the projectile</param>
        /// <param name="theta">The angle at which the projectile is launched</param>
        /// <param name="g">The acceleration due to gravity</param>
        public static void Range(float range, float v0, float theta, out float g)
        {
            g = (float)((Math.Pow(v0, 2) * Math.Sin(2 * theta)) / range);
        }

        /// <summary>
        /// Calculates the range of a projectile that follows a parabolic path.
        /// </summary>
        /// <param name="v0">The initial velocity of the projectile</param>
        /// <param name="theta">The angle at which the projectile is launched</param>
        /// <param name="g">The accleration due to gravity</param>
        /// <param name="startHeight">The starting height of the projectile</param>
        /// <param name="endHeight">The ending height of the projectile</param>
        /// <returns></returns>
        public static float CalculateParabolicRange(float v0, float theta, float g, float startHeight, float endHeight)
        {
            float range = 0f;
            if (startHeight == endHeight)
            {
                Range(out range, v0, theta, -g);
            }
            else
            {
                float t;
                OneDimensional.Displacement(startHeight - endHeight, v0, out t, g);
                OneDimensional.Displacement(out range, v0, t, 0);
            }
            return range;
        }
    }
}
