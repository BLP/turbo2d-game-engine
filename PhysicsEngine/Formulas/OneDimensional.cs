﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turbo2D.Formulas
{
    /// <summary>
    /// Provides one-dimensional motion equations solved for different variables.
    /// </summary>
    public static class OneDimensional
    {
        /// <summary>
        /// Returns the velocity of an object in one dimension (v = delta_x/delta_t).
        /// </summary>
        /// <param name="delta_x">The change in the object's displacement</param>
        /// <param name="delta_t">The amount of time it takes the change to happen</param>
        /// <returns>The velocity of an object</returns>
        public static float Velocity(float delta_x, float delta_t)
        {
            if (delta_t <= 0)
                throw new Exception("Error, delta_T must be greater than zero.");
            return delta_x / delta_t;
        }

        /// <summary>
        /// Returns the speed of an object in one dimension (speed = delta_d/delta_t).
        /// </summary>
        /// <param name="delta_d">The change in the object's distance</param>
        /// <param name="delta_t">The amount of time it takes the change to happen</param>
        /// <returns>The speed of an object</returns>
        public static float Speed(float delta_d, float delta_t)
        {
            if (delta_d < 0)
                throw new Exception("Error, delta_d cannot be negative.");
            if (delta_t <= 0)
                throw new Exception("Error, delta_t must be greater than zero.");
            return delta_d / delta_t;
        }

        /// <summary>
        /// Returns the acceleration of an object in one dimension (a = delta_v/delta_t).
        /// </summary>
        /// <param name="delta_v">The change in the object's velocity</param>
        /// <param name="delta_t">The amount of time it takes the change to happen</param>
        /// <returns>The acceleration of an object</returns>
        public static float Acceleration(float delta_v, float delta_t)
        {
            if (delta_t <= 0)
                throw new Exception("Error, delta_t must be greater than zero.");
            return delta_v / delta_t;
        }

        /// <summary>
        /// Implementation of the formula: v*v = v0*v0 + 2*a*delta_x.
        /// </summary>
        /// <param name="v">The object's final velocity</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="a">The object's acceleration</param>
        /// <param name="delta_x">The change in the object's displacement</param>
        public static void VelocitySquared(out float v, float v0, float a, float delta_x)
        {
            v = (float)Math.Sqrt(Math.Pow(v0, 2) + (2 * a * delta_x));
        }

        /// <summary>
        /// Implementation of the formula: v*v = v0*v0 + 2*a*delta_x.
        /// </summary>
        /// <param name="v">The object's final velocity</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="a">The object's acceleration</param>
        /// <param name="delta_x">The change in the object's displacement</param>
        public static void VelocitySquared(float v, out float v0, float a, float delta_x)
        {
            v0 = (float)Math.Sqrt(Math.Pow(v, 2) - (2 * a * delta_x));
        }

        /// <summary>
        /// Implementation of the formula: v*v = v0*v0 + 2*a*delta_x.
        /// </summary>
        /// <param name="v">The object's final velocity</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="a">The object's acceleration</param>
        /// <param name="delta_x">The change in the object's displacement</param>
        public static void VelocitySquared(float v, float v0, out float a, float delta_x)
        {
            a = (float)((Math.Pow(v, 2) - Math.Pow(v0, 2)) / (2 * delta_x));
        }

        /// <summary>
        /// Implementation of the formula: v*v = v0*v0 + 2*a*delta_x.
        /// </summary>
        /// <param name="v">The object's final velocity</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="a">The object's acceleration</param>
        /// <param name="delta_x">The change in the object's displacement</param>
        public static void VelocitySquared(float v, float v0, float a, out float delta_x)
        {
            delta_x = (float)((Math.Pow(v, 2) - Math.Pow(v0, 2)) / (2 * a));
        }

        /// <summary>
        /// Implementation of the formula: delta_x = v0*t + 1/2*a*t*t.
        /// </summary>
        /// <param name="delta_x">The change in the object's displacement</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="t">The amount of time it takes for the change to occur</param>
        /// <param name="a">The object's acceleration</param>
        public static void Displacement(out float delta_x, float v0, float t, float a)
        {
            delta_x = (float)(v0 * t + 0.5 * a * Math.Pow(t, 2));
        }

        /// <summary>
        /// Implementation of the formula: delta_x = v0*t + 1/2*a*t*t.
        /// </summary>
        /// <param name="delta_x">The change in the object's displacement</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="t">The amount of time it takes for the change to occur</param>
        /// <param name="a">The object's acceleration</param>
        public static void Displacement(float delta_x, out float v0, float t, float a)
        {
            v0 = (float)((delta_x - 0.5 * a * Math.Pow(t, 2)) / t);
        }

        /// <summary>
        /// Implementation of the formula: delta_x = v0*t + 1/2*a*t*t.
        /// </summary>
        /// <param name="delta_x">The change in the object's displacement</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="t">The amount of time it takes for the change to occur</param>
        /// <param name="a">The object's acceleration</param>
        public static void Displacement(float delta_x, float v0, out float t, float a)
        {
            float t1 = (float)((-v0 + Math.Sqrt(Math.Pow(v0, 2) - 2 * a * delta_x)) / a);
            float t2 = (float)((-v0 - Math.Sqrt(Math.Pow(v0, 2) - 2 * a * delta_x)) / a);

            if (t1 > 0)
            {
                t = t1;
            }
            else if (t2 > 0)
            {
                t = t2;
            }
            else
            {
                throw new Exception("Error! Impossible situation.");
            }
        }

        /// <summary>
        /// Implementation of the formula: delta_x = v0*t + 1/2*a*t*t.
        /// </summary>
        /// <param name="delta_x">The change in the object's displacement</param>
        /// <param name="v0">The object's initial velocity</param>
        /// <param name="t">The amount of time it takes for the change to occur</param>
        /// <param name="a">The object's acceleration</param>
        public static void Displacement(float delta_x, float v0, float t, out float a)
        {
            a = (float)((delta_x - v0 * t) / (0.5 * Math.Pow(t, 2)));
        }
    }
}
