﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Turbo2D.Formulas
{
    /// <summary>
    /// Provides two-dimensional vector equations solved for different variables.
    /// </summary>
    class TwoDimensionalVector
    {
        /// <summary>
        /// Returns the magnitude of a two-dimensional vector.
        /// </summary>
        /// <param name="vector_x">The x-component of the vector</param>
        /// <param name="vector_y">The y-component of the vector</param>
        public static float Magnitude(float vector_x, float vector_y)
        {
            return (float)Math.Sqrt(Math.Pow(vector_x, 2) + Math.Pow(vector_y, 2));
        }

        /// <summary>
        /// Returns the heading of a two-dimensional vector.
        /// </summary>
        /// <param name="vector_x">The x-component of the vector</param>
        /// <param name="vector_y">The y-component of the vector</param>
        /// <returns></returns>
        public static float Heading(float vector_x, float vector_y)
        {
            if (vector_x == 0)
            {
                if (vector_y <= 0)
                    return (float)(Math.PI / 2);
                else if (vector_y > 0)
                    return (float)(Math.PI * 1.5);
            }
            else
            {
                float rawHeading = (float)Math.Atan((double)(-vector_y / vector_x));
                if (vector_x < 0)
                    return rawHeading + (float)Math.PI;
                else if (vector_y < 0)
                    return rawHeading + (float)(Math.PI * 2);
                else
                    return rawHeading;
            }
            return 0f;
        }

        /// <summary>
        /// Returns the x-component of a two-dimensional vector.
        /// </summary>
        /// <param name="magnitude">The magnitude of the vector</param>
        /// <param name="heading">The heading of the vector</param>
        /// <returns></returns>
        public static float XComponent(float magnitude, float heading)
        {
            return magnitude * (float)Math.Cos(heading);
        }

        /// <summary>
        /// Returns the y-component of a two-dimensional vector.
        /// </summary>
        /// <param name="magnitude">The magnitude of the vector</param>
        /// <param name="heading">The heading of the vector</param>
        /// <returns></returns>
        public static float YComponent(float magnitude, float heading)
        {
            return magnitude * (float)-Math.Sin(heading);
        }
    }
}
