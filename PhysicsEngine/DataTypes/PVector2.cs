﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Formulas;

namespace Turbo2D.DataTypes
{
    public class PVector2
    {
        /// <summary>
        /// The X-coordinate of the vector
        /// </summary>
        public float X { get { return x; } set { x = value; CoordinateChange(); } }
        protected float x;
        /// <summary>
        /// The Y-coordinate of the vector
        /// </summary>
        public float Y { get { return y; } set { y = value; CoordinateChange(); } }
        protected float y;
        /// <summary>
        /// The angle from the positive X-axis to the vector
        /// </summary>
        public float Heading { get { return heading; } set { heading = value; AngleLengthChange(); } }
        protected float heading;
        /// <summary>
        /// The length of the vector
        /// </summary>
        public float Magnitude { get { return magnitude; } set { magnitude = value; AngleLengthChange(); } }
        protected float magnitude;

        public PVector2(float x, float y)
        {
            this.x = x;
            Y = y;
        }

        /// <summary>
        /// Updates the magnitude and direction when the X or Y-coordinate changes
        /// </summary>
        protected void CoordinateChange()
        {
            magnitude = TwoDimensionalVector.Magnitude(X, Y);
            heading = TwoDimensionalVector.Heading(X, Y);
        }

        /// <summary>
        /// Updates the X and Y-coordinates when the magnitude or direction changes
        /// </summary>
        protected void AngleLengthChange()
        {
            X = TwoDimensionalVector.XComponent(Magnitude, Heading);
            Y = TwoDimensionalVector.YComponent(Magnitude, Heading);
        }

        public static PVector2 operator +(PVector2 a, PVector2 b)
        {
            return new PVector2(a.X + b.X, a.Y + b.Y);
        }

        public static PVector2 operator -(PVector2 a, PVector2 b)
        {
            return new PVector2(a.X - b.X, a.Y - b.Y);
        }

        public static PVector2 operator *(PVector2 a, float b)
        {
            return new PVector2(a.X * b, a.Y * b);
        }

        public static float operator *(PVector2 a, PVector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static PVector2 operator /(PVector2 a, float b)
        {
            return new PVector2(a.X / b, a.Y / b);
        }
    }
}
