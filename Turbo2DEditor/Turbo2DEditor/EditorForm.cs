﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Turbo2D;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Microsoft.Xna.Framework;
using Turbo2D.Interfaces;
using Turbo2D.GameObjects;

namespace Turbo2D
{
    public partial class EditorForm : Form
    {
        public IntPtr ViewPtr { get { return ViewPanel.Handle; } }

        private Dictionary<string, object> Objects = new Dictionary<string, object>();

        BaseEngine Game;
        EditorScreen Screen;

        public EditorForm(BaseEngine game, EditorScreen screen)
        {
            InitializeComponent();

            this.Shown += new EventHandler(EditorForm_Shown);
            // Register node selection event
            ObjectView.AfterSelect += new TreeViewEventHandler(ObjectView_AfterSelect);
            // Register property value change event
            ObjectProperties.PropertyValueChanged += new PropertyValueChangedEventHandler(ObjectProperties_PropertyValueChanged);

            Game = game;
            Screen = screen;

            // Size view panel
            ViewPanel.Width = Game.DefaultViewport.Width;
            ViewPanel.Height = Game.DefaultViewport.Height;

            // Add objects to dictionary
            //Objects.Add("Screens Below", Screen.ScreenBelowOptions);
            //Objects.Add("Transition", Screen.Transition);

            // Add game settings to object view
            /*ObjectView.Nodes.Add("Screen", "Screen");
            ObjectView.Nodes["Screen"].Nodes.Add("Transition");
            ObjectView.Nodes.Add("Viewports", "Viewports");
            ObjectView.Nodes.Add("RenderGroups", "RenderGroups");
            ObjectView.Nodes.Add("Managers", "Managers");
            ObjectView.Nodes.Add("GameObjects", "GameObjects");*/
        }

        void EditorForm_Shown(object sender, EventArgs e)
        {
            // Add the Viewports to the tree view
            foreach (RenderViewport viewport in Screen.Viewports)
            {
                string formattedName = string.Format("Viewport_{0}", viewport.Name);
                string cameraManagerName = formattedName + "_Cameras";
                Objects.Add(formattedName, viewport);
                ObjectView.Nodes["Viewports"].Nodes.Add(formattedName, viewport.Name);
                ObjectView.Nodes["Viewports"].Nodes[formattedName].Name = formattedName;
                ObjectView.Nodes["Viewports"].Nodes[formattedName].Nodes.Add(cameraManagerName, "Cameras");
                ObjectView.Nodes["Viewports"].Nodes[formattedName].Nodes[cameraManagerName].Name = cameraManagerName;

                Objects.Add(formattedName + "_Cameras", viewport.Cameras);
                foreach (Camera2D camera in viewport.Cameras)
                {
                    string cameraName = string.Format("Viewport_{0}.Camera_{1}", viewport.Name, camera.Name);
                    Objects.Add(cameraName, camera);
                    ObjectView.Nodes["Viewports"].Nodes[formattedName].Nodes[cameraManagerName].Nodes.Add(cameraName, camera.Name);
                    ObjectView.Nodes["Viewports"].Nodes[formattedName].Nodes[cameraManagerName].Nodes[cameraName].Name = cameraName;
                }
            }

            // Add the RenderGroups to the tree view
            foreach (RenderGroup group in Screen.RenderDevice.RenderGroups)
            {
                string formattedName = string.Format("RenderGroup_{0}", group.Name);
                Objects.Add(formattedName, group);
                ObjectView.Nodes["RenderGroups"].Nodes.Add(formattedName, group.Name);
                ObjectView.Nodes["RenderGroups"].Nodes[formattedName].Name = formattedName;
            }

            // Add the Managers to the tree view
            foreach (IManager manager in Screen.Managers)
            {
                string formattedName = string.Format("Manager_{0}", manager.ManagedType.ToString());
                Objects.Add(formattedName, manager);
                ObjectView.Nodes["Managers"].Nodes.Add(formattedName, manager.ManagedType.ToString());
                ObjectView.Nodes["Managers"].Nodes[formattedName].Name = formattedName;
            }

            // Add the GameObjects to the tree view
            foreach (GameObject gObject in Screen.GameObjects)
            {
                string formattedName = string.Format("GameObjects_{0}", gObject.Name);
                Objects.Add(formattedName, gObject);
                ObjectView.Nodes["GameObjects"].Nodes.Add(formattedName, gObject.Name);
                ObjectView.Nodes["GameObjects"].Nodes[formattedName].Name = formattedName;
            }
        }

        void ObjectProperties_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // Save the screen
            Screen.SaveChanges();
        }

        private void HideAllGroupControls()
        {
            grpViewports.Visible = false;
            grpRenderGroups.Visible = false;
            grpManagers.Visible = false;
            grpGameObjects.Visible = false;
        }

        void ObjectView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // Display properties for selected object
            if (Objects.ContainsKey(e.Node.Name))
            {
                ObjectProperties.SelectedObject = Objects[e.Node.Name];
            }
            else
            {
                // Don't display any properties
                ObjectProperties.SelectedObject = null;

                HideAllGroupControls();

                // Display the appropriate controls if it is a main group
                if (e.Node.Text == "Viewports")
                    grpViewports.Visible = true;
                else if (e.Node.Text == "RenderGroups")
                    grpRenderGroups.Visible = true;
                else if (e.Node.Text == "Managers")
                    grpManagers.Visible = true;
                else if (e.Node.Text == "GameObjects")
                    grpGameObjects.Visible = true;
                else
                {
                    grpViewports.Visible = false;
                    grpRenderGroups.Visible = false;
                    grpManagers.Visible = false;
                    grpGameObjects.Visible = false;
                }
            }
        }
    }
}
