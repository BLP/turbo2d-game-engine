﻿using Turbo2D;
using Turbo2D.Serialization;
using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Input;

namespace Turbo2D
{
    class EditorGame : TurboGame
    {
        EditorForm editor;
        EditorScreen screen;

        public EditorGame(string settingsFile, string screenSaveFile)
        {
            // Create the first GameScreen. TODO: Insert the type for your first GameScreen.
            screen = new EditorScreen(screenSaveFile);

            // Create the settings for the game. TODO: Insert the title of your game, and adjust any settings that need it.
            Settings settings = Settings.Load(settingsFile);
            settings.Graphics_IsFullScreen = false;
            settings.Game_IsMouseVisible = true;

            this.InitializeEvent += Initialize;
            this.DrawEvent += Draw;

            // Get the Engine running.
            SetupEngine(screen, settings);
        }

        private void Initialize(object sender, EventArgs e)
        {
            // Hook onto show event
            Form gameForm = (Form)Form.FromHandle(Engine.Window.Handle);
            gameForm.Shown += new EventHandler(gameForm_Shown);

            // Create EditorForm
            editor = new EditorForm(Engine, screen);
            editor.HandleDestroyed += new EventHandler(editor_HandleDestroyed);
            editor.Show();
            Mouse.WindowHandle = editor.ViewPtr;
        }

        void editor_HandleDestroyed(object sender, EventArgs e)
        {
            Engine.Exit();
        }

        void gameForm_Shown(object sender, EventArgs e)
        {
            ((Form)sender).Hide();
        }

        private void Draw(object sender, DrawEventArgs e)
        {
            Engine.GraphicsDevice.Present(null, null, editor.ViewPtr);
        }
    }
}