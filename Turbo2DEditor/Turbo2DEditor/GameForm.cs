﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Turbo2D.Serialization;
using System.Threading;
using System.IO;

namespace Turbo2D
{
    public partial class GameForm : Form
    {
        Settings SettingsFile;
        GameData GameInfo;
        string SavePath;

        public GameForm()
        {
            InitializeComponent();

            // Add settings change event
            GameSettings.PropertyValueChanged += new PropertyValueChangedEventHandler(GameSettings_PropertyValueChanged);

            // Add selected index changed event
            lstGameScreens.SelectedIndexChanged += new EventHandler(lstGameScreens_SelectedIndexChanged);
        }

        void lstGameScreens_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstGameScreens.SelectedIndex != -1)
                GameScreenButtonsEnable(true);
            else
                GameScreenButtonsEnable(false);
        }

        void GameSettings_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            // Save the settings file
            SaveSettings();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            // Get game name from user
            NameInput input = new NameInput("Please enter the game's name:");
            DialogResult result = input.ShowDialog();

            if (result == DialogResult.OK)
            {
                // Get save location
                DialogResult result2 = FolderBrowser.ShowDialog();

                if (result2 == DialogResult.OK)
                {
                    // Create new GameData
                    GameInfo = GameData.CreateDefault();
                    SetGameName(input.Result);

                    // Set save path
                    SavePath = FolderBrowser.SelectedPath;

                    // Save game info
                    SaveGameData();

                    // Create new settings file and load into settings viewer
                    SettingsFile = Settings.CreateDefault();
                    GameSettings.SelectedObject = SettingsFile;

                    // Save settings file
                    SaveSettings();

                    // Enable controls
                    ControlsEnable(true);
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Get name for screen
            NameInput input = new NameInput("Please enter the screen's name:");
            DialogResult result = input.ShowDialog();

            if (result == DialogResult.OK && !lstGameScreens.Items.Contains(input.Result))
            {
                // Add the new screen to the list
                lstGameScreens.Items.Add(input.Result);

                // Add the new screen to the GameData
                GameInfo.Screens.Add(input.Result);
                SaveGameData();

                // Create new thread and create screen editor
                Thread editorThread = new Thread(() => new EditorGame(string.Format("{0}\\{1}", SavePath, GameInfo.SettingsFile),
                    string.Format("{0}\\{1}.dat", SavePath, input.Result)));
                editorThread.Start();
            }
            else if (lstGameScreens.Items.Contains(input.Result))
            {
                // Display error message
                MessageBox.Show("Error! Screen name already in use.", "Turbo2D Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ControlsEnable(bool enable)
        {
            GameSettings.Enabled = enable;
            grpScreens.Enabled = enable;
        }

        private void GameScreenButtonsEnable(bool enable)
        {
            btnEdit.Enabled = enable;
            btnRemove.Enabled = enable;
            btnSetFirst.Enabled = enable;
        }

        private void SaveSettings()
        {
            Settings.Save(SettingsFile, string.Format("{0}\\{1}", SavePath, GameInfo.SettingsFile));
        }

        private void SaveGameData()
        {
            GameData.Save(GameInfo, string.Format("{0}\\{1}.gme", SavePath, GameInfo.Name));
        }
        private void SetFirstScreen(string name)
        {
            GameInfo.FirstScreen = name;
            lblFirstScreen.Text = name;
        }

        private void SetGameName(string name)
        {
            GameInfo.Name = name;
            GameInfo.SettingsFile = name + "Settings.dat";
            lblGameName.Text = name;
        }

        private void btnLoadGame_Click(object sender, EventArgs e)
        {
            // Select GameData file to load
            DialogResult result = OpenFile.ShowDialog();

            if (result == DialogResult.OK)
            {
                // Load GameData file
                GameInfo = GameData.Load(OpenFile.FileName);

                // Display game name
                lblGameName.Text = GameInfo.Name;

                // Extract save path from file location
                SavePath = OpenFile.FileName.Remove(OpenFile.FileName.IndexOf(OpenFile.SafeFileName) - 1);

                // Load settings
                SettingsFile = Settings.Load(string.Format("{0}\\{1}", SavePath, GameInfo.SettingsFile));
                GameSettings.SelectedObject = SettingsFile;

                // Add GameScreens to list
                lstGameScreens.Items.Clear();

                foreach (string screenName in GameInfo.Screens)
                    lstGameScreens.Items.Add(screenName);

                // Display first screen
                lblFirstScreen.Text = GameInfo.FirstScreen;

                // Enable controls
                ControlsEnable(true);
            }
        }

        private void btnSetFirst_Click(object sender, EventArgs e)
        {
            // Set the currently selected screen as the first screen
            GameInfo.FirstScreen = lstGameScreens.SelectedItem.ToString();
            
            // Display first screen
            lblFirstScreen.Text = GameInfo.FirstScreen;

            SaveGameData();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            // Remove the screen from the game's list of screens
            GameInfo.Screens.Remove(lstGameScreens.SelectedItem.ToString());

            // Remove it from being the first screen
            if (GameInfo.FirstScreen == lstGameScreens.SelectedItem.ToString())
            {
                SetFirstScreen("");
            }
            
            // Delete the data file
            File.Delete(string.Format("{0}\\{1}.dat", SavePath, lstGameScreens.SelectedItem.ToString()));

            // Remove from list
            lstGameScreens.Items.Remove(lstGameScreens.SelectedItem);

            SaveGameData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            // Create new thread and create screen editor
            string screenFile = string.Format("{0}\\{1}.dat", SavePath, lstGameScreens.SelectedItem.ToString());
            Thread editorThread = new Thread(() => new EditorGame(string.Format("{0}\\{1}", SavePath, GameInfo.SettingsFile),
                screenFile));
            editorThread.Start();
        }
    }
}
