﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Turbo2D
{
    public partial class NameInput : Form
    {
        public string Result { get { return txtInput.Text; } }

        public NameInput(string prompt)
        {
            InitializeComponent();

            // Set prompt text
            lblPrompt.Text = prompt;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            // Hide form
            Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Hide form
            Hide();
        }
    }
}
