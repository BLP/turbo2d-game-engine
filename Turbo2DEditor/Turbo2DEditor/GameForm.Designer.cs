﻿namespace Turbo2D
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GameSettings = new System.Windows.Forms.PropertyGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.grpScreens = new System.Windows.Forms.GroupBox();
            this.lblFirstScreen = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSetFirst = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lstGameScreens = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGameName = new System.Windows.Forms.Label();
            this.btnNewGame = new System.Windows.Forms.Button();
            this.btnLoadGame = new System.Windows.Forms.Button();
            this.OpenFile = new System.Windows.Forms.OpenFileDialog();
            this.FolderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.grpScreens.SuspendLayout();
            this.SuspendLayout();
            // 
            // GameSettings
            // 
            this.GameSettings.Enabled = false;
            this.GameSettings.Location = new System.Drawing.Point(12, 25);
            this.GameSettings.Name = "GameSettings";
            this.GameSettings.Size = new System.Drawing.Size(232, 303);
            this.GameSettings.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Game Settings:";
            // 
            // grpScreens
            // 
            this.grpScreens.Controls.Add(this.lblFirstScreen);
            this.grpScreens.Controls.Add(this.label3);
            this.grpScreens.Controls.Add(this.btnSetFirst);
            this.grpScreens.Controls.Add(this.btnRemove);
            this.grpScreens.Controls.Add(this.btnEdit);
            this.grpScreens.Controls.Add(this.btnAdd);
            this.grpScreens.Controls.Add(this.lstGameScreens);
            this.grpScreens.Enabled = false;
            this.grpScreens.Location = new System.Drawing.Point(250, 12);
            this.grpScreens.Name = "grpScreens";
            this.grpScreens.Size = new System.Drawing.Size(191, 316);
            this.grpScreens.TabIndex = 4;
            this.grpScreens.TabStop = false;
            this.grpScreens.Text = "Game Screens";
            // 
            // lblFirstScreen
            // 
            this.lblFirstScreen.AutoSize = true;
            this.lblFirstScreen.Location = new System.Drawing.Point(71, 297);
            this.lblFirstScreen.Name = "lblFirstScreen";
            this.lblFirstScreen.Size = new System.Drawing.Size(0, 13);
            this.lblFirstScreen.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 297);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "First Screen:";
            // 
            // btnSetFirst
            // 
            this.btnSetFirst.Enabled = false;
            this.btnSetFirst.Location = new System.Drawing.Point(47, 267);
            this.btnSetFirst.Name = "btnSetFirst";
            this.btnSetFirst.Size = new System.Drawing.Size(97, 23);
            this.btnSetFirst.TabIndex = 7;
            this.btnSetFirst.Text = "Set First Screen";
            this.btnSetFirst.UseVisualStyleBackColor = true;
            this.btnSetFirst.Click += new System.EventHandler(this.btnSetFirst_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Enabled = false;
            this.btnRemove.Location = new System.Drawing.Point(129, 238);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(55, 23);
            this.btnRemove.TabIndex = 6;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Enabled = false;
            this.btnEdit.Location = new System.Drawing.Point(68, 237);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(55, 23);
            this.btnEdit.TabIndex = 5;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(7, 238);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(55, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lstGameScreens
            // 
            this.lstGameScreens.FormattingEnabled = true;
            this.lstGameScreens.Location = new System.Drawing.Point(6, 19);
            this.lstGameScreens.Name = "lstGameScreens";
            this.lstGameScreens.Size = new System.Drawing.Size(178, 212);
            this.lstGameScreens.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 365);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Game Name:";
            // 
            // lblGameName
            // 
            this.lblGameName.AutoSize = true;
            this.lblGameName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGameName.Location = new System.Drawing.Point(95, 365);
            this.lblGameName.Name = "lblGameName";
            this.lblGameName.Size = new System.Drawing.Size(0, 16);
            this.lblGameName.TabIndex = 6;
            // 
            // btnNewGame
            // 
            this.btnNewGame.Location = new System.Drawing.Point(12, 385);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(75, 23);
            this.btnNewGame.TabIndex = 7;
            this.btnNewGame.Text = "Create New";
            this.btnNewGame.UseVisualStyleBackColor = true;
            this.btnNewGame.Click += new System.EventHandler(this.btnNewGame_Click);
            // 
            // btnLoadGame
            // 
            this.btnLoadGame.Location = new System.Drawing.Point(94, 384);
            this.btnLoadGame.Name = "btnLoadGame";
            this.btnLoadGame.Size = new System.Drawing.Size(81, 23);
            this.btnLoadGame.TabIndex = 8;
            this.btnLoadGame.Text = "Load Existing";
            this.btnLoadGame.UseVisualStyleBackColor = true;
            this.btnLoadGame.Click += new System.EventHandler(this.btnLoadGame_Click);
            // 
            // OpenFile
            // 
            this.OpenFile.Filter = "Game files|*.gme";
            this.OpenFile.Title = "Load Game";
            // 
            // FolderBrowser
            // 
            this.FolderBrowser.Description = "Select the save location for this game\'s data";
            this.FolderBrowser.RootFolder = System.Environment.SpecialFolder.MyDocuments;
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 453);
            this.Controls.Add(this.btnLoadGame);
            this.Controls.Add(this.btnNewGame);
            this.Controls.Add(this.lblGameName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.grpScreens);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GameSettings);
            this.Name = "GameForm";
            this.Text = "Turbo2D Game Editor";
            this.grpScreens.ResumeLayout(false);
            this.grpScreens.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PropertyGrid GameSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpScreens;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ListBox lstGameScreens;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGameName;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.Button btnLoadGame;
        private System.Windows.Forms.OpenFileDialog OpenFile;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowser;
        private System.Windows.Forms.Button btnSetFirst;
        private System.Windows.Forms.Label lblFirstScreen;
        private System.Windows.Forms.Label label3;
    }
}