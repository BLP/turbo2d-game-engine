﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using System.ComponentModel;
using System.Drawing.Design;
using Turbo2D.Serialization;
using Turbo2D.Interfaces;
using System.IO;
using Microsoft.Xna.Framework.Input;

namespace Turbo2D
{
    public class TransitionSettings
    {
        public TimeSpan TransitionOnTime { get { return OnTime; } set { OnTime = value; } }
        public TimeSpan TransitionOffTime { get { return OffTime; } set { OffTime = value; } }

        private TimeSpan OnTime;
        private TimeSpan OffTime;

        public TransitionSettings(TimeSpan onTime, TimeSpan offTime)
        {
            OnTime = onTime;
            OffTime = offTime;
        }
    }

    public class ScreenBelow
    {
        public bool AllowUpdateBelow { get { return allowUpdateBelow; } set { allowUpdateBelow = value; } }
        public bool AllowDrawBelow { get { return allowDrawBelow; } set { allowDrawBelow = value; } }
        public bool AllowInputBelow { get { return allowInputBelow; } set { allowInputBelow = value; } }

        private bool allowUpdateBelow;
        private bool allowDrawBelow;
        private bool allowInputBelow;

        public ScreenBelow()
        {
        }
    }

    public class EditorScreen : GameScreen
    {
        public TransitionSettings Transition;
        public ScreenBelow ScreenBelowOptions;
        private string SaveFile;
        private bool DefaultRenderGroups = true;

        public EditorScreen(string saveFile)
        {
            Transition = new TransitionSettings(TransitionOnTime, TransitionOffTime);
            ScreenBelowOptions = new ScreenBelow();
            SaveFile = saveFile;
        }

        protected internal override void LoadContent(ContentManager Content, GameScreenHelper parent)
        {
            // Do the automatic loading.
            base.LoadContent(Content, parent);

            // Attempt to load screen
            Load(SaveFile);

            // Save initial state
            Save(SaveFile);

            // Set what GameScreens below this one are allowed to do.
            /*AllowUpdateBelow = false;
            AllowInputBelow = false;
            AllowDrawBelow = false;

            // Set the transiton times.
            TransitionOnTime = TimeSpan.FromSeconds(0);
            TransitionOffTime = TimeSpan.FromSeconds(0);*/

            // TODO: Create an load your GameObjects here.
        }

        protected override void UpdateOnTransition(GameTime gameTime)
        {
            // TODO: Add your code to create the transiton on effect here.
        }

        protected override void UpdateActive(GameTime gameTime)
        {
            // TODO: Add your code to be executed when the GameScreen is active here.
        }

        protected override void UpdateOffTransition(GameTime gameTime)
        {
            // TODO: Add your code to create the transition off effect here.
        }

        public void AddRenderGroup(string name, bool cameraAffected, int drawOrder)
        {
            RenderDevice.RenderGroups.Add(name, cameraAffected, drawOrder);

            if (DefaultRenderGroups)
            {
                // Remove default render groups
                RenderDevice.RenderGroups.Remove("Default", name);

                DefaultRenderGroups = false;
            }
        }

        public void SaveChanges()
        {
            Save(SaveFile);
        }
    }
}
