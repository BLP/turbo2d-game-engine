﻿namespace Turbo2D
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Screen");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Viewports");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("RenderGroups");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Managers");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("GameObjects");
            this.ObjectView = new System.Windows.Forms.TreeView();
            this.ObjectProperties = new System.Windows.Forms.PropertyGrid();
            this.ViewPanel = new System.Windows.Forms.Panel();
            this.grpRenderGroups = new System.Windows.Forms.GroupBox();
            this.btnAddRenderGroup = new System.Windows.Forms.Button();
            this.grpManagers = new System.Windows.Forms.GroupBox();
            this.grpGameObjects = new System.Windows.Forms.GroupBox();
            this.grpViewports = new System.Windows.Forms.GroupBox();
            this.grpRenderGroups.SuspendLayout();
            this.grpManagers.SuspendLayout();
            this.grpGameObjects.SuspendLayout();
            this.SuspendLayout();
            // 
            // ObjectView
            // 
            this.ObjectView.Dock = System.Windows.Forms.DockStyle.Left;
            this.ObjectView.Location = new System.Drawing.Point(0, 0);
            this.ObjectView.Name = "ObjectView";
            treeNode1.Name = "Screen";
            treeNode1.Text = "Screen";
            treeNode2.Name = "Viewports";
            treeNode2.Text = "Viewports";
            treeNode3.Name = "RenderGroups";
            treeNode3.Text = "RenderGroups";
            treeNode4.Name = "Managers";
            treeNode4.Text = "Managers";
            treeNode5.Name = "GameObjects";
            treeNode5.Text = "GameObjects";
            this.ObjectView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5});
            this.ObjectView.Size = new System.Drawing.Size(166, 432);
            this.ObjectView.TabIndex = 0;
            // 
            // ObjectProperties
            // 
            this.ObjectProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.ObjectProperties.Location = new System.Drawing.Point(408, 0);
            this.ObjectProperties.Name = "ObjectProperties";
            this.ObjectProperties.Size = new System.Drawing.Size(288, 432);
            this.ObjectProperties.TabIndex = 1;
            // 
            // ViewPanel
            // 
            this.ViewPanel.Location = new System.Drawing.Point(172, 0);
            this.ViewPanel.Name = "ViewPanel";
            this.ViewPanel.Size = new System.Drawing.Size(800, 600);
            this.ViewPanel.TabIndex = 2;
            // 
            // grpRenderGroups
            // 
            this.grpRenderGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpRenderGroups.Controls.Add(this.btnAddRenderGroup);
            this.grpRenderGroups.Controls.Add(this.grpManagers);
            this.grpRenderGroups.Location = new System.Drawing.Point(0, 332);
            this.grpRenderGroups.Name = "grpRenderGroups";
            this.grpRenderGroups.Size = new System.Drawing.Size(166, 100);
            this.grpRenderGroups.TabIndex = 3;
            this.grpRenderGroups.TabStop = false;
            this.grpRenderGroups.Text = "RenderGroups";
            this.grpRenderGroups.Visible = false;
            // 
            // btnAddRenderGroup
            // 
            this.btnAddRenderGroup.Location = new System.Drawing.Point(34, 37);
            this.btnAddRenderGroup.Name = "btnAddRenderGroup";
            this.btnAddRenderGroup.Size = new System.Drawing.Size(75, 23);
            this.btnAddRenderGroup.TabIndex = 0;
            this.btnAddRenderGroup.Text = "Add New";
            this.btnAddRenderGroup.UseVisualStyleBackColor = true;
            // 
            // grpManagers
            // 
            this.grpManagers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpManagers.Controls.Add(this.grpGameObjects);
            this.grpManagers.Location = new System.Drawing.Point(0, 0);
            this.grpManagers.Name = "grpManagers";
            this.grpManagers.Size = new System.Drawing.Size(166, 100);
            this.grpManagers.TabIndex = 4;
            this.grpManagers.TabStop = false;
            this.grpManagers.Text = "Add Manager";
            this.grpManagers.Visible = false;
            // 
            // grpGameObjects
            // 
            this.grpGameObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpGameObjects.Controls.Add(this.grpViewports);
            this.grpGameObjects.Location = new System.Drawing.Point(0, 0);
            this.grpGameObjects.Name = "grpGameObjects";
            this.grpGameObjects.Size = new System.Drawing.Size(166, 100);
            this.grpGameObjects.TabIndex = 4;
            this.grpGameObjects.TabStop = false;
            this.grpGameObjects.Text = "Add GameObject";
            this.grpGameObjects.Visible = false;
            // 
            // grpViewports
            // 
            this.grpViewports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.grpViewports.Location = new System.Drawing.Point(0, 0);
            this.grpViewports.Name = "grpViewports";
            this.grpViewports.Size = new System.Drawing.Size(166, 100);
            this.grpViewports.TabIndex = 4;
            this.grpViewports.TabStop = false;
            this.grpViewports.Text = "Add Vieport";
            this.grpViewports.Visible = false;
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 432);
            this.Controls.Add(this.grpRenderGroups);
            this.Controls.Add(this.ObjectProperties);
            this.Controls.Add(this.ViewPanel);
            this.Controls.Add(this.ObjectView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "EditorForm";
            this.Text = "Turbo2D GameScreen Editor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.grpRenderGroups.ResumeLayout(false);
            this.grpManagers.ResumeLayout(false);
            this.grpGameObjects.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView ObjectView;
        private System.Windows.Forms.PropertyGrid ObjectProperties;
        private System.Windows.Forms.Panel ViewPanel;
        private System.Windows.Forms.GroupBox grpRenderGroups;
        private System.Windows.Forms.GroupBox grpViewports;
        private System.Windows.Forms.GroupBox grpManagers;
        private System.Windows.Forms.GroupBox grpGameObjects;
        private System.Windows.Forms.Button btnAddRenderGroup;
    }
}