﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Turbo2D.GameObjects;
using System.Reflection;

namespace ScreenEditor
{
    public partial class ScreenEditor : Form
    {
        protected Dictionary<string, Type> GameObjectTypes = new Dictionary<string, Type>();
        protected const string DefaultScreen = "using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\n" +
            "using Microsoft.Xna.Framework;\r\nusing Microsoft.Xna.Framework.Graphics;\r\nusing Microsoft.Xna.Framework.Content;\r\nusing Turbo2D.Helpers;\r\n" +
            "using Turbo2D.Implementation;\r\nusing Turbo2D.Implementation.Menus;\r\nusing Turbo2D.Implementation.ScreenTypes;\r\n\r\nnamespace {0}\r\n{\r\n" +
            "\tpublic class {1} : GameScreen\r\n\t{\r\n\t\tpublic {1}()\r\n\t\t{\r\n\t\t}\r\n\t}\r\n}";

        public ScreenEditor()
        {
            InitializeComponent();
            LoadAssembly("C:\\Users\\nace\\Documents\\Visual Studio 2008\\Projects\\Super Kart\\Turbo2D\\bin\\x86\\Release\\Turbo2D.dll");
        }

        private void btnLoadAssem_Click(object sender, EventArgs e)
        {
            DialogResult result = OpenFile.ShowDialog();
            if (result == DialogResult.OK)
                LoadAssembly(OpenFile.FileName);
        }

        protected void LoadAssembly(string path)
        {
            try
            {
                AssemblyName name = AssemblyName.GetAssemblyName(path);
                Assembly assembly = Assembly.Load(name);
                Type[] types = assembly.GetExportedTypes();
                for (int i = 0; i < types.Length; i++)
                {
                    if (!types[i].IsAbstract)
                    {
                        Type currType = types[i];
                        while (currType.BaseType != typeof(Object) && currType.BaseType != null)
                            currType = currType.BaseType;
                        if (currType.Name == "GameObject")
                        {
                            GameObjectTypes.Add(types[i].Name, types[i]);
                            lstGameObjects.Items.Add(types[i].Name);
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error loading assembly", "Turbo2D GameScreen Editor", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            CreateNew dialog = new CreateNew();
            dialog.ShowDialog();
            StringBuilder builder = new StringBuilder(DefaultScreen);
            builder.Replace("{0}", dialog.txtNamespace.Text);
            builder.Replace("{1}", dialog.txtName.Text);
            txtCode.Text = builder.ToString();
        }
    }
}
