﻿namespace ScreenEditor
{
    partial class ScreenEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoadAssem = new System.Windows.Forms.Button();
            this.OpenFile = new System.Windows.Forms.OpenFileDialog();
            this.lstGameObjects = new System.Windows.Forms.ListBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLoadAssem
            // 
            this.btnLoadAssem.Location = new System.Drawing.Point(13, 13);
            this.btnLoadAssem.Name = "btnLoadAssem";
            this.btnLoadAssem.Size = new System.Drawing.Size(97, 23);
            this.btnLoadAssem.TabIndex = 0;
            this.btnLoadAssem.Text = "Load Assembly";
            this.btnLoadAssem.UseVisualStyleBackColor = true;
            this.btnLoadAssem.Click += new System.EventHandler(this.btnLoadAssem_Click);
            // 
            // OpenFile
            // 
            this.OpenFile.Filter = "dll Files (*.dll)|*.dll|exe Files (*.exe)|*.exe";
            // 
            // lstGameObjects
            // 
            this.lstGameObjects.FormattingEnabled = true;
            this.lstGameObjects.HorizontalScrollbar = true;
            this.lstGameObjects.Location = new System.Drawing.Point(1, 53);
            this.lstGameObjects.Name = "lstGameObjects";
            this.lstGameObjects.Size = new System.Drawing.Size(145, 108);
            this.lstGameObjects.TabIndex = 1;
            // 
            // txtCode
            // 
            this.txtCode.AcceptsReturn = true;
            this.txtCode.AcceptsTab = true;
            this.txtCode.Location = new System.Drawing.Point(290, 12);
            this.txtCode.Multiline = true;
            this.txtCode.Name = "txtCode";
            this.txtCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCode.Size = new System.Drawing.Size(536, 524);
            this.txtCode.TabIndex = 2;
            this.txtCode.WordWrap = false;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(1, 188);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 3;
            this.btnCreate.Text = "Create New";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(1, 217);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 4;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // ScreenEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 548);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lstGameObjects);
            this.Controls.Add(this.btnLoadAssem);
            this.Name = "ScreenEditor";
            this.Text = "Turbo2D GameScreen Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadAssem;
        private System.Windows.Forms.OpenFileDialog OpenFile;
        private System.Windows.Forms.ListBox lstGameObjects;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnLoad;
    }
}

