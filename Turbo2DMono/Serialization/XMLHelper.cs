using System.IO;
using System.Xml.Serialization;

namespace Turbo2D.Serialization
{
    public static class XMLHelper
    {
        public static T Load<T>(string FileName)
        {
            T Object;
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            // To read the file, create a FileStream.
            FileStream myFileStream = new FileStream(FileName, FileMode.Open);
            // Call the Deserialize method and cast to the object type.
            Object = (T)mySerializer.Deserialize(myFileStream);
            myFileStream.Close();
            return Object;
        }

        public static void Save<T>(string FileName, T SaveObject)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            // To write to a file, create a StreamWriter object.
            StreamWriter myWriter = new StreamWriter(FileName);
            mySerializer.Serialize(myWriter, SaveObject);
            myWriter.Close();
        }
    }
}
