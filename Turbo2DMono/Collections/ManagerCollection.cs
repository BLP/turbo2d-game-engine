﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Managers;
using Turbo2D.GameScreens;
using Turbo2D.GameObjects;
using Turbo2D.Interfaces;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of object managers
    /// </summary>
    public sealed class ManagerCollection : List<IManager>
    {
        /// <summary>
        /// Creates a new instance of a ManagerCollection
        /// </summary>
        public ManagerCollection()
        {
        }

        /// <summary>
        /// Adds a GameObject to manage
        /// </summary>
        /// <param name="gameObject">The GameObject to be managed</param>
        public void RegisterGameObject(GameObject gameObject)
        {
            // Loop through each manager and add the GameObject to each manager that manages its type
            foreach (IManager manager in this)
            {
                if (manager.ManagedType.IsInstanceOfType(gameObject))
                    manager.Add(gameObject);
            }
        }

        /// <summary>
        /// Removes a GameObject from management
        /// </summary>
        /// <param name="gameObject">The GameObject to cease being managed</param>
        public void DeregisterGameObject(GameObject gameObject)
        {
            // Loop through each manager and remove the GameObject to each manager that manages its type
            foreach (IManager manager in this)
            {
                if (manager.ManagedType.IsInstanceOfType(gameObject))
                    manager.Remove(gameObject);
            }
        }
    }
}
