﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Interfaces;
using Turbo2D.Helpers;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of RenderGroups
    /// </summary>
    public sealed class RenderGroupCollection : IEnumerable<RenderGroup>
    {
        /// <summary>
        /// The RenderGroups contained in the collection sorted by DrawIndex
        /// </summary>
        private List<RenderGroup> SortedList = new List<RenderGroup>();

        /// <summary>
        /// Get a RenderGroup by name
        /// </summary>
        /// <param name="key">The name of the RenderGroup</param>
        /// <returns>A RenderGroup</returns>
        public RenderGroup this[string key] { get { return RenderGroups[key]; } }

        /// <summary>
        /// Get a RenderGroup by its key in the sorted list
        /// </summary>
        /// <param name="index">The index of the RenderGroup in the sorted list</param>
        /// <returns>A RenderGroup</returns>
        public RenderGroup this[int index] { get { return SortedList[index]; } }

        /// <summary>
        /// The number of RenderGroups in the collection
        /// </summary>
        public int Count { get { return RenderGroups.Count; } }

        // Internal storage of RenderGroups
        private Dictionary<string, RenderGroup> RenderGroups = new Dictionary<string, RenderGroup>();

        /// <summary>
        /// Creates a new instance of a RenderGroupCollection
        /// </summary>
        public RenderGroupCollection()
        {
        }

        /// <summary>
        /// Creates a new RenderGroup for drawing.
        /// </summary>
        /// <param name="groupName">The name of the RenderGroup.</param>
        /// <param name="cameraAffected">If the objects drawn in this RenderGroup are affected by cameras.</param>
        /// <param name="drawOrder">The position in the draw order where this RenderGroup should be drawn.
        /// The higher the index, the earlier in the draw order.</param>
        public void Add(string groupName, bool cameraAffected, int drawOrder)
        {
            // Create the new RenderGroup
            RenderGroup newRenderGroup = new RenderGroup(cameraAffected, drawOrder, groupName);

            // Add it to the dictionary storage
            RenderGroups.Add(newRenderGroup.Name, newRenderGroup);
            // Add it to the sorted list
            SortedList.Add(newRenderGroup);
            // Sort the list
            SortedList.Sort((group1, group2) => group1.DrawIndex - group2.DrawIndex);
        }

        /// <summary>
        /// Adds a GameObject to draw
        /// </summary>
        /// <param name="renderGroups">The RenderGroup to add the GameObject to</param>
        public void RegisterGameObject(IDrawableObject gameObject)
        {
            // Add the GameObject to the RenderGroup it belongs in
            RenderGroups[gameObject.RenderGroup].AddObject(gameObject);
            // Register this with the object's RenderGroupChanged event
            gameObject.RenderGroupChanged += new EventHandler<RenderGroupChangedEventArgs>(gameObject_RenderGroupChanged);
        }

        private void gameObject_RenderGroupChanged(object sender, RenderGroupChangedEventArgs e)
        {
            IDrawableObject drawableObject = sender as IDrawableObject;
            // Remove the object from the old group
            RenderGroups[e.OldGroup].RemoveObject(drawableObject);
            // Add it to the new group
            RenderGroups[e.NewGroup].AddObject(drawableObject);
        }

        /// <summary>
        /// Removes a GameObject from drawing
        /// </summary>
        /// <param name="renderGroups">The RenderGroups to remove the GameObject from</param>
        /// <param name="gameObject">The GameObject to be drawn</param>
        public void DeregisterGameObject(IDrawableObject gameObject)
        {
            // Remove the GameObject from the RenderGroup it belongs in
            RenderGroups[gameObject.RenderGroup].RemoveObject(gameObject);
            // Deregister this with the object's RenderGroupChanged event
            gameObject.RenderGroupChanged -= new EventHandler<RenderGroupChangedEventArgs>(gameObject_RenderGroupChanged);
        }

        /// <summary>
        /// Gets the enumerator for the collection
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<RenderGroup> GetEnumerator()
        {
            return SortedList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return SortedList.GetEnumerator();
        }
    }
}
