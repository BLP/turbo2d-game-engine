﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.Helpers;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.Collections
{
    /// <summary>
    /// Stores a collection of RenderViewports
    /// </summary>
    public sealed class RenderViewportCollection : IEnumerable<RenderViewport>
    {
        /// <summary>
        /// Gets a RenderViewport by name
        /// </summary>
        /// <param name="key">The RenderViewport's name</param>
        /// <returns>The requested RenderViewport</returns>
        public RenderViewport this[string key] { get { return ViewportStorage[key]; } }

        private Dictionary<string, RenderViewport> ViewportStorage = new Dictionary<string, RenderViewport>();

        /// <summary>
        /// The number of RenderViewports in the collection
        /// </summary>
        public int Count { get { return ViewportStorage.Count; } }

        /// <summary>
        /// Creates a new instance of a RenderViewportCollection
        /// </summary>
        public RenderViewportCollection()
        {
        }

        /// <summary>
        /// Adds a RenderViewport to the collection
        /// </summary>
        /// <param name="viewport">The drawing Viewport</param>
        /// <param name="name">The RenderViewport's name</param>
        public void Add(Viewport viewport, string name)
        {
            RenderViewport rViewport = new RenderViewport(viewport, name);
            ViewportStorage.Add(name, rViewport);
        }

        /// <summary>
        /// Adds a RenderViewport to the collection
        /// </summary>
        /// <param name="viewport">The drawing Viewport</param>
        /// <param name="renderGroups">The RenderGroups that are drawn in this viewport</param>
        /// <param name="name">The RenderViewport's name</param>
        public void AddViewport(Viewport viewport, string[] renderGroups, string name)
        {
            RenderViewport rViewport = new RenderViewport(viewport, name, renderGroups);
            ViewportStorage.Add(name, rViewport);
        }

        /// <summary>
        /// Gets the names of all the RenderViewports that a specified RenderGroup appears in
        /// </summary>
        /// <param name="renderGroupName">The name of the render group</param>
        /// <returns>The Viewport names</returns>
        public string[] GetViewportNames(string renderGroupName)
        {
            List<string> names = new List<string>();
            foreach (RenderViewport viewport in this)
            {
                if (viewport.RenderGroups.Contains(renderGroupName))
                    names.Add(viewport.Name);
            }

            return names.ToArray();
        }

        /// <summary>
        /// Gets the enumerator for the collection
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<RenderViewport> GetEnumerator()
        {
            return ViewportStorage.Values.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ViewportStorage.Values.GetEnumerator();
        }
    }
}
