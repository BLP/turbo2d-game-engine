﻿using System;
using Turbo2D.GameScreens;
using Turbo2D.Serialization;

namespace Turbo2D
{
    public abstract class TurboGame
    {
        protected BaseEngine Engine;
        public EventHandler InitializeEvent;
        public EventHandler LoadContentEvent;
        public EventHandler UnloadContentEvent;
        public EventHandler<UpdateEventArgs> UpdateEvent;
        public EventHandler<DrawEventArgs> DrawEvent;
        public EventHandler ActivatedEvent;
        public EventHandler DeactivatedEvent;
        public EventHandler ExitingEvent;

        protected void SetupEngine(GameScreen firstScreen)
        {
            Engine = new BaseEngine(this, firstScreen);
            SetupEvents();
            Engine.Run();
        }

        protected void SetupEngine(GameScreen firstScreen, Settings settings)
        {
            Engine = new BaseEngine(this, firstScreen, settings);
            SetupEvents();
            Engine.Run();
        }

        private void SetupEvents()
        {
            Engine.InitializeEvent += new EventHandler(Initialize);
            Engine.LoadContentEvent += new EventHandler(LoadContent);
            Engine.UnloadContentEvent += new EventHandler(UnloadContent);
            Engine.UpdateEvent += new EventHandler<UpdateEventArgs>(Update);
            Engine.DrawEvent += new EventHandler<DrawEventArgs>(Draw);
            Engine.Activated += new EventHandler<EventArgs>(Activated);
            Engine.Deactivated += new EventHandler<EventArgs>(Deactivated);
            Engine.Exiting += new EventHandler<EventArgs>(Exiting);
        }

        private void Activated(object sender, EventArgs e)
        {
            if (ActivatedEvent != null)
                ActivatedEvent(this, e);
        }

        private void Deactivated(object sender, EventArgs e)
        {
            if (DeactivatedEvent != null)
                DeactivatedEvent(this, e);
        }

        private void Initialize(object sender, EventArgs e)
        {
            if (InitializeEvent != null)
                InitializeEvent(this, e);
        }

        private void LoadContent(object sender, EventArgs e)
        {
            if (LoadContentEvent != null)
                LoadContentEvent(this, e);
        }

        private void UnloadContent(object sender, EventArgs e)
        {
            if (UnloadContentEvent != null)
                UnloadContentEvent(this, e);
        }

        private void Exiting(object sender, EventArgs e)
        {
            if (ExitingEvent != null)
                ExitingEvent(this, e);
        }

        private void Update(object sender, UpdateEventArgs e)
        {
            if (UpdateEvent != null)
                UpdateEvent(this, e);
        }

        private void Draw(object sender, DrawEventArgs e)
        {
            if (DrawEvent != null)
                DrawEvent(this, e);
        }
    }
}
