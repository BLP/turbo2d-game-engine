﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;

namespace Turbo2D.Managers
{
    public class CollisionEventArgs : EventArgs
    {
        public GameObject A { get; protected set; }
        public GameObject B { get; protected set; }

        public CollisionEventArgs(GameObject a, GameObject b)
        {
            A = a;
            B = b;
        }
    }

    public abstract class CollisionManager : Manager<ICollidable>
    {
        //public event EventHandler<CollisionEventArgs> Collision;

        public CollisionManager(GameScreen parent)
            : base(parent)//, true)
        {
        }

        /// <summary>
        /// This function determines whether two objects are eligible to collide. Override this to add more functionality.
        /// </summary>
        /// <param name="collidable1">The first collidable object</param>
        /// <param name="collidable2">The second collidable object</param>
        /// <returns>Whether the two objects can collide</returns>
        protected virtual bool CanCollide(ICollidable collidable1, ICollidable collidable2)
        {
            return (collidable1.Collidable && collidable2.Collidable);
        }
    }
}
