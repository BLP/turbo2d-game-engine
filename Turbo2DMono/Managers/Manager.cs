﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.GameObjects;

namespace Turbo2D.Managers
{
    public abstract class Manager<T> : IManager
    {
        protected GameScreen Parent { get; private set; }
        protected BaseEngine Engine { get; private set; }
        public bool Enabled { get; set; }
        public Type ManagedType { get; private set; }
        protected List<T> Objects = new List<T>();

        public Manager(GameScreen parent)//, bool manageInheritedTypes)
        {
            ManagedType = typeof(T);
            Parent = parent;
            Engine = Parent.Engine;
            Enabled = true;
        }

        public void Add(object managedObject)
        {
            if (managedObject is T)
                Add((T)managedObject);
        }

        protected virtual void Add(T managedObject)
        {
            Objects.Add(managedObject);
        }

        public void Remove(object managedObject)
        {
            Remove((T)managedObject);
        }

        protected virtual void Remove(T managedObject)
        {
            Objects.Remove(managedObject);
        }

        public abstract void Update(GameTime gameTime);
    }
}