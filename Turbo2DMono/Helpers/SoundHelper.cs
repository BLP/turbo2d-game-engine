﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace Turbo2D.Helpers
{
    public class CueDoneEventArgs : EventArgs
    {
        public string CueName { get; protected set; }

        public CueDoneEventArgs(string cueName)
        {
            CueName = cueName;
        }
    }

    public sealed class SoundHelper
    {
        private Dictionary<string, SoundBank> SoundBanks = new Dictionary<string, SoundBank>();
        private Dictionary<string, WaveBank> WaveBanks = new Dictionary<string, WaveBank>();
        private Dictionary<string, AudioEngine> AudioEngines = new Dictionary<string, AudioEngine>();
        private Dictionary<string, AudioCategory> AudioCategories = new Dictionary<string, AudioCategory>();
        private List<Cue> PlayingCues = new List<Cue>();
        private Dictionary<string, SoundEffect> SoundEffects = new Dictionary<string, SoundEffect>();
        private List<SoundEffectInstance> PlayingSoundEffects = new List<SoundEffectInstance>();
        public event EventHandler<CueDoneEventArgs> CueDone;
        private ContentManager Content;

        internal SoundHelper()
        {
        }

        internal void Initialize(ContentManager content)
        {
            Content = content;
        }

        public void AddAudioEngine(string engineFile, string engineName)
        {
            AudioEngines.Add(engineName, new AudioEngine(engineFile));
        }

        public void AddSoundAndWaveBank(string waveBankFile, string waveBankName, string soundBankFile, string soundBankName, string engineName)
        {
            if (AudioEngines.ContainsKey(engineName))
            {
                WaveBanks.Add(waveBankName, new WaveBank(AudioEngines[engineName], waveBankFile));
                SoundBanks.Add(soundBankName, new SoundBank(AudioEngines[engineName], soundBankFile));
            }
            else
                throw new Exception("AudioEngine " + engineName + " does not exist.");
        }

        public void ControlAudioCategory(string categoryName, string engineName)
        {
            if (AudioEngines.ContainsKey(engineName))
            {
                AudioCategories.Add(string.Format("{0}_{1}", categoryName, engineName), AudioEngines[engineName].GetCategory(categoryName));
            }
            else
                throw new Exception("AudioEngine " + engineName + " does not exist.");
        }

        public void SetAudioCategoryVolume(string categoryName, string engineName, float volume)
        {
            if (AudioCategories.ContainsKey(string.Format("{0}_{1}", categoryName, engineName)))
            {
                AudioCategories[string.Format("{0}_{1}", categoryName, engineName)].SetVolume(volume);
            }
            else
                throw new Exception("AudioCategory " + categoryName + "in AudioEngine " + engineName + " is not being controlled.");
        }

        public void LoadSoundEffect(string fileName, string effectName)
        {
            SoundEffects.Add(effectName, Content.Load<SoundEffect>(fileName));
        }

        public SoundEffectInstance PlaySoundEffect(string effectName)
        {
            if (SoundEffects.ContainsKey(effectName))
            {
                SoundEffectInstance instance = SoundEffects[effectName].CreateInstance();
                PlayingSoundEffects.Add(instance);
                return instance;
            }
            else
                throw new Exception(string.Format("SoundEffect name {0} does not exist.", effectName));
        }

        internal void Update()
        {
            foreach (string key in AudioEngines.Keys)
                AudioEngines[key].Update();
            for (int i = 0; i < PlayingCues.Count; i++)
                if (PlayingCues[i].IsStopped)
                {
                    if (CueDone != null)
                        CueDone.Invoke(this, new CueDoneEventArgs(PlayingCues[i].Name));
                    PlayingCues.RemoveAt(i);
                    i--;
                }
        }

        public void PlayCue(string name, string soundBankName)
        {
            if (SoundBanks.ContainsKey(soundBankName))
            {
                Cue cue = SoundBanks[soundBankName].GetCue(name);
                cue.Play();
                PlayingCues.Add(cue);
            }
            else
                throw new Exception("SoundBank " + soundBankName + " does not exist.");
        }

        internal void PauseCues()
        {
            for (int i = 0; i < PlayingCues.Count; i++)
                if (PlayingCues[i].IsPlaying)
                    PlayingCues[i].Pause();
            for (int i = 0; i < PlayingSoundEffects.Count; i++)
                if (PlayingSoundEffects[i].State == SoundState.Playing)
                    PlayingSoundEffects[i].Pause();
        }

        internal void ResumeCues()
        {
            for (int i = 0; i < PlayingCues.Count; i++)
                if (PlayingCues[i].IsPaused)
                    PlayingCues[i].Resume();
            for (int i = 0; i < PlayingSoundEffects.Count; i++)
                if (PlayingSoundEffects[i].State == SoundState.Paused)
                    PlayingSoundEffects[i].Resume();
        }

        internal void Dispose()
        {
            PlayingCues.ForEach(cue => cue.Dispose());
            PlayingSoundEffects.ForEach(instance => instance.Dispose());
            foreach (SoundEffect effect in SoundEffects.Values)
                effect.Dispose();
            foreach (string key in SoundBanks.Keys)
                SoundBanks[key].Dispose();
            foreach (string key in WaveBanks.Keys)
                WaveBanks[key].Dispose();
            foreach (string key in AudioEngines.Keys)
                AudioEngines[key].Dispose();
        }
    }
}
