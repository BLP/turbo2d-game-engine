﻿using System.Collections.Generic;
using Turbo2D.Interfaces;

namespace Turbo2D.Helpers
{
    public class RenderGroup
    {
        private List<IDrawableObject> DrawableObjects = new List<IDrawableObject>();
        public bool Visible { get; set; }
        public bool CameraAffected { get; protected set; }
        public string Name { get; protected set; }
        public int DrawIndex { get; protected set; }

        /// <summary>
        /// Creates a new RenderGroup.
        /// </summary>
        /// <param name="cameraAffected">If the objects drawn in this RenderGroup are affected by cameras.</param>
        /// <param name="drawIndex">The position in the draw order where this RenderGroup should be drawn.
        /// The higher the index, the earlier in the draw order.</param>
        internal RenderGroup(bool cameraAffected, int drawIndex, string name)
        {
            Visible = true;
            CameraAffected = cameraAffected;
            DrawIndex = drawIndex;
            Name = name;
        }

        internal void AddObject(IDrawableObject Object)
        {
            //if (!DrawableObjects.Contains(Object))
            //{
                DrawableObjects.Add(Object);
            //}
        }

        internal void RemoveObject(IDrawableObject Object)
        {
            //if (DrawableObjects.Contains(Object))
                DrawableObjects.Remove(Object);
        }

        internal bool Contains(IDrawableObject Object)
        {
            return DrawableObjects.Contains(Object);
        }

        internal void Draw()
        {
            for (int i = 0; i < DrawableObjects.Count; i++)
            {
                if (DrawableObjects[i].Alive)
                {
                    if (DrawableObjects[i].Visible)
                        DrawableObjects[i].Draw();
                }
                else
                {
                    DrawableObjects.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
