﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;
using System;
using System.Linq;

namespace Turbo2D.Helpers
{
    public sealed class GameScreenHelper
    {
        /// <summary>
        /// The list of active GameScreens
        /// </summary>
        private List<GameScreen> Screens = new List<GameScreen>();
        /// <summary>
        /// The game engine
        /// </summary>
        internal BaseEngine Engine { get; private set; }
        /// <summary>
        /// The GameScreen that is currently on top of the stack
        /// </summary>
        public GameScreen CurrentScreen { get { return Screens[Screens.Count - 1]; } }
        /// <summary>
        /// The number of active GameScreens
        /// </summary>
        public int ScreenCount { get { return Screens.Count; } }

        /// <summary>
        /// Create a new instance of GameScreenHelper
        /// </summary>
        /// <param name="engine">The game engine</param>
        internal GameScreenHelper(BaseEngine engine)
        {
            // Set the engine
            Engine = engine;
        }

        /// <summary>
        /// Adds a GameScreen on top of the stack
        /// </summary>
        /// <param name="screen">The GameScreen to add</param>
        public void AddGameScreen(GameScreen screen)
        {
            // Load the GameScreen
            screen.LoadContent(Engine.Content, this);
            // Add it to the list
            Screens.Add(screen);
        }

        /// <summary>
        /// Inserts a GameScreen one down from the top of the stack
        /// </summary>
        /// <param name="screen">The GameScreen to add</param>
        internal void InsertGameScreen(GameScreen screen)
        {
            // Load the GameScreen
            screen.LoadContent(Engine.Content, this);
            // Add it to the list
            Screens.Insert(Screens.Count - 1, screen);
        }

        /// <summary>
        /// Gets all instances of a specified type of GameScreen
        /// </summary>
        /// <typeparam name="T">The type of GameScreen</typeparam>
        /// <returns>A list of all matching GameScreens</returns>
        public T[] GetGameScreen<T>() where T : GameScreen
        {
            // Loop through all of the GameScreens and find the matching ones
            //List<T> match = new List<T>();
            T[] match = Screens.OfType<T>().ToArray<T>();
            // Return the results
            return match;
        }

        /// <summary>
        /// Gets the GameScreen at a specified index
        /// </summary>
        /// <param name="index">The index of the GameScreen in the stack</param>
        /// <returns>The GameScreen at the index</returns>
        public GameScreen GetGameScreen(int index)
        {
            // Make sure the index is within bounds
            if (index >= 0 && index < Screens.Count)
                // Return the GameScreen
                return Screens[index];
            else
                // Index was out of bounds - no GameScreen to return
                return null;
        }

        /// <summary>
        /// Exits all of the GameScreens and loads the specified screen
        /// </summary>
        /// <param name="nextScreen">The next GameScreen to load</param>
        public void ExitGameScreens(GameScreen nextScreen)
        {
            // Clear out all of the current screens
            Screens.Clear();
            // Add the next screen
            AddGameScreen(nextScreen);
        }

        /// <summary>
        /// Exits all GameScreens but the current one
        /// </summary>
        public void ExitAllButCurrent()
        {
            // Loop through all but the last GameScreen and remove them from the list
            for (int i = 0; i < Screens.Count - 1; i++)
            {
                Screens.RemoveAt(i);
                i--;
            }
        }

        /// <summary>
        /// Updates the GameScreens
        /// </summary>
        /// <param name="gameTime">The amount of time since the last update</param>
        internal void Update(GameTime gameTime)
        {
            // Make a temporary list of GameScreens, so removed GameScreens don't mess with the loop
            List<GameScreen> temp = new List<GameScreen>(Screens);

            // Make sure the are GameScreens
            if (Screens.Count > 0)
            {
                int counter = Screens.Count - 1;
                List<int> screenIndexes = new List<int>();

                for (int i = temp.Count - 1; i >= 0; i--)
                {
                    // Update the screens input
                    temp[i].UpdateInput(gameTime);
                    // If it is transitioning off and finished, remove the screen
                    if (temp[i].State == GameScreenState.TransitioningOff && temp[i].TransitionLocation >= 1.0f)
                    {
                        temp[i].Dispose();
                        Screens.Remove(temp[i]);       
                    }
                    // If the GameScreen doesn't allow input below it, exit the loop
                    if (!temp[i].AllowInputBelow)
                        break;
                }

                for (int i = temp.Count - 1; i >= 0; i--)
                {
                    // If the GameScreen has finished transitioning off, skip it. It was handled in the input loop
                    if (!(temp[i].State == GameScreenState.TransitioningOff && temp[i].TransitionLocation >= 1.0f))
                    {
                        // Update the GameScreen
                        temp[i].Update(gameTime);
                        // If it is transitioning off and finished, remove the screen
                        if (temp[i].State == GameScreenState.TransitioningOff && temp[i].TransitionLocation >= 1.0f)
                        {
                            temp[i].Dispose();
                            Screens.Remove(temp[i]);
                        }
                    }
                    // If the GameScreen doesn't allow update below it, exit the loop
                    if (!temp[i].AllowUpdateBelow)
                        break;
                }
            }
        }

        /// <summary>
        /// Draws the GameScreens
        /// </summary>
        internal void Draw()
        {
            // Make sure there are GameScreens
            if (Screens.Count > 0)
            {
                // Loop backward through the GameScreens to find all the ones that should be drawn
                int bottomScreen = Screens.FindLastIndex(screen => !screen.AllowDrawBelow && screen.State == GameScreenState.Active);
                // If -1 was returned, make sure the bottom screen index is 0
                bottomScreen = Math.Max(bottomScreen, 0);
                /*int counter = Screens.Count - 1;
                List<int> screenIndexes = new List<int>();
                while (counter > 0 && (Screens[counter].AllowDrawBelow || Screens[counter].State != GameScreenState.Active))
                {
                    screenIndexes.Add(counter);
                    counter--;
                }
                screenIndexes.Add(counter);*/
                // Loop through the GameScreens that should be drawn from back to front and draw them.
                for (int i = bottomScreen; i < Screens.Count; i++)
                    Screens[i].Draw();
                /*for (int i = screenIndexes.Count - 1; i >= 0; i--)
                    Screens[screenIndexes[i]].Draw();*/
            }
        }
    }
}
