﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Turbo2D.Helpers
{
    public class SpriteAnimation
    {
        protected List<Rectangle> Frames = new List<Rectangle>();
        protected int CurrentFrameNum = 0;
        public Rectangle CurrentFrame { get { return Frames[CurrentFrameNum]; } }
        public bool Playing { get; protected set; }
        public TimeSpan FrameTime { get; protected set; }
        protected TimeSpan currentTime;
        public bool Reverse { get; protected set; }
        public event EventHandler FrameChangedEvent;

        public SpriteAnimation(Rectangle sourceRectangle, int numFrames, TimeSpan frameTime)
        {
            for (int i = 0; i < numFrames; i++)
            {
                Frames.Add(new Rectangle(i * (sourceRectangle.Width / numFrames), sourceRectangle.Y, sourceRectangle.Width / numFrames, 
                    sourceRectangle.Height));
            }
            FrameTime = frameTime;
        }

        public SpriteAnimation(List<Rectangle> frames, TimeSpan frameTime)
        {
            Frames = frames;
            FrameTime = frameTime;
        }

        public void SetCurrentFrame(int frameNum)
        {
            if (frameNum < 0)
                CurrentFrameNum = 0;
            else if (frameNum >= Frames.Count)
                CurrentFrameNum = Frames.Count - 1;
            else
                CurrentFrameNum = frameNum;
            if (FrameChangedEvent != null)
                FrameChangedEvent.Invoke(this, new EventArgs());
        }

        public void Play(bool reverse)
        {
            CurrentFrameNum = 0;
            Playing = true;
            Reverse = reverse;
        }

        public void Play(bool reverse, int startingFrame)
        {
            CurrentFrameNum = startingFrame;
            Reverse = reverse;
            Playing = true;
        }

        public void Pause()
        {
            Playing = false;
        }

        public void Resume()
        {
            Playing = true;
        }

        public void Update(GameTime gameTime)
        {
            currentTime += gameTime.ElapsedGameTime;
            if (currentTime >= FrameTime)
            {
                currentTime = TimeSpan.Zero;
                if (!Reverse)
                    CurrentFrameNum++;
                else
                    CurrentFrameNum--;
                if (CurrentFrameNum >= Frames.Count)
                    CurrentFrameNum = 0;
                if (CurrentFrameNum < 0)
                    CurrentFrameNum = Frames.Count - 1;
                if (FrameChangedEvent != null)
                    FrameChangedEvent(this, new EventArgs());
            }
        }
    }
}
