﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;

namespace Turbo2D.Helpers
{
    public sealed class TurboSong
    {
        /// <summary>
        /// The Song that is being played
        /// </summary>
        public Song XnaSong { get; private set; }
        private float Volume;
        /// <summary>
        /// Gets or sets the volume of the song when it is played. Note: This cannot be modified while the song is fading
        /// </summary>
        public float SongVolume 
        {
            get { return Volume; } 
            set 
            {
                // Set the volume only if the song is not fading
                if (!Fading)
                {
                    Volume = value;
                    MediaPlayer.Volume = value;
                }
            } 
        }
        /// <summary>
        /// The volume that the song is being played at
        /// </summary>
        public float CurrentVolume { get { return MediaPlayer.Volume; } }
        private TimeSpan FadeLength;
        private TimeSpan FadeTime;
        private bool FadeIn;
        /// <summary>
        /// Whether the song is in a state of being faded
        /// </summary>
        public bool Fading { get { return FadeTime < FadeLength; } }
        /// <summary>
        /// The song's current state
        /// </summary>
        public MediaState State { get; private set; }

        internal TurboSong(Song xnaSong, float volume)
        {
            // Set the song and volume
            XnaSong = xnaSong;
            Volume = volume;
        }

        /// <summary>
        /// Plays the song
        /// </summary>
        public void Play()
        {
            Play(TimeSpan.Zero, false);
        }

        /// <summary>
        /// Plays the song
        /// </summary>
        /// <param name="looping">Whether the song should be looped</param>
        public void Play(bool looping)
        {
            Play(TimeSpan.Zero, looping);
        }

        /// <summary>
        /// Plays the song
        /// </summary>
        /// <param name="fadeInLength">The amount of time to fade in the song's volume</param>
        /// <param name="looping">Whether the song should be looped</param>
        public void Play(TimeSpan fadeInLength, bool looping)
        {
            // Set the fade variables
            FadeTime = TimeSpan.Zero;
            FadeLength = fadeInLength;
            FadeIn = true;

            if (FadeTime > TimeSpan.Zero)
                // Set the volume to zero
                MediaPlayer.Volume = 0f;
            else
                // Set the volume to the specified level
                MediaPlayer.Volume = SongVolume;

            // Set the repeating property and play the song
            MediaPlayer.IsRepeating = looping;
            MediaPlayer.Play(XnaSong);

            // Set the state to playing
            State = MediaState.Playing;
        }

        /// <summary>
        /// Pauses the song
        /// </summary>
        public void Pause()
        {
            // If the song is playing, pause it
            if (State == MediaState.Playing)
            {
                MediaPlayer.Pause();
                State = MediaState.Paused;
            }
        }

        /// <summary>
        /// Resumes the song after it has been paused
        /// </summary>
        public void Resume()
        {
            // If the song is paused, resume it
            if (State == MediaState.Paused)
            {
                MediaPlayer.Resume();
                State = MediaState.Playing;
            }
        }

        /// <summary>
        /// Stops the song
        /// </summary>
        public void Stop()
        {
            Stop(TimeSpan.Zero);
        }

        /// <summary>
        /// Stops the song
        /// </summary>
        /// <param name="fadeOutLength">The amount of time to fade out the song's volume</param>
        public void Stop(TimeSpan fadeOutLength)
        {
            // Set the fade variables
            FadeTime = TimeSpan.Zero;
            FadeLength = fadeOutLength;
            FadeIn = false;

            if (FadeLength == TimeSpan.Zero)
            {
                // If there is no fade, stop the song
                MediaPlayer.Stop();

                // Set the state to stopped
                State = MediaState.Stopped;
            }
        }

        internal void UpdateFade(GameTime gameTime)
        {
            // Update the fade time
            FadeTime += gameTime.ElapsedGameTime;

            if (FadeIn)
            {
                // If the song is fading in, set the volume so that it increases the longer the fade goes
                MediaPlayer.Volume = SongVolume * MathHelper.Clamp((float)(FadeTime.TotalSeconds / FadeLength.TotalSeconds), 0f, 1.0f);
            }
            else
            {
                // If the song is fading out, set the volume so that it decreases the longer the fade goes
                MediaPlayer.Volume = SongVolume * MathHelper.Clamp(1.0f - (float)(FadeTime.TotalSeconds / FadeLength.TotalSeconds), 0f, 
                    1.0f);

                // If the volume has reaches zero, stop the song
                if (MediaPlayer.Volume == 0f)
                {
                    MediaPlayer.Stop();
                    State = MediaState.Stopped;
                }
            }
        }

        internal void LostFocus(object sender, EventArgs e)
        {
            // If the song is playing when the window loses focus, pause it
            if (State == MediaState.Playing)
                MediaPlayer.Pause();
        }

        internal void GainedFocus(object sender, EventArgs e)
        {
            // If the song was playing when the window lost focus, resume it
            if (State == MediaState.Playing)
                MediaPlayer.Resume();
        }
    }
}
