﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Turbo2D.Helpers
{
    public sealed class CameraManager
    {
        private Dictionary<string, Camera2D> Cameras = new Dictionary<string, Camera2D>();
        private string ActiveCameraName;
        public Camera2D ActiveCamera { get { return (Cameras.Count > 0 && !String.IsNullOrEmpty(ActiveCameraName) ? 
            Cameras[ActiveCameraName] : null); } }

        internal CameraManager()
        {
        }

        public void AddCamera(Camera2D camera, string name)
        {
            Cameras.Add(name, camera);
        }

        /// <summary>
        /// Sets the active camera in the CameraManager
        /// </summary>
        /// <param name="name">The name of the camera. Set to "" if you want no camera to be active.</param>
        public void SetActiveCamera(string name)
        {
            ActiveCameraName = name;
        }

        /// <summary>
        /// Updates the CameraManager
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Update</param>
        internal void Update(GameTime gameTime)
        {
            // Update the active camera
            if (ActiveCamera != null)
                ActiveCamera.Update(gameTime);
        }
    }
}
