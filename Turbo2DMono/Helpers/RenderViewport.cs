﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.Helpers
{
    public sealed class RenderViewport
    {
        public Viewport Viewport { get; private set; }
        public List<string> RenderGroups { get; private set; }
        public CameraManager Cameras { get; private set; }
        public string Name { get; private set; }

        internal RenderViewport(Viewport viewport, string name)//, Renderer renderer)
            : this(viewport, name, new string[] { "Default" })//, renderer)
        {
        }

        /// <summary>
        /// Creates a new RenderViewport
        /// </summary>
        /// <param name="viewport">The Viewport to use for drawing</param>
        /// <param name="renderGroups">The RenderGroups to draw in the Viewport</param>
        /// <param name="renderer">The Renderer to user for drawing</param>
        internal RenderViewport(Viewport viewport, string name, string[] renderGroups)//, Renderer renderer)
        {
            Viewport = viewport;
            RenderGroups = new List<string>(renderGroups);
            Cameras = new CameraManager();
            Name = name;
        }

        /// <summary>
        /// Updates the RenderViewport's cameras
        /// </summary>
        /// <param name="gameTime">Time passed since the last call to Update</param>
        internal void UpdateCameras(GameTime gameTime)
        {
            // Update the CameraManager
            Cameras.Update(gameTime);
        }
    }
}
