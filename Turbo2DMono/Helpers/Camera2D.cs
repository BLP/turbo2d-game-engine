﻿using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;

namespace Turbo2D.Helpers
{
    public class Camera2D
    {
        public Vector2 Position { get; set; }
        public float Rotation { get; set; }
        public float Scale { get; set; }
        public Rectangle CameraBounds { get; set; }
        public Vector2 RotationBounds { get; set; }
        public bool WrapRotation { get; set; }
        public Vector2 ScaleBounds { get; set; }
        public Vector2 CameraOrigin { get; set; }
        /*public Rectangle CameraView
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, Engine.CurrentViewport.Width,
                    Engine.CurrentViewport.Height);
            }
        }*/
        protected GameScreen Parent { get; private set; }
        protected BaseEngine Engine { get; private set; }
        public Matrix Transformation { get { return CreateCameraMatrix(CameraOrigin, Rotation, Position, Scale); } }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin)
            : this(parent, cameraBounds, cameraOrigin, new Vector2(0f, MathHelper.TwoPi))
        {
        }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin, Vector2 rotationBounds)
            : this(parent, cameraBounds, cameraOrigin, rotationBounds, new Vector2(0.1f, 2f))
        {
        }

        public Camera2D(GameScreen parent, Rectangle cameraBounds, Vector2 cameraOrigin, Vector2 rotationBounds, Vector2 scaleBounds)
        {
            Parent = parent;
            Engine = parent.Engine;
            CameraBounds = cameraBounds;
            CameraOrigin = cameraOrigin;
            Rotation = 0f;
            RotationBounds = rotationBounds;
            WrapRotation = true;
            Scale = 1f;
            ScaleBounds = scaleBounds;
        }

        protected internal virtual void Update(GameTime gameTime)
        {
            Position = Vector2.Max(new Vector2(CameraBounds.Left, CameraBounds.Top), Position);
            Position = Vector2.Min(new Vector2(CameraBounds.Right, CameraBounds.Bottom), Position);
            if (WrapRotation)
            {
                if (Rotation > RotationBounds.Y)
                {
                    Rotation = RotationBounds.X + (Rotation - RotationBounds.Y);
                }
                else if (Rotation < RotationBounds.X)
                {
                    Rotation = RotationBounds.Y + (Rotation - RotationBounds.X);
                }
            }
            else
            {
                Rotation = MathHelper.Clamp(Rotation, RotationBounds.X, RotationBounds.Y);
            }
            Scale = MathHelper.Clamp(Scale, ScaleBounds.X, ScaleBounds.Y);
        }

        protected static Matrix CreateCameraMatrix(Vector2 origin, float rotation, Vector2 position, float scale)
        {
            return Matrix.CreateTranslation(new Vector3(-position, 0)) * 
                Matrix.CreateRotationZ(-rotation) *      
                Matrix.CreateScale(new Vector3(scale, scale, 0)) * 
                Matrix.CreateTranslation(new Vector3(origin, 0));
        }
    }
}
