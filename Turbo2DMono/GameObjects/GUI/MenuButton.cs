﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using System.Collections.Generic;

namespace Turbo2D.GameObjects.GUI
{
    /// <summary>
    /// A textured button
    /// </summary>
    public class MenuButton : Sprite, IButton
    {
        /// <summary>
        /// Occurs when the button is clicked
        /// </summary>
        public event EventHandler<ButtonClickedEventArgs> Clicked;
        /// <summary>
        /// Occurs when the mouse is hovering over the button
        /// </summary>
        public event EventHandler<ButtonClickedEventArgs> MouseOver;
        /// <summary>
        /// Occurs when the mouse first enters the area of the button
        /// </summary>
        public event EventHandler<ButtonClickedEventArgs> MouseEnter;
        /// <summary>
        /// Occurs when the mouse first leaves the area of the button
        /// </summary>
        public event EventHandler<ButtonClickedEventArgs> MouseLeave;

        private Rectangle ClickRect
        {
            get
            {
                return CollisionHelper.CalculateBoundingRectangle(new Rectangle(0, 0, SourceRectangle.Width,
                    SourceRectangle.Height), CollisionHelper.CreateMatrix(ActualOrigin, ActualRotation, ActualPosition, ActualScale));
            }
        }

        /// <summary>
        /// The names of the viewports that this button is contained in
        /// </summary>
        public string[] Viewports { get; set; }

        /// <summary>
        /// Whether the button should be updated
        /// </summary>
        public bool Enabled { get; set; }


        /// <summary>
        /// Creates a new instance of a MenuButton
        /// </summary>
        /// <param name="parent">The GameScreen that owns the button</param>
        /// <param name="name">The button's name</param>
        /// <param name="textureName">The name of the button's texture</param>
        /// <param name="position">The button's positon on the screen</param>
        /// <param name="parentObject">The parent object of the button</param>
        public MenuButton(GameScreen parent, string name, string textureName, Vector2 position, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, parentObject)
        {
            Enabled = true;
            //Viewports = new List<string>();
            //Viewports.Add("Screen");
        }

        protected bool IsMouseOver(MouseInputHelper input)
        {
            return ClickRect.Contains((int)input.NewMousePosition.X, (int)input.NewMousePosition.Y);
        }

        protected bool MouseClicked(MouseInputHelper input)
        {
            return (IsMouseOver(input) && input.MouseButtonReleased(MouseButtons.LeftButton));
        }

        protected bool MouseClicking(MouseInputHelper input)
        {
            return (IsMouseOver(input) && input.NewMouseState.LeftButton == ButtonState.Pressed);
        }

        protected bool MouseEntered(MouseInputHelper input)
        {
            return (IsMouseOver(input) && !ClickRect.Contains((int)input.OldMousePosition.X, (int)input.OldMousePosition.Y));
        }

        protected bool MouseLeft(MouseInputHelper input)
        {
            return (!IsMouseOver(input) && ClickRect.Contains((int)input.OldMousePosition.X, (int)input.OldMousePosition.Y));
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            Origin = Vector2.Zero;
        }

        /// <summary>
        /// Updates the button based on mouse input
        /// </summary>
        /// <param name="gameTime">The amoun of time passed since the last call to update</param>
        /// <param name="input">The helper to get mouse input from</param>
        public override void UpdateMouse(GameTime gameTime, MouseInputHelper input)
        {
            base.UpdateMouse(gameTime, input);

            if (MouseEntered(input))
            {
                if (MouseEnter != null)
                    MouseEnter(this, new ButtonClickedEventArgs(Name));
            }
            else if (MouseLeft(input))
            {
                if (MouseLeave != null)
                    MouseLeave(this, new ButtonClickedEventArgs(Name));
            }

            if (MouseClicked(input))
            {
                if (Clicked != null)
                    Clicked(this, new ButtonClickedEventArgs(Name));
            }
            else if (IsMouseOver(input))
            {
                if (MouseOver != null)
                    MouseOver(this, new ButtonClickedEventArgs(Name));
            }
        }
    }
}
