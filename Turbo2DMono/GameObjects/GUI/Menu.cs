﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Microsoft.Xna.Framework.Content;

namespace Turbo2D.GameObjects.GUI
{
    public abstract class Menu : DrawableGameObject
    {
        public Rectangle MenuArea { get { return new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y); } }
        protected List<List<MenuItem>> ArrItems = new List<List<MenuItem>>();
        public bool Enabled { get; set; }
        public List<MenuItem> Items { get; protected set; }
        public MenuAlignments Alignment { get; protected set; }
        public float VerticalSpacing { get; protected set; }
        public float HorizontalSpacing { get; protected set; }
        protected string FontName;
        protected Vector2 Size;
        public int CurrentPage { get { return pCurrentPage; } set { ChangePage(pCurrentPage, value); pCurrentPage = value; } }
        protected int pCurrentPage;

        public Menu(GameScreen parent, string name, string fontName, Vector2 menuSize, DrawableGameObject parentObject = null)
            :this(parent, name, fontName, menuSize, MenuAlignments.Center, parentObject)
        {
        }

        public Menu(GameScreen parent, string name, string fontName, Vector2 menuSize, MenuAlignments alignment, 
            DrawableGameObject parentObject = null)
            : this(parent, name, fontName, menuSize, alignment, 1f, 5f, parentObject)
        {
        }

        public Menu(GameScreen parent, string name, string fontName, Vector2 menuSize, MenuAlignments alignment, 
            float verticalSpacing, float horizontalSpacing, DrawableGameObject parentObject = null)
            :base(parent, name, parentObject)
        {
            Size = menuSize;
            FontName = fontName;
            Items = new List<MenuItem>();
            VerticalSpacing = verticalSpacing;
            HorizontalSpacing = horizontalSpacing;
            Alignment = alignment;
            Enabled = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            UpdateItemPositions();
        }

        /*public void AddMenuItem(string text, Color color, Color selectedColor)
        {
            MenuItem item = new MenuItem(this.Parent, "", text, FontName, color, this);
            AddMenuItem(item);
        }*/

        public void AddMenuItem(MenuItem item)
        {
            Items.Add(item);
            Components.Add(item);
        }

        protected class ColumnInfo
        {
            public List<MenuItem> Items = new List<MenuItem>();
            public float Width = 0;
            public float WidestIndex = 0;
            public int Page;

            public ColumnInfo()
            {
            }

            public void AddItem(MenuItem item)
            {
                Items.Add(item);
                if (item.TextSize.X > Width)
                    Width = item.TextSize.X;
                WidestIndex = Items.Count - 1;
            }
        }

        protected class ColumnPair
        {
            public ColumnInfo Column1;
            public ColumnInfo Column2;
            public float HighestAvWidth;
            public float HighestAvWidthIndex;

            public ColumnPair(ColumnInfo column1, ColumnInfo column2)
            {
                Column1 = column1;
                Column2 = column2;
                for (int i = 0; i < Column1.Items.Count; i++)
                {
                    if (i < Column2.Items.Count)
                    {
                        float av = (Column1.Items[i].TextSize.X + Column2.Items[i].TextSize.X) / 2;
                        if (av > HighestAvWidth)
                        {
                            av = HighestAvWidth;
                            HighestAvWidthIndex = i;
                        }
                    }
                }
            }
        }

        protected void UpdateItemPositions()
        {
            ArrItems.Clear();
            List<ColumnInfo> columns = new List<ColumnInfo>();
            columns.Add(new ColumnInfo());
            Vector2 nextPos = Vector2.Zero;
            for (int i = 0; i < Items.Count; i++)
            {
                if (nextPos.Y + Items[i].TextSize.Y > Size.Y)
                {
                    columns.Add(new ColumnInfo());
                    nextPos.Y = 0;
                }
                Items[i].Position = nextPos;
                columns[columns.Count - 1].AddItem(Items[i]);
                nextPos.Y += Items[i].TextSize.Y + VerticalSpacing;
            }

            ArrItems.Add(new List<MenuItem>());
            switch (Alignment)
            {
                case MenuAlignments.Left:
                    float prevX = 0;
                    /*foreach (MenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X, item.Position.Y);*/
                    columns[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(columns[0].Items);
                    for (int i = 1; i < columns.Count; i++)
                    {
                        prevX += columns[i - 1].Width + HorizontalSpacing;
                        if (prevX + columns[i].Width  > MenuArea.Right)
                        {
                            ArrItems.Add(new List<MenuItem>());
                            prevX = 0;
                        }
                        columns[i].Page = ArrItems.Count;
                        foreach (MenuItem item in columns[i].Items)
                            item.Position = new Vector2(prevX, item.Position.Y);
                        ArrItems[ArrItems.Count - 1].AddRange(columns[i].Items);
                    }
                    break;
                case MenuAlignments.Center:
                    foreach (MenuItem item in columns[0].Items)
                        item.Position = new Vector2(columns[0].Width / 2, item.Position.Y);
                    prevX = columns[0].Width / 2;
                    columns[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(columns[0].Items);
                    for (int i = 1; i < columns.Count; i++)
                    {
                        prevX += HorizontalSpacing + columns[i - 1].Width / 2 + columns[i].Width / 2;
                        if (prevX + columns[i].Width / 2 > MenuArea.Width)
                        {
                            ArrItems.Add(new List<MenuItem>());
                            prevX = columns[i].Width / 2;
                        }
                        foreach (MenuItem item in columns[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(columns[i].Items);
                    }
                    int minColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (columns.Count == 1)
                        {
                            xShift = (Size.X - (columns[0].Items[0].Position.X + columns[0].Width / 2)) / 2;
                        }
                        else
                        {
                            for (int column = minColumn; column < columns.Count; column++)
                            {
                                if (column == columns.Count - 1)
                                {
                                    xShift = (Size.X - (columns[column].Items[0].Position.X + columns[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }

                                if (columns[column].Page != columns[column - 1].Page)
                                {
                                    xShift = (Size.X - (columns[column - 1].Items[0].Position.X + columns[column].Width / 2)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (MenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }
                    break;
                case MenuAlignments.Right:
                    foreach (MenuItem item in columns[0].Items)
                        item.Position = new Vector2(columns[0].Width, item.Position.Y);
                    prevX = columns[0].Width;
                    columns[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(columns[0].Items);
                    for (int i = 1; i < columns.Count; i++)
                    {
                        prevX += HorizontalSpacing + columns[i].Width;
                        if (prevX > MenuArea.Width)
                        {
                            ArrItems.Add(new List<MenuItem>());
                            prevX = columns[i].Width;
                        }
                        foreach (MenuItem item in columns[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(columns[i].Items);
                    }
                    int prevColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (columns.Count == 1)
                        {
                            xShift = Size.X - (columns[0].Items[0].Position.X);
                        }
                        else
                        {
                            for (int column = prevColumn; column < columns.Count; column++)
                            {
                                if (column == columns.Count - 1)
                                {
                                    xShift = Size.X - (columns[column].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                                if (columns[column].Page != columns[column - 1].Page)
                                {
                                    xShift = Size.X - (columns[column - 1].Items[0].Position.X);
                                    prevColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (MenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }

                    break;
            }

            // Hide all but the first page
            for (int i = 1; i < ArrItems.Count; i++)
                ArrItems[i].ForEach(item => item.Visible = false);
            CurrentPage = 0;
        }

        protected virtual void ChangePage(int oldPage, int newPage)
        {
            // Hide the old page
            ArrItems[oldPage].ForEach(item => item.Visible = false);
            // Show the new page
            ArrItems[newPage].ForEach(item => item.Visible = true);
        }
    }
}
