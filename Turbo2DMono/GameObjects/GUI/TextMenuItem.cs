﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Turbo2D.GameObjects.GUI
{
    public class TextMenuItem : MenuItem
    {
        public Color SelectedColor { get; protected set; }
        public Color StandardColor { get; protected set; }
        public new TextMenu Menu { get { return base.Menu as TextMenu; } }
        protected bool Selected { get { return Menu.SelectedItem == this; } }

        public TextMenuItem(string name, string text, string fontName, Color standardColor, Color selectedColor, TextMenu menu)
            : base(name, text, fontName, standardColor, menu)
        {
            StandardColor = standardColor;
            SelectedColor = selectedColor;
        }

        public override void Draw()
        {
            if (Selected)
                Color = SelectedColor;
            else
                Color = StandardColor;
            base.Draw();
        }
    }
}
