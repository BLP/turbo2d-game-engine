﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using System.Collections.Generic;

namespace Turbo2D.GameObjects.GUI
{
    public class TextButton : TextItem, IButton
    {
        public event EventHandler<ButtonClickedEventArgs> Clicked;
        public event EventHandler<ButtonClickedEventArgs> MouseOver;
        public event EventHandler<ButtonClickedEventArgs> MouseEnter;
        public event EventHandler<ButtonClickedEventArgs> MouseLeave;

        private Rectangle TextRect { get { return CalcRect(); } }

        public bool Enabled { get; set; }
        public string[] Viewports { get; set;  }

        public TextButton(GameScreen parent, string name, string text, string fontName, Color color, DrawableGameObject parentObject = null)
            : this(parent, name, text, fontName, color, Vector2.Zero, parentObject)
        {
        }

        public TextButton(GameScreen parent, string name, string text, string fontName, Color color, Vector2 position, 
            DrawableGameObject parentObject = null)
            : base(parent, name, fontName, text, color, position, parentObject)
        {
            Enabled = true;
            //Viewports = new List<string>();
            //Viewports.Add("Screen");
        }

        private Rectangle CalcRect()
        {
            // Check to make sure the font isn't null
            if (Font != null)
            {
                return CollisionHelper.CalculateBoundingRectangle(
                    new Rectangle(0, 0, (int)TextSize.X, (int)TextSize.Y),
                    CollisionHelper.CreateMatrix(ActualOrigin, ActualRotation, ActualPosition, ActualScale));
            }
            else
                return Rectangle.Empty;
        }

        protected bool IsMouseOver(MouseInputHelper input)
        {
            if (TextRect.Contains((int)input.NewMousePosition.X, (int)input.NewMousePosition.Y))
                return true;
            return false;
        }

        protected bool MouseClicked(MouseInputHelper input)
        {
            if (IsMouseOver(input) && input.MouseButtonReleased(MouseButtons.LeftButton))
                return true;
            return false;
        }

        protected bool MouseClicking(MouseInputHelper input)
        {
            return (IsMouseOver(input) && input.NewMouseState.LeftButton == ButtonState.Pressed);
        }

        protected bool MouseEntered(MouseInputHelper input)
        {
            return (IsMouseOver(input) && !TextRect.Contains((int)input.OldMousePosition.X, (int)input.OldMousePosition.Y));
        }

        protected bool MouseLeft(MouseInputHelper input)
        {
            return (!IsMouseOver(input) && TextRect.Contains((int)input.OldMousePosition.X, (int)input.OldMousePosition.Y));
        }

        public override void UpdateMouse(GameTime gameTime, MouseInputHelper input)
        {
            base.UpdateMouse(gameTime, input);

            if (MouseEntered(input))
            {
                if (MouseEnter != null)
                    MouseEnter(this, new ButtonClickedEventArgs(Name));
            }
            else if (MouseLeft(input))
            {
                if (MouseLeave != null)
                    MouseLeave(this, new ButtonClickedEventArgs(Name));
            }

            if (MouseClicked(input))
            {
                if (Clicked != null)
                    Clicked(this, new ButtonClickedEventArgs(Name));
            }
            else if (IsMouseOver(input))
            {
                if (MouseOver != null)
                    MouseOver(this, new ButtonClickedEventArgs(Name));
            }
        }
    }
}
