﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Turbo2D.GameObjects;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Helpers;

namespace Turbo2D.GameObjects.GUI
{
    public enum MenuAlignments
    {
        Left,
        Right,
        Center
    }

    public class TextPointerMenu : Menu, IAcceptsKeyboardInput
    {
        public List<MenuPointer> Pointers { get; protected set; }

        public TextPointerMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, DrawableGameObject parentObject = null)
            :this(parent, name, fontName, menuSize, MenuAlignments.Center, parentObject)
        {
        }

        public TextPointerMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, MenuAlignments alignment, 
            DrawableGameObject parentObject = null)
            :this(parent, name, fontName, menuSize, alignment, 1f, 5f, parentObject)
        {
        }

        public TextPointerMenu(GameScreen parent, string name, string fontName, Vector2 menuSize, MenuAlignments alignment, 
            float verticalSpacing, float horizontalSpacing, DrawableGameObject parentObject = null)
            :base(parent, name, fontName, menuSize, alignment, verticalSpacing, horizontalSpacing, parentObject)
        {
            Pointers = new List<MenuPointer>();
        }

        public void AddMenuItem(string text, Color color)
        {
            MenuItem item = new MenuItem("", text, FontName, color, this);
            AddMenuItem(item);
        }

        public void AddPointer(string textureName, PointerAlignment alignment)
        {
            MenuPointer pointer = new MenuPointer("", textureName, alignment, this);
            AddPointer(pointer);
        }

        public void AddPointer(string textureName, PointerAlignment alignment, EventHandler<MenuScrollEventArgs> scroll, 
            EventHandler<MenuSelectEventArgs> selectEvent)
        {
            MenuPointer pointer = new MenuPointer("", textureName, alignment, this);
            if (scroll != null)
                pointer.Scroll += scroll;
            if (selectEvent != null)
                pointer.SelectEvent += selectEvent;
            AddPointer(pointer);
        }

        public void AddPointer(string textureName, PointerAlignment alignment, Keys scrollDown, Keys scrollUp, Keys select)
        {
            MenuPointer pointer = new MenuPointer("", textureName, alignment, scrollDown, scrollUp, select, this);
            AddPointer(pointer);
        }

        public void AddPointer(string textureName, PointerAlignment alignment, Keys scrollDown, Keys scrollUp, Keys select,
            EventHandler<MenuScrollEventArgs> scroll, EventHandler<MenuSelectEventArgs> selectEvent)
        {
            MenuPointer pointer = new MenuPointer("", textureName, alignment, scrollDown, scrollUp, select, this);
            if (scroll != null)
                pointer.Scroll += scroll;
            if (selectEvent != null)
                pointer.SelectEvent += selectEvent;
            AddPointer(pointer);
        }

        public void AddPointer(MenuPointer pointer)
        {
            Pointers.Add(pointer);
            Components.Add(pointer);
            pointer.Scroll += new EventHandler<MenuScrollEventArgs>(pointer_Scroll);
            HorizontalSpacing += pointer.SourceRectangle.Width;
        }

        void pointer_Scroll(object sender, MenuScrollEventArgs e)
        {
            if (!ArrItems[CurrentPage].Contains(Items[e.CurrentIndex]))
            {
                int newPage = CurrentPage;
                if (e.CurrentIndex > e.PreviousIndex)
                {
                    newPage++;
                    if (newPage >= ArrItems.Count)
                        newPage = 0;
                }
                else if (e.CurrentIndex < e.PreviousIndex)
                {
                    newPage--;
                    if (newPage < 0)
                        newPage = ArrItems.Count - 1;
                }
                CurrentPage = newPage;
            }
        }

        /*protected float ReturnPointerWidth(PointerAlignment alignment)
        {
            float width = 0f;
            for (int i = 0; i < Pointers.Count; i++)
            {
                if (Pointers[i].Alignment == alignment && Pointers[i].SourceRectangle.Width > width)
                    width = Pointers[i].SourceRectangle.Width;
            }
            if (width > 0)
            {
                return width + MenuPointer.BufferRoom;
            }
            else
                return width;
        }

        protected class ColumnInfo
        {
            public List<TextPointerMenuItem> Items = new List<TextPointerMenuItem>();
            public float Width = 0;
            public float WidestIndex = 0;
            public int Page;

            public ColumnInfo()
            {
            }

            public void AddItem(TextPointerMenuItem item)
            {
                Items.Add(item);
                if (item.TextSize.X > Width)
                    Width = item.TextSize.X;
                WidestIndex = Items.Count - 1;
            }
        }

        protected class ColumnPair
        {
            public ColumnInfo Column1;
            public ColumnInfo Column2;
            public float HighestAvWidth;
            public float HighestAvWidthIndex;

            public ColumnPair(ColumnInfo column1, ColumnInfo column2)
            {
                Column1 = column1;
                Column2 = column2;
                for (int i = 0; i < Column1.Items.Count; i++)
                {
                    if (i < Column2.Items.Count)
                    {
                        float av = (Column1.Items[i].TextSize.X + Column2.Items[i].TextSize.X) / 2;
                        if (av > HighestAvWidth)
                        {
                            av = HighestAvWidth;
                            HighestAvWidthIndex = i;
                        }
                    }
                }
            }
        }

        protected void UpdateItemPositions()
        {
            ArrItems.Clear();
            List<ColumnInfo> temp = new List<ColumnInfo>();
            temp.Add(new ColumnInfo());
            float lpointerWidth = ReturnPointerWidth(PointerAlignment.Left);
            float rpointerWidth = ReturnPointerWidth(PointerAlignment.Right);
            float bpointerWidth = lpointerWidth + rpointerWidth + HorizontalSpacing;
            Vector2 nextPos = new Vector2(MenuArea.X + lpointerWidth, MenuArea.Y);
            for (int i = 0; i < Items.Count; i++)
            {
                if (nextPos.Y + Items[i].TextSize.Y > MenuArea.Bottom)
                {
                    temp.Add(new ColumnInfo());
                    nextPos.Y = MenuArea.Y;
                }
                Items[i].Position = nextPos;
                temp[temp.Count - 1].AddItem(Items[i]);
                nextPos.Y += Items[i].TextSize.Y + VerticalSpacing;
            }

            ArrItems.Add(new List<TextPointerMenuItem>());
            switch (Alignment)
            {
                case MenuAlignments.Left:
                    float prevX;
                    foreach (TextPointerMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X + lpointerWidth, item.Position.Y);
                    temp[0].Page = 1;
                    prevX = MenuArea.X + lpointerWidth;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += temp[i - 1].Width + bpointerWidth;
                        if (prevX + temp[i].Width + rpointerWidth > MenuArea.Right)
                        {
                            ArrItems.Add(new List<TextPointerMenuItem>());
                            prevX = MenuArea.X + lpointerWidth;
                        }
                        temp[i].Page = ArrItems.Count;
                        foreach (TextPointerMenuItem item in temp[i].Items)
                            item.Position = new Vector2(prevX, item.Position.Y);
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    break;
                case MenuAlignments.Center:
                    foreach (TextPointerMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X + lpointerWidth + temp[0].Width / 2, item.Position.Y);
                    prevX = MenuArea.X + lpointerWidth + temp[0].Width / 2;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += bpointerWidth + temp[i - 1].Width / 2 + temp[i].Width / 2;
                        if (prevX + temp[i].Width / 2 + rpointerWidth > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextPointerMenuItem>());
                            prevX = MenuArea.X + lpointerWidth + temp[i].Width / 2;
                        }
                        foreach (TextPointerMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int minColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = (MenuArea.Right - (temp[0].Items[0].Position.X + temp[0].Width / 2 + rpointerWidth)) / 2;
                        }
                        else
                        {
                            for (int column = minColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = (MenuArea.Right - (temp[column].Items[0].Position.X + temp[column].Width / 2 + rpointerWidth)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }

                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = (MenuArea.Right - (temp[column - 1].Items[0].Position.X + temp[column].Width / 2 + rpointerWidth)) / 2;
                                    minColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextPointerMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }
                    break;
                case MenuAlignments.Right:
                    foreach (TextPointerMenuItem item in temp[0].Items)
                        item.Position = new Vector2(MenuArea.X + lpointerWidth + temp[0].Width, item.Position.Y);
                    prevX = MenuArea.X + lpointerWidth + temp[0].Width;
                    temp[0].Page = 1;
                    ArrItems[ArrItems.Count - 1].AddRange(temp[0].Items);
                    for (int i = 1; i < temp.Count; i++)
                    {
                        prevX += bpointerWidth + temp[i].Width;
                        if (prevX + rpointerWidth > MenuArea.Width)
                        {
                            ArrItems.Add(new List<TextPointerMenuItem>());
                            prevX = MenuArea.X + lpointerWidth + temp[i].Width;
                        }
                        foreach (TextPointerMenuItem item in temp[i].Items)
                        {
                            item.Position = new Vector2(prevX, item.Position.Y);
                        }
                        ArrItems[ArrItems.Count - 1].AddRange(temp[i].Items);
                    }
                    int prevColumn = 1;
                    for (int i = 0; i < ArrItems.Count; i++)
                    {
                        float xShift = 0;
                        if (temp.Count == 1)
                        {
                            xShift = MenuArea.Right - (temp[0].Items[0].Position.X + rpointerWidth);
                        }
                        else
                        {
                            for (int column = prevColumn; column < temp.Count; column++)
                            {
                                if (column == temp.Count - 1)
                                {
                                    xShift = MenuArea.Right - (temp[column].Items[0].Position.X + rpointerWidth);
                                    prevColumn = column + 1;
                                    break;
                                }
                                if (temp[column].Page != temp[column - 1].Page)
                                {
                                    xShift = MenuArea.Right - (temp[column - 1].Items[0].Position.X + rpointerWidth);
                                    prevColumn = column + 1;
                                    break;
                                }
                            }
                        }
                        foreach (TextPointerMenuItem item in ArrItems[i])
                            item.Position += new Vector2(xShift, 0);
                    }

                    break;
            }

            for (int i = 0; i < Pointers.Count; i++)
                Pointers[i].UpdatePosition();
            CurrentPage = 0;
        }*/

        protected override void ChangePage(int oldPage, int newPage)
        {
            base.ChangePage(oldPage, newPage);
            for (int i = 0; i < Pointers.Count; i++)
                Pointers[i].SelectedIndex = Items.IndexOf(ArrItems[CurrentPage][0]);
        }
    }
}
