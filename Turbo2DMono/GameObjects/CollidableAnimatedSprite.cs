﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;

namespace Turbo2D.GameObjects
{
    public class CollidableAnimatedSprite : AnimatedSprite, ICollidable
    {
        public Rectangle CollisionRectangle
        {
            get
            {
                return CollisionHelper.CalculateBoundingRectangle(new Rectangle(0, 0, SourceRectangle.Width, SourceRectangle.Height),
                    Transformation);
            }
        }
        //protected List<Rectangle> PartRectangles = new List<Rectangle>();
        //public List<Rectangle> TransformedPartRectangles { get; protected set; }
        public Matrix Transformation { get { return CollisionHelper.CreateMatrix(ActualOrigin, ActualRotation, ActualPosition); } }
        public Color[] CollisionColors { get; protected set; }
        public bool Collidable { get; protected set; }
        public int TextureWidth { get { return CurrentAnimation.CurrentFrame.Width; } }
        public int TextureHeight { get { return CurrentAnimation.CurrentFrame.Height; } }
        //public Type ObjectType { get { return this.GetType(); } }

        public CollidableAnimatedSprite(GameScreen parent, string name, string textureName, Dictionary<string, List<Rectangle>> animationFrames, 
            TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, animationFrames, frameTime, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public CollidableAnimatedSprite(GameScreen parent, string name, string textureName, Vector2 position, 
            Dictionary<string, List<Rectangle>> animationFrames, TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, animationFrames, frameTime, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public CollidableAnimatedSprite(GameScreen parent, string name, string textureName, Vector2 position, Rectangle sourceRectangle, 
            Vector2 scale, Dictionary<string, List<Rectangle>> animationFrames, TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, position, sourceRectangle, scale, animationFrames, frameTime, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public CollidableAnimatedSprite(GameScreen parent, string name, string textureName, string[] animationNames, int numFramesPerAnimation, 
            TimeSpan frameTime, DrawableGameObject parentObject = null)
            : base(parent, name, textureName, animationNames, numFramesPerAnimation, frameTime, parentObject)
        {
            //PartRectangles = new List<Rectangle>();
            //TransformedPartRectangles = new List<Rectangle>();
            Collidable = true;
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);

            base.FrameChanged += new EventHandler(UpdateColors);
            base.AnimationChanged += new EventHandler(UpdateColors);
            CollisionColors = new Color[TextureWidth * TextureHeight];
            Texture.GetData<Color>(0, CurrentAnimation.CurrentFrame, CollisionColors, 0, CollisionColors.Length);
        }

        void UpdateColors(object sender, EventArgs e)
        {
            CollisionColors = new Color[TextureWidth * TextureHeight];
            Texture.GetData<Color>(0, CurrentAnimation.CurrentFrame, CollisionColors, 0, CollisionColors.Length);
        }

        public virtual void CollisionOccurred(GameObject collidedWith)
        {
        }
    }
}
