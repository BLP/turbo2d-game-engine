﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;
using Turbo2D.Helpers;

namespace Turbo2D.GameObjects
{
    public abstract class DrawableGameObject : GameObject, IDrawableObject
    {
        public bool Visible { get; set; } // Whether this GameObject should be drawn
        public Vector2 Position { get; set; } // The position of the GameObject
        public Vector2 ActualPosition { get { return ParentObject == null ? Position : ParentObject.ActualPosition; } }
        public Vector2 Origin { get; set; } // The "anchor point" of the GameObject
        public Vector2 ActualOrigin { get { return ParentObject == null ? Origin : (ParentObject.ActualOrigin - (Position - Origin)); } }  
        public float Rotation { get; set; } // The rotation of the GameObject
        public float ActualRotation { get { return ParentObject == null ? Rotation : Rotation + ParentObject.ActualRotation; } }
        public Vector2 Scale { get; set; } // The scale of the GameObject
        public Vector2 ActualScale { get { return ParentObject == null ? Scale : Scale * ParentObject.ActualScale; } }
        public float DrawLayer { get; set; } // The GameObject's depth on the screen
        public float ActualDrawLayer { get { return ParentObject == null ? DrawLayer : ParentObject.ActualDrawLayer - (DrawLayer / (float)Math.Pow(10, ComponentLayer)); } }
        private string renderGroup; // The GameObject's RenderGroup name
        public string RenderGroup
        {
            get { return ParentObject == null ? renderGroup : ParentObject.RenderGroup; }
            set
            {
                if (RenderGroupChanged != null)
                    RenderGroupChanged(this, new RenderGroupChangedEventArgs(renderGroup, value));
                renderGroup = value;

                // If this accepts mouse input, update the viewports
                if (this is IAcceptsMouseInput)
                    UpdateViewports();
            }
        }
        public event EventHandler<RenderGroupChangedEventArgs> RenderGroupChanged; // An event when the RenderGoup name is changed

        public new DrawableGameObject ParentObject { get { return base.ParentObject as DrawableGameObject; } } // The GameObject that owns this one

        public DrawableGameObject(GameScreen parent, string name, DrawableGameObject parentObject = null)
            : base(parent, name, parentObject)
        {
            Scale = Vector2.One;
            Visible = true;
            RenderGroup = "Default";
        }

        private void UpdateViewports()
        {
            IAcceptsMouseInput asMouse = this as IAcceptsMouseInput;

            // Clear out the viewports
            asMouse.Viewports = Parent.Viewports.GetViewportNames(RenderGroup);

            /*foreach (RenderViewport viewport in Parent.Viewports)
            {
                if (viewport.RenderGroups.Contains(this.RenderGroup))
                    asMouse.Viewports.Add(viewport.Name);
            }*/
        }
    }
}
