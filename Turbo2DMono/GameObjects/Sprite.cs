﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Turbo2D.GameScreens;
using Turbo2D.Interfaces;

namespace Turbo2D.GameObjects
{
    public class Sprite : DrawableGameObject
    {
        public Texture2D Texture { get; protected set; }
        public Rectangle SourceRectangle { get; set; }
        public Color TintColor { get; set; }
        public SpriteEffects Effects { get; set; }
        private string TextureName;

        public Sprite(GameScreen parent, string name, string textureName, DrawableGameObject parentObject = null)
            : this(parent, name, textureName, Vector2.Zero, parentObject)
        {
        }

        public Sprite(GameScreen parent, string name, string textureName, Vector2 position, DrawableGameObject parentObject = null)
            : this(parent, name, textureName, position, new Rectangle(-1, -1, -1, -1), Vector2.One, parentObject)
        {
        }

        public Sprite(GameScreen parent, string name, string textureName, Vector2 position, Rectangle sourceRectangle, Vector2 scale,
            DrawableGameObject parentObject = null)
            : base(parent, name, parentObject)
        {
            TextureName = textureName;
            Rotation = 0f;
            Position = position;
            SourceRectangle = sourceRectangle;
            TintColor = Color.White;
            Scale = scale;
            Effects = SpriteEffects.None;
            DrawLayer = 0.0f;
            Origin = new Vector2(-1, -1);
        }


        public override void LoadContent(ContentManager Content)
        {
            if (TextureName != "")
                Texture = Content.Load<Texture2D>(TextureName);
            if (SourceRectangle == new Rectangle(-1, -1, -1, -1))
                SourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            if (Origin == new Vector2(-1, -1))
                Origin = new Vector2(SourceRectangle.Width / 2, SourceRectangle.Height / 2);
        }

        public override void Draw()
        {
            base.Draw();

            if (Visible)
                RenderDevice.DrawTexture(Texture, ActualPosition, SourceRectangle, TintColor, ActualRotation, ActualOrigin, ActualScale, 
                    Effects, ActualDrawLayer);
        }
    }
}
