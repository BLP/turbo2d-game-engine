﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Turbo2D.GameScreens;
using Turbo2D.Helpers;
using Turbo2D.Interfaces;
using Turbo2D.Collections;

namespace Turbo2D.GameObjects
{
    public abstract class GameObject
    {
        protected static Dictionary<Type, int> NumInstances = new Dictionary<Type, int>();

        protected internal GameObject ParentObject { get; private set; }
        protected internal GameScreen Parent { get; private set; }
        protected internal Renderer RenderDevice { get; private set; }
        protected internal BaseEngine Engine { get; private set; }
        public string Name { get; private set; }
        public bool Alive { get; private set; }

        //protected Dictionary<string, GameObject> Components = new Dictionary<string, GameObject>();
        protected GameObjectCollection Components;
        internal int ComponentLayer = 0;
        //private List<IUpdateableObject> UpdateableObjects = new List<IUpdateableObject>();
        //private List<IDrawableObject> DrawableObjects = new List<IDrawableObject>();
        //private List<IAcceptsInput> InputObjects = new List<IAcceptsInput>();

        public GameObject(GameScreen parent, string name, GameObject parentObject = null)
        {
            ParentObject = parentObject;
            ComponentLayer = (ParentObject == null ? 0 : ParentObject.ComponentLayer + 1);
            Parent = parent;
            Engine = Parent.Engine;
            Components = new GameObjectCollection(Engine.Content);
            if (string.IsNullOrEmpty(name))
                Name = GetDefaultName();
            else
                Name = name;
            RenderDevice = Parent.RenderDevice;
            Alive = true;

        }

        private string GetDefaultName()
        {
            string name;
            if (!NumInstances.ContainsKey(this.GetType()))
                NumInstances.Add(this.GetType(), 0);
            name = this.GetType().Name + NumInstances[this.GetType()].ToString();
            NumInstances[this.GetType()]++;
            return name;
        }

        protected void Kill()
        {
            Alive = false;
            if (this is IUpdateable)
                (this as IUpdateableObject).Enabled = false;
            if (this is IAcceptsKeyboardInput)
                (this as IAcceptsKeyboardInput).Enabled = false;
            if (this is IAcceptsMouseInput)
                (this as IAcceptsMouseInput).Enabled = false;
            if (this is IDrawableObject)
                (this as IDrawableObject).Visible = false;
        }


        public abstract void LoadContent(ContentManager Content);

        public virtual void Update(GameTime gameTime)
        {
            IUpdateableObject[] updateable = Components.OfType<IUpdateableObject>().ToArray<IUpdateableObject>();
            foreach (IUpdateableObject gameObject in updateable)
            {
                if (gameObject.Enabled)
                    gameObject.Update(gameTime);
                if (!gameObject.Alive)
                    Components.Remove(gameObject.Name);
            }
        }

        public virtual void UpdateKeyboard(GameTime gameTime, KeyboardInputHelper input)
        {
            IAcceptsKeyboardInput[] updateable = Components.OfType<IAcceptsKeyboardInput>().ToArray<IAcceptsKeyboardInput>();
            foreach (IAcceptsKeyboardInput gameObject in updateable)
            {
                if (gameObject.Enabled)
                    gameObject.UpdateKeyboard(gameTime, input);
                if (!gameObject.Alive)
                    Components.Remove(gameObject.Name);
            }
        }

        public virtual void UpdateMouse(GameTime gameTime, MouseInputHelper input)
        {
            IAcceptsMouseInput[] updateable = Components.OfType<IAcceptsMouseInput>().ToArray<IAcceptsMouseInput>();
            foreach (IAcceptsMouseInput gameObject in updateable)
            {
                if (gameObject.Enabled)
                    gameObject.UpdateMouse(gameTime, input);
                if (!gameObject.Alive)
                    Components.Remove(gameObject.Name);
            }
        }

        public virtual void Draw()
        {
            IDrawableObject[] drawable = Components.OfType<IDrawableObject>().ToArray<IDrawableObject>();
            foreach (IDrawableObject gameObject in drawable)
            {
                if (gameObject.Visible)
                    gameObject.Draw();
                if (!gameObject.Alive)
                    Components.Remove(gameObject.Name);
            }
        }
    }
}
