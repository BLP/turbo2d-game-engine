﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Turbo2D.GameScreens;
using Microsoft.Xna.Framework.Content;

namespace Turbo2D.GameObjects
{
    public class DrawableRectangle : DrawableGameObject
    {
        public Color Color { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        public DrawableRectangle(GameScreen parent, string name, Color color, float width, float height,
            DrawableGameObject parentObject = null)
            : base(parent, name, parentObject)
        {
            Color = color;
            Width = width;
            Height = height;
        }

        public override void LoadContent(ContentManager Content)
        {
            // No content to load
        }

        public override void Draw()
        {
            base.Draw();

            // Draw the rectangle
            RenderDevice.DrawRectangle(ActualPosition, Width, Height, Color, ActualOrigin, ActualRotation, ActualScale, ActualDrawLayer);
        }
    }
}
