﻿using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Turbo2D.Helpers;

namespace Turbo2D.GameScreens
{
    public abstract class LoadScreen<T> : GameScreen where T : GameScreen
    {
        private T ScreenToLoad;
        private Thread LoadThread;
        public bool LoadFinished { get; protected set; }
        private bool LoadStarted;

        public LoadScreen(T screenToLoad)
        {
            ScreenToLoad = screenToLoad;
        }

        protected internal override void LoadContent(ContentManager content, GameScreenHelper parent)
        {
            base.LoadContent(content, parent);

            LoadFinished = false;

            LoadThread = new Thread(new ThreadStart(DoLoad));
        }

        protected override void UpdateActive(GameTime gameTime)
        {
            if (!LoadStarted)
            {
                LoadThread.Start();
                LoadStarted = true;
            }
        }

        protected void DoLoad()
        {
            Parent.ExitAllButCurrent();
            Parent.InsertGameScreen(ScreenToLoad);
            LoadFinished = true;
        }
    }
}
