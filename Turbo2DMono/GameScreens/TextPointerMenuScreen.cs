using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Turbo2D.Implementation;
using Turbo2D.Implementation.Menus;

namespace Turbo2D.Implementation.ScreenTypes
{
    public abstract class TextPointerMenuScreen : GameScreen
    {
        protected List<TextMenuItem> MenuItems = new List<TextMenuItem>();
        /// <summary>
        /// No more than two pointers, please!
        /// </summary>
        protected List<MenuPointer> Pointers = new List<MenuPointer>(2);

        protected void AddPointer(MenuPointer pointer)
        {
            Pointers.Add(pointer);
            AddGameObject(pointer);
        }

        protected void AddTextItem(TextMenuItem item)
        {
            MenuItems.Add(item);
            AddGameObject(item);
        }
    }
}
