﻿using System;

namespace Turbo2D.Interfaces
{
    public interface IDrawableObject
    {
        bool Visible { get; set; }
        bool Alive { get; }
        string Name { get; }
        string RenderGroup { get; set; }
        event EventHandler<RenderGroupChangedEventArgs> RenderGroupChanged;

        void Draw();
    }

    public class RenderGroupChangedEventArgs : EventArgs
    {
        public string OldGroup { get; protected set; }
        public string NewGroup { get; protected set; }

        public RenderGroupChangedEventArgs(string oldGroup, string newGroup)
        {
            OldGroup = oldGroup;
            NewGroup = newGroup;
        }
    }
}
