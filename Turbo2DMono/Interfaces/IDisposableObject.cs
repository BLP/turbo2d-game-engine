﻿
namespace Turbo2D.Interfaces
{
    public interface IDisposableObject
    {
        string Name { get; }

        void Dispose();
    }
}
