﻿using System;

namespace Turbo2D.Interfaces
{
    public interface IButton : IAcceptsMouseInput
    {
        event EventHandler<ButtonClickedEventArgs> Clicked;
        event EventHandler<ButtonClickedEventArgs> MouseOver;
        event EventHandler<ButtonClickedEventArgs> MouseEnter;
        event EventHandler<ButtonClickedEventArgs> MouseLeave;
    }

    public class ButtonClickedEventArgs : EventArgs
    {
        public string Name { get; protected set; }

        public ButtonClickedEventArgs(string name)
        {
            Name = name;
        }
    }
}
