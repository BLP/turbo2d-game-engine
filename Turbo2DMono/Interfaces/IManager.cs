﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Turbo2D.GameScreens;
using Microsoft.Xna.Framework;
using Turbo2D.GameObjects;

namespace Turbo2D.Interfaces
{
    public interface IManager
    {
        bool Enabled { get; set; }
        Type ManagedType { get; }
        //public bool ManageInheritedTypes { get; protected set; }

        void Update(GameTime gameTime);
        void Add(object managedObject);
        void Remove(object managedObject);
   }
}
